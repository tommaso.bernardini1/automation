1
00:00:01,111 --> 00:00:05,692
1. openBrowser("")

2
00:00:05,717 --> 00:00:05,969
5. maximizeWindow()

3
00:00:05,976 --> 00:00:07,353
9. navigateToUrl("https://qa.stella.cloud.c3.cgm.ag/")

4
00:00:07,362 --> 00:00:07,406
13. REGISTRATIONN_START_BUTTON = TestObject().addProperty("xpath", EQUALS, "//button[@id="startRegistrationButton"]")

5
00:00:07,414 --> 00:00:07,997
17. waitForElementVisible(REGISTRATIONN_START_BUTTON, 5)

6
00:00:08,008 --> 00:00:08,295
21. waitForElementPresent(REGISTRATIONN_START_BUTTON, 5)

7
00:00:08,300 --> 00:00:08,738
25. click(REGISTRATIONN_START_BUTTON)

8
00:00:08,746 --> 00:00:08,747
29. REGISTRATIONN_CODEPIN_INPUT = TestObject().addProperty("xpath", EQUALS, "//input[@name="codePin"]")

9
00:00:08,752 --> 00:00:09,284
33. waitForElementVisible(REGISTRATIONN_CODEPIN_INPUT, 5)

10
00:00:09,290 --> 00:00:09,573
37. waitForElementPresent(REGISTRATIONN_CODEPIN_INPUT, 5)

11
00:00:09,577 --> 00:00:10,266
41. setText(REGISTRATIONN_CODEPIN_INPUT, "iiii-0000-iiii")

12
00:00:10,270 --> 00:00:11,304
45. delay(1)

13
00:00:11,310 --> 00:00:11,313
49. REGISTRATIONN_NEXT_BUTTON = TestObject().addProperty("xpath", EQUALS, "//button[@id="next-button"]")

14
00:00:11,319 --> 00:00:11,644
53. waitForElementVisible(REGISTRATIONN_NEXT_BUTTON, 5)

15
00:00:11,650 --> 00:00:11,922
57. waitForElementPresent(REGISTRATIONN_NEXT_BUTTON, 5)

16
00:00:11,928 --> 00:00:12,266
61. click(REGISTRATIONN_NEXT_BUTTON)

17
00:00:12,271 --> 00:00:12,272
65. REGISTRATIONN_USERNAME_INPUT = TestObject().addProperty("xpath", EQUALS, "//input[@name="username"]")

18
00:00:12,279 --> 00:00:12,918
69. waitForElementVisible(REGISTRATIONN_USERNAME_INPUT, 5)

19
00:00:12,924 --> 00:00:13,537
73. waitForElementPresent(REGISTRATIONN_USERNAME_INPUT, 5)

20
00:00:13,542 --> 00:00:14,200
77. setText(REGISTRATIONN_USERNAME_INPUT, "bruno.marotta")

21
00:00:14,204 --> 00:00:14,205
81. REGISTRATIONN_PSW_INPUT = TestObject().addProperty("xpath", EQUALS, "//input[@name="password"]")

22
00:00:14,212 --> 00:00:14,529
85. waitForElementVisible(REGISTRATIONN_PSW_INPUT, 5)

23
00:00:14,536 --> 00:00:14,811
89. waitForElementPresent(REGISTRATIONN_PSW_INPUT, 5)

24
00:00:14,819 --> 00:00:15,467
93. setText(REGISTRATIONN_PSW_INPUT, "password")

25
00:00:15,472 --> 00:00:16,488
97. delay(1)

26
00:00:16,495 --> 00:00:16,805
101. click(REGISTRATIONN_NEXT_BUTTON)

27
00:00:16,814 --> 00:00:16,815
105. REGISTRATIONN_WORKSTATIONNAME_INPUT = TestObject().addProperty("xpath", EQUALS, "//input[@name="workstationName"]")

28
00:00:16,822 --> 00:00:18,353
109. waitForElementVisible(REGISTRATIONN_WORKSTATIONNAME_INPUT, 5)

29
00:00:18,360 --> 00:00:18,631
113. waitForElementPresent(REGISTRATIONN_WORKSTATIONNAME_INPUT, 5)

30
00:00:18,636 --> 00:00:19,293
117. setText(REGISTRATIONN_WORKSTATIONNAME_INPUT, "WorkStation_Tommaso")

31
00:00:19,300 --> 00:00:19,835
121. click(REGISTRATIONN_NEXT_BUTTON)

32
00:00:19,843 --> 00:00:19,846
125. REGISTRATIONN_GOTOLOGIN_BUTTON = TestObject().addProperty("xpath", EQUALS, "//button[@data-automation="WSREGWIZ-SUCCESS-GO-TO-LOGIN-BUTTON"]")

33
00:00:19,853 --> 00:00:20,808
129. waitForElementVisible(REGISTRATIONN_GOTOLOGIN_BUTTON, 5)

34
00:00:20,814 --> 00:00:21,090
133. waitForElementPresent(REGISTRATIONN_GOTOLOGIN_BUTTON, 5)

35
00:00:21,096 --> 00:00:21,459
137. click(REGISTRATIONN_GOTOLOGIN_BUTTON)

36
00:00:21,467 --> 00:00:22,485
141. delay(1)

37
00:00:22,490 --> 00:00:22,491
145. LOGIN_USERNAME_INPUT = TestObject().addProperty("xpath", EQUALS, "//input[@name="username"]")

38
00:00:22,496 --> 00:00:22,794
149. waitForElementVisible(LOGIN_USERNAME_INPUT, 5)

39
00:00:22,801 --> 00:00:23,079
153. waitForElementPresent(LOGIN_USERNAME_INPUT, 5)

40
00:00:23,086 --> 00:00:23,719
157. setText(LOGIN_USERNAME_INPUT, "bruno.marotta")

41
00:00:23,726 --> 00:00:23,728
161. LOGIN_PSW_INPUT = TestObject().addProperty("xpath", EQUALS, "//input[@name="password"]")

42
00:00:23,736 --> 00:00:24,020
165. waitForElementVisible(LOGIN_PSW_INPUT, 5)

43
00:00:24,025 --> 00:00:24,301
169. waitForElementPresent(LOGIN_PSW_INPUT, 5)

44
00:00:24,306 --> 00:00:24,949
173. setText(LOGIN_PSW_INPUT, "password")

45
00:00:24,955 --> 00:00:25,965
177. delay(1)

46
00:00:25,972 --> 00:00:25,973
181. LOGIN_ACCEDI_BUTTON = TestObject().addProperty("xpath", EQUALS, "//button[@data-automation="Login - Login button"]")

47
00:00:25,980 --> 00:00:26,272
185. waitForElementVisible(LOGIN_ACCEDI_BUTTON, 5)

48
00:00:26,277 --> 00:00:26,550
189. waitForElementPresent(LOGIN_ACCEDI_BUTTON, 5)

49
00:00:26,555 --> 00:00:26,893
193. click(LOGIN_ACCEDI_BUTTON)

