<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>INORDER_FISSO_SHP</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>100</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8469672e-0f22-47a6-a2c3-3b0b566a8d90</testSuiteGuid>
   <testCaseLink>
      <guid>1f40d279-412f-4d35-a6d9-28aea4759940</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/IN_ORDER_DATA_PREPARATION/InOrder_SHP_FISSO</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b149cd7a-a5c7-4d49-9a75-e9a06473e054</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/FILE_INPUT_ANAGRAFICA_DATA_PREPARATION/INPUT_ANAGRAFICA_FISSO_SHP</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>681186f6-2453-47ef-a24d-432db1dbc169</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>b5aacf2a-e0bb-411c-9ab1-a6e894a36f4d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>32795cbd-014e-42cc-b7dd-d1e5105ad3bd</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>ad9e965f-344d-4dce-b5ac-a35b74006eb9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>9a0f078e-9232-4b60-92d8-40af14b2d779</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>d050e4dd-57a1-400d-ad3f-adf7777f028f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>8ee693fd-133b-4dc4-a678-e9c207e2a1a7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>70e31173-5759-42af-9e8c-8b4cc8e05316</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>c66abc5a-16fe-4240-ba08-950da936126c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>dd2c8187-5fa5-4caa-8494-b69505b79938</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f71d6550-52b5-4cf5-b53c-3a48832945fa</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1c56282a-05d5-47ac-a820-72849eea8464</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ceb34d1f-726c-48d4-b78a-06b247517005</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>76a5aebd-581e-41ea-a994-5f47f441008a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4702cd5e-6a62-4dba-94a8-7101bb917c24</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5ef4056e-b909-44ca-8496-b8848a12bc07</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c5c54da9-c61d-4a9e-938f-37e2d3c72c2e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>0c5f0fc9-cd54-4569-8c18-e923e66f8586</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>00a96555-7d8c-4763-9ac9-aa86203ca97d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>0c45a92d-e24d-44c8-a437-eac5981e9d4e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>e117d267-a7ad-4ea5-b2b5-02c21b54f5e4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>151a947b-70a8-4f36-850e-48124a2c50f8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>47ecf38d-454a-48e9-8893-da4f604e8703</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>b800ed62-15e0-430f-b04f-c09a7d397035</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>f9894c78-3b5a-4176-86dd-5f48374f7744</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>9d39971a-a736-4b7b-b8b1-47d2cba62353</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>4c2f5986-e6c0-4562-bc3b-a0ff1b239d8b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>826817f9-192f-429a-b938-cea6081bed28</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DATA_COLUMN</type>
         <value></value>
         <variableId>0319325f-122c-4616-90f5-8f475cd80089</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>14a8cf9c-d39d-4860-a3e4-6931b53879d7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c4665579-0b14-4d19-bee0-16868138451d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>05539fc0-66ed-4355-97d5-99193cfa40e8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3e6fe095-3913-48c8-a100-14792b411990</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3108c253-07de-487f-9097-841bd305f500</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4b418837-9dd9-4c98-bb21-b9a328b59eb9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>536f8867-f52a-4756-a52f-8a8fd6b7f812</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c39011e3-1b3a-4729-8a20-ff92a55b5ce9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6fb6a25b-2280-4015-9d96-7408ad11afae</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8b0d17f2-ebc3-4202-9a41-4bd8d4a4b042</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c1340d10-e361-4625-80a4-5eea99f2f664</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>88f1ddeb-ba4e-445b-a547-d53fef28c9c5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bc92e6c4-9ba8-4e6b-9ef2-77daee9fdf9b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>619eafe8-8603-4485-8294-2e4b734a3ddc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6d2d810a-ebe8-4dc5-be2a-35f884f2013e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>462210c5-adc9-49db-b460-acfd255b2115</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>89950f34-99cb-4d6d-9e47-f285d0c09e75</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dbb13cd6-14b7-4f4f-820a-76553fe12d91</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b9a6c87c-8f49-4be6-a1f7-475bade882d2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>97f888ac-2fee-4f6f-a57a-551c2270ea52</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
