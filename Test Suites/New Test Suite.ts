<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>100</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b28b0893-d8f9-4a3e-941f-6760f486b2a4</testSuiteGuid>
   <testCaseLink>
      <guid>ae3b4855-4bd0-4e6c-ab06-62131bc80508</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/IN_ORDER_DATA_PREPARATION/CGM</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>97453da4-c64d-418d-93e6-be093854c7f0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/New Test Data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>97453da4-c64d-418d-93e6-be093854c7f0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>USERNAME</value>
         <variableId>8ac2f0d6-26b2-47c8-aed9-756cd830e69e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>97453da4-c64d-418d-93e6-be093854c7f0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>PSW</value>
         <variableId>6ec515fc-f997-4146-bffc-03526428c00b</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
