package com.at.util
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable



import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class Anagrafica_SHP {
	/**
	 * Inserimento anagrafica Soho Professional
	 */
	@Keyword
	def Inserimento_Anagrafica_SHP(RAGIONE_SOCIALE, FORMA_GIURIDICA, NOME_RAPPRESENTANTE_LEGALE, COGNOME_RAPPRESENTANTE_LEGALE, SESSO_RAPPRESENTANTE_LEGALE, DATA_DI_NASCITA_RAPPRESENTANTE_LEGALE,
			NAZIONE_DI_NASCITA_RAPPRESENTANTE_LEGALE, COMUNE_DI_NASCITA_RAPPRESENTANTE_LEGALE, NUMERO_MOBILE, EMAIL, CODICE_FISCALE_RAPPRESENTANTE_LEGALE, PARTITA_IVA, TIPOLOGIA_OFFERTA) {


		def Seleziona_new

		def click = new ClickUsingJS()

		WebUI.waitForElementVisible(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_Segmento_SHP'), 60)

		WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiFiscali'), 5)

		WebUI.click(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_Segmento_SHP'))

		WebUI.setText(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_Ragione_Sociale'), RAGIONE_SOCIALE)

		WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/select_Forma_Giuridica'), FORMA_GIURIDICA, true)

		WebUI.setText(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_Nome_Rappresentante_Legale'), NOME_RAPPRESENTANTE_LEGALE)

		WebUI.setText(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_Cognome_Rappresentante_Legale'), COGNOME_RAPPRESENTANTE_LEGALE)

		switch (SESSO_RAPPRESENTANTE_LEGALE) {
			case 'M':
				WebUI.click(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_M_Sesso_Rappresentante_Legale'))
				break

			case 'F':
				WebUI.click(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_F_Sesso_Rappresentante_Legale'))
				break
		}

		WebUI.setText(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_Data_di_Nascita_Rappresentante_Legale'), DATA_DI_NASCITA_RAPPRESENTANTE_LEGALE)

		WebUI.setText(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_Nazione_Rappresentante_legale'), NAZIONE_DI_NASCITA_RAPPRESENTANTE_LEGALE)

		Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
				NAZIONE_DI_NASCITA_RAPPRESENTANTE_LEGALE, true)

		click.clickUsingJS(Seleziona_new, 60)

		WebUI.setText(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_Comune_Rappresentante_legale'), COMUNE_DI_NASCITA_RAPPRESENTANTE_LEGALE)

		Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
				COMUNE_DI_NASCITA_RAPPRESENTANTE_LEGALE, true)

		click.clickUsingJS(Seleziona_new, 60)

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numerazione Mobile Rappr'), NUMERO_MOBILE)

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Mail_section1_email'), EMAIL)

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Conferma Mail_section1_e'), EMAIL)

		WebUI.setText(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_Codice_Fiscale_Rappresentante_Legale'), CODICE_FISCALE_RAPPRESENTANTE_LEGALE)

		WebUI.setText(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_Partita_Iva'), PARTITA_IVA)

		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
				60)

		WebUI.scrollToElement(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/button_Verifica_Codice_Fiscale_Rappresentante_Legale'), 5)

		WebUI.focus(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/button_Verifica_Codice_Fiscale_Rappresentante_Legale'))

		WebUI.waitForElementClickable(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/button_Verifica_Codice_Fiscale_Rappresentante_Legale'), 5)

		WebUI.click(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/button_Verifica_Codice_Fiscale_Rappresentante_Legale'))



		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
				60)

		WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiFiscali'), 5)

		switch (TIPOLOGIA_OFFERTA) {
			case 'FISSO':
				WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Tipologia_Offerta_FISSO'), FailureHandling.OPTIONAL)
				break

			case 'FISSO E MOBILE':
				WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Tipologia_Offerta_FISSO_E_MOBILE'), FailureHandling.OPTIONAL)
				break

			case 'MOBILE':
				WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Tipologia_Offerta_MOBILE'), FailureHandling.OPTIONAL)
				break
		}

		WebUI.delay(1)
	}
}