package com.at.util
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable


import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class Selezione_Metodo_di_Pagamento {
	/**
	 * Inserimento anagrafica Residenziale
	 */
	@Keyword
	def Inserimento_Ind_Attivazione_Ind_Residenza(TIPOLOGIA_OFFERTA, METODO_PAGAMENTO) {

		if (TIPOLOGIA_OFFERTA == 'MOBILE') {

			switch (METODO_PAGAMENTO) {
				case 'CARTA DI CREDITO':
					WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Carta_di_Credito'),
					5)

					WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Carta_di_Credito'))

					WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Carta_di_Credito'))

					break
				case 'CONTO CORRENTE':
					WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Conto corrente'),
					5)

					WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Conto corrente'))

					WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Conto corrente'))

					break
				case 'BOLLETTINO POSTALE':
					WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Bollettino_Postale'),
					5)

					WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Bollettino_Postale'))

					WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Bollettino_Postale'))

					break
			}
		}

		else {

			switch (METODO_PAGAMENTO) {
				case 'CARTA DI CREDITO':
					WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Carta di Credito_section (1)'), 5)

					WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/input_Carta di Credito_section (1)'))

					WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Carta di Credito_section (1)'))

					break
				case 'CONTO CORRENTE':
					WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Conto Corrente_Section (1)'), 5)

					WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/input_Conto Corrente_Section (1)'))

					WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Conto Corrente_Section (1)'))

					break
			}
		}
	}
}