package com.at.util
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable


import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException




class Anagrafica_RES {
	/**
	 * Inserimento anagrafica Residenziale
	 */
	@Keyword
	def Inserimento_Anagrafica_RES(NOME, COGNOME, SESSO, DATA_DI_NASCITA, NAZIONE_DI_NASCITA, COMUNE_DI_NASCITA, NUMERO_MOBILE, EMAIL, CODICE_FISCALE, TIPOLOGIA_OFFERTA) {


		def Seleziona_new

		def click = new ClickUsingJS()

		WebUI.waitForElementVisible(findTestObject('Page_Fastweb - Abbonamento Online/input_Nome_section1_nome'), 60)

		WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiFiscali'), 5)

		//WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Nome_section1_nome'), 5)
		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Nome_section1_nome'), NOME)

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Cognome_section1_cognome'), COGNOME)

		switch (SESSO) {
			case 'M':
				WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_M_section1_sesso'))

				break
			case 'F':
				WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_F_section1_sesso'))

				break
		}

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Data di Nascita_section1'), DATA_DI_NASCITA)

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Stato di Nascita_section'), NAZIONE_DI_NASCITA)

		Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
				NAZIONE_DI_NASCITA, true)

		click.clickUsingJS(Seleziona_new, 60)

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Comune di Nascita_sectio'), COMUNE_DI_NASCITA)

		Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
				COMUNE_DI_NASCITA, true)

		click.clickUsingJS(Seleziona_new, 60)

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numerazione Mobile Rappr'), NUMERO_MOBILE)

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Mail_section1_email'), EMAIL)

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Conferma Mail_section1_e'), EMAIL)

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Codice Fiscale_section1_'), CODICE_FISCALE)

		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
				60)

		WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Codice Fiscale'), 5)

		WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Codice Fiscale'))

		WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Codice Fiscale'), 5)

		WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Codice Fiscale'))

		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
				60)

		WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiFiscali'), 5)

		switch (TIPOLOGIA_OFFERTA) {
			case 'FISSO':
				WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Tipologia_Offerta_FISSO'), FailureHandling.OPTIONAL)

				break
			case 'FISSO E MOBILE':
				WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Tipologia_Offerta_FISSO_E_MOBILE'), FailureHandling.OPTIONAL)

				break
			case 'MOBILE':
				WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Tipologia_Offerta_MOBILE'), FailureHandling.OPTIONAL)

				break
		}

		WebUI.delay(1)


	}


}