package com.at.util
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable



import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import java.io.FileInputStream as FileInputStream
import java.io.FileOutputStream as FileOutputStream
import java.io.FileNotFoundException as FileNotFoundException
import java.io.IOException as IOException
import java.util.Date as Date
import org.apache.poi.hssf.usermodel.HSSFCell as HSSFCell
import org.apache.poi.hssf.usermodel.HSSFCellStyle as HSSFCellStyle
import org.apache.poi.hssf.usermodel.HSSFDataFormat as HSSFDataFormat
import org.apache.poi.hssf.usermodel.HSSFRow as HSSFRow
import org.apache.poi.hssf.usermodel.HSSFSheet as HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook as HSSFWorkbook
import org.apache.poi.hssf.util.HSSFColor as HSSFColor
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import org.apache.poi.openxml4j.exceptions.InvalidFormatException as InvalidFormatException
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.ss.usermodel.Workbook as Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory as WorkbookFactory
import org.apache.poi.ss.usermodel.Sheet as Sheet
import com.kms.katalon.core.exception.StepErrorException as StepErrorException
import com.kms.katalon.core.logging.KeywordLogger




class Selezione_Offerta {
	/**
	 * Selezione offerta Mobile
	 */
	@Keyword
	def Seleziona_Offerta_Mobile(TIPO_OFFERTA_MOBILE, OFFERTA_MOBILE, colonna) {

		def lavorazione_file_excel = new Lavorazione_file_excel()


		switch (TIPO_OFFERTA_MOBILE) {
			case 'MOBILE VOCE':
				WebUI.waitForElementVisible(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'), 60)

				WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'), 60)

				WebUI.focus(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'))

				WebUI.click(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'))

				break
			case 'MOBILE DATI':
				WebUI.waitForElementVisible(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'), 60)

				WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'), 60)

				WebUI.focus(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'))

				WebUI.click(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'))

				break
		}

		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
				60)

		//seleziona offerta
		def offerta_string = "//span[(text() = '$OFFERTA_MOBILE')]/parent::td/parent::tr/td[5]"

		def Seleziona_offerta_new = WebUI.modifyObjectProperty(findTestObject('Page_/Seleziona_Offerta_Fissa'), 'xpath', 'equals',
				offerta_string, true)

		WebUI.waitForElementClickable(Seleziona_offerta_new, 10)

		//properties= Seleziona_offerta_new.getProperties()
		//for (prop in properties) {
		//	prop.name;
		//	}
		try {
			WebUI.click(Seleziona_offerta_new, FailureHandling.STOP_ON_FAILURE //This will fail as locator is wrong

					)
		}
		catch (Exception ex) {

			WebUI.delay(1)
			lavorazione_file_excel.scrivi_valore_cella(colonna, 'OFFERTA MOBILE NON TROVATA')
			KeywordUtil.markFailedAndStop('Offerta NON Trovata - er: ' + ex)

		}

		lavorazione_file_excel.scrivi_valore_cella(colonna, 'OK')
		WebUI.delay(1)

	}



	/**
	 * Selezione offerta Fissa
	 */
	@Keyword
	def Seleziona_Offerta_Fissa(OFFERTA_FISSA, colonna) {

		def lavorazione_file_excel = new Lavorazione_file_excel()

		def offerta_string
		offerta_string = "//span[(text() = '$OFFERTA_FISSA')]/parent::td/parent::tr/td[5]"

		def Seleziona_offerta_new = WebUI.modifyObjectProperty(findTestObject('Page_/Seleziona_Offerta_Fissa'), 'xpath', 'equals',
				offerta_string, true)

		WebUI.waitForElementClickable(Seleziona_offerta_new, 10)

		try {
			WebUI.click(Seleziona_offerta_new, FailureHandling.STOP_ON_FAILURE //This will fail as locator is wrong
					)
		}
		catch (Exception ex) {
			WebUI.delay(1)
			lavorazione_file_excel.scrivi_valore_cella(colonna, 'OFFERTA FISSA NON TROVATA')
			KeywordUtil.markFailedAndStop('Offerta NON Trovata - er: ' + ex)
		}

		lavorazione_file_excel.scrivi_valore_cella(colonna, 'OK')
		WebUI.delay(1)


	}

}