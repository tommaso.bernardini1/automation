package com.at.util
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable



import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class Inserimento_IndirizzoAttivazione_e_IndirizzoResidenza {
	/**
	 * Inserimento anagrafica Residenziale
	 */
	@Keyword
	def Inserimento_Ind_Attivazione_Ind_Residenza_RES(TIPOLOGIA_OFFERTA, TECNOLOGIA, SEGMENTO, CITTA_DI_RESIDENZA, INDIRIZZO_DI_RESIDENZA, CIVICO_DI_RESIDENZA) {

		def Seleziona_new
		def click = new ClickUsingJS()
		def string_id_file_tecnologie='Data Files/FILE_EXCEL_SUPPORTO/INDIRIZZI_TECNOLOGIE'

		if (TIPOLOGIA_OFFERTA != 'MOBILE') {


			def row_tec
			def max_row_num_tec = findTestData(string_id_file_tecnologie).getRowNumbers()


			for (def rowNum_tec = 1; rowNum_tec <= max_row_num_tec; rowNum_tec++) {
				def tec = findTestData(string_id_file_tecnologie).getValue(1, rowNum_tec)

				if (tec == TECNOLOGIA) {
					row_tec = rowNum_tec
				}
			}



			WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiAttivazione'), 5)

			WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Citta di attivazione'), findTestData(string_id_file_tecnologie).getValue(
					2, row_tec))

			Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
					findTestData(string_id_file_tecnologie).getValue(2, row_tec), true)

			click.clickUsingJS(Seleziona_new, 60)

			WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Indirizzo di Attivazione'), findTestData(string_id_file_tecnologie).getValue(
					3, row_tec))

			Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
					findTestData(string_id_file_tecnologie).getValue(3, row_tec), true)

			click.clickUsingJS(Seleziona_new, 60)

			WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Civico di Attivazione_se (1)'), findTestData(string_id_file_tecnologie).getValue(
					4, row_tec))

			Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
					findTestData(string_id_file_tecnologie).getValue(5, row_tec), true)

			click.clickUsingJS(Seleziona_new, 60)

			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
					60)

			WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Copertura (1)'), 5)

			WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Copertura (1)'))

			WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Copertura (1)'))

			WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'), 60)

			WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'), 5)

			WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'))

			WebUI.delay(1)

			WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'), 5)


			WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Indirizzo di Atti (1)'), 60)
			WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Indirizzo di Atti (1)'))
			WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Indirizzo di Atti (1)'))
			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',60)
		} else {

			WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'), 5)

			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',	60)

			WebUI.setText(findTestObject('Object_MOBILE/input_Citta_RESIDENZA'), CITTA_DI_RESIDENZA)

			Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
					CITTA_DI_RESIDENZA, true)

			click.clickUsingJS(Seleziona_new, 60)

			WebUI.setText(findTestObject('Object_MOBILE/input_Indirizzo_RESIDENZA'), INDIRIZZO_DI_RESIDENZA)

			Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
					INDIRIZZO_DI_RESIDENZA, true)

			click.clickUsingJS(Seleziona_new, 60)

			WebUI.setText(findTestObject('Object_MOBILE/input_Civico_RESIDENZA'), CIVICO_DI_RESIDENZA)

			Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
					CIVICO_DI_RESIDENZA, true)

			click.clickUsingJS(Seleziona_new, 60)
		}


		WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Dati Fiscali (1)'))

		WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Dati Fiscali (1)'))

		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
				60)

		WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'), 5)

		WebUI.delay(1)
	}



	/**
	 * Inserimento anagrafica SHP
	 */
	@Keyword
	def Inserimento_Ind_Attivazione_Ind_Residenza_SHP(TIPOLOGIA_OFFERTA, TECNOLOGIA, SEGMENTO, CITTA_DI_RESIDENZA_SEDE_LEGALE, INDIRIZZO_DI_RESIDENZA_SEDE_LEGALE, CIVICO_DI_RESIDENZA_SEDE_LEGALE) {

		def Seleziona_new
		def click = new ClickUsingJS()
		def string_id_file_tecnologie='Data Files/FILE_EXCEL_SUPPORTO/INDIRIZZI_TECNOLOGIE'

		if (TIPOLOGIA_OFFERTA != 'MOBILE') {


			def row_tec
			def max_row_num_tec = findTestData(string_id_file_tecnologie).getRowNumbers()


			for (def rowNum_tec = 1; rowNum_tec <= max_row_num_tec; rowNum_tec++) {
				def tec = findTestData(string_id_file_tecnologie).getValue(1, rowNum_tec)

				if (tec == TECNOLOGIA) {
					row_tec = rowNum_tec
				}
			}



			WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiAttivazione'), 5)

			WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Citta di attivazione'), findTestData(string_id_file_tecnologie).getValue(
					2, row_tec))

			Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
					findTestData(string_id_file_tecnologie).getValue(2, row_tec), true)

			click.clickUsingJS(Seleziona_new, 60)

			WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Indirizzo di Attivazione'), findTestData(string_id_file_tecnologie).getValue(
					3, row_tec))

			Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
					findTestData(string_id_file_tecnologie).getValue(3, row_tec), true)

			click.clickUsingJS(Seleziona_new, 60)

			WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Civico di Attivazione_se (1)'), findTestData(string_id_file_tecnologie).getValue(
					4, row_tec))

			Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
					findTestData(string_id_file_tecnologie).getValue(5, row_tec), true)

			click.clickUsingJS(Seleziona_new, 60)

			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
					60)

			WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Copertura (1)'), 5)

			WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Copertura (1)'))

			WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Copertura (1)'))

			WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'), 60)

			WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'), 5)

			WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'))

			WebUI.delay(1)

			WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'))

			WebUI.delay(1)

			WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'), 5)

			WebUI.waitForElementClickable(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_Attivazione_Sede_Legale'), 60)
			WebUI.focus(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_Attivazione_Sede_Legale'))
			WebUI.click(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_Attivazione_Sede_Legale'))
			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',60)
		} else {



			WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'), 5)

			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
					60)

			WebUI.setText(findTestObject('Object Repository/Object_MOBILE/input_Citta_Residenza_Sede_Legale'), CITTA_DI_RESIDENZA_SEDE_LEGALE)

			Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
					CITTA_DI_RESIDENZA_SEDE_LEGALE, true)

			click.clickUsingJS(Seleziona_new, 60)

			WebUI.setText(findTestObject('Object Repository/Object_MOBILE/input_Indirizzo_Residenza_Sede_Legale'), INDIRIZZO_DI_RESIDENZA_SEDE_LEGALE)

			Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
					INDIRIZZO_DI_RESIDENZA_SEDE_LEGALE, true)

			click.clickUsingJS(Seleziona_new, 60)

			WebUI.setText(findTestObject('Object Repository/Object_MOBILE/input_Civico_Residenza_Sede_Legale'), CIVICO_DI_RESIDENZA_SEDE_LEGALE)

			Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
					CIVICO_DI_RESIDENZA_SEDE_LEGALE, true)

			click.clickUsingJS(Seleziona_new, 60)
		}


		WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Dati Fiscali (1)'))

		WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Dati Fiscali (1)'))

		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
				60)

		WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'), 5)

		WebUI.delay(1)
	}
}