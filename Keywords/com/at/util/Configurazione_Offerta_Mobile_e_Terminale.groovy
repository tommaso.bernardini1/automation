package com.at.util
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable


import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

import java.io.FileInputStream as FileInputStream
import java.io.FileOutputStream as FileOutputStream
import java.io.FileNotFoundException as FileNotFoundException
import java.io.IOException as IOException
import java.util.Date as Date
import org.apache.poi.hssf.usermodel.HSSFCell as HSSFCell
import org.apache.poi.hssf.usermodel.HSSFCellStyle as HSSFCellStyle
import org.apache.poi.hssf.usermodel.HSSFDataFormat as HSSFDataFormat
import org.apache.poi.hssf.usermodel.HSSFRow as HSSFRow
import org.apache.poi.hssf.usermodel.HSSFSheet as HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook as HSSFWorkbook
import org.apache.poi.hssf.util.HSSFColor as HSSFColor
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import org.apache.poi.openxml4j.exceptions.InvalidFormatException as InvalidFormatException
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.ss.usermodel.Workbook as Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory as WorkbookFactory
import org.apache.poi.ss.usermodel.Sheet as Sheet
import com.kms.katalon.core.exception.StepErrorException as StepErrorException



class Configurazione_Offerta_Mobile_e_Terminale {
	/**
	 * Seleziona la tipologia della famiglia dell'offerta mobile, la tipologia di vendita del terminale e il modello del terminale
	 */
	@Keyword
	def Selezione_Tipologia_Mobile_e_terminale(TIPO_FAMIGLIA_MOBILE, TIPO_VENDITA_TERMINALE, TIPO_MODELLO_TERMINALE, colonna ) {

		def click = new ClickUsingJS()
		def lavorazione_file_excel = new Lavorazione_file_excel()

		WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_TIPO_MOBILE'), 2, FailureHandling.OPTIONAL)

		//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_COUPON'), FailureHandling.CONTINUE_ON_FAILURE)
		click.clickUsingJS(findTestObject('Object_MOBILE/Open_Accordion_TIPO_MOBILE'), 60)
		//WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_TIPO_MOBILE'), FailureHandling.OPTIONAL)

		WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

		try {
			WebUI.selectOptionByValue(findTestObject('Object_MOBILE/select_Tipo_Famiglia_Mobile'), TIPO_FAMIGLIA_MOBILE, true) //This will fail as locator is wrong
		}
		catch (Exception ex) {
			WebUI.delay(1)
			lavorazione_file_excel.scrivi_valore_cella(colonna, 'TIPO FAMIGLIA MOBILE NON TROVATA')
			KeywordUtil.markFailedAndStop('Tipo di Famiglia Mobile NON Presente - error: ' + ex)
		}

		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
				60)

		try {
			WebUI.selectOptionByValue(findTestObject('Object_MOBILE/select_Tipo_Vendita_Terminale'), TIPO_VENDITA_TERMINALE, true) //This will fail as locator is wrong
		}
		catch (Exception ex) {
			WebUI.delay(1)
			lavorazione_file_excel.scrivi_valore_cella(colonna, 'TIPO VENDITA TERMINALE NON TROVATA')
			KeywordUtil.markFailedAndStop('Tipo Vendita Terminale NON Presente - error: ' + ex)
		}


		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
				60)

		if (TIPO_MODELLO_TERMINALE != '') {

			try {
				WebUI.selectOptionByValue(findTestObject('Object_MOBILE/select_Tipo_Modello_Terminale'), TIPO_MODELLO_TERMINALE, true) //This will fail as locator is wrong
			}
			catch (Exception ex) {
				WebUI.delay(1)
				lavorazione_file_excel.scrivi_valore_cella(colonna, 'TIPO MODELLO TERMINALE NON TROVATO')
				KeywordUtil.markFailedAndStop('Modello Terminale NON Presente ' + ex)
			}
		}


		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
				60)

		lavorazione_file_excel.scrivi_valore_cella(colonna, 'OK')

	}



	/**
	 * Seleziona il terminale
	 */
	@Keyword
	def Seleziona_Terminale(TERMINALE, colonna) {

		def click = new ClickUsingJS()
		def lavorazione_file_excel = new Lavorazione_file_excel()

		WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_TERMINALI'), 2, FailureHandling.OPTIONAL)

		//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_APPARATI'), FailureHandling.CONTINUE_ON_FAILURE)
		click.clickUsingJS(findTestObject('Object_MOBILE/Open_Accordion_TERMINALI'), 60)
		//WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_TERMINALI'), FailureHandling.OPTIONAL)

		//def terminale_string = "//span[text()='$TERMINALE']/preceding::input[@type='radio'][1]"
		def terminale_string = "//span[text()[contains(.,'$TERMINALE')]]/preceding::input[@type='radio'][1]"
		def Seleziona_terminale_new = WebUI.modifyObjectProperty(findTestObject('/Object_MOBILE/Seleziona_Terminale'), 'xpath',
				'equals', terminale_string, true)

		try {
			click.clickUsingJS(Seleziona_terminale_new, 60)
		}
		catch (Exception ex) {
			WebUI.delay(1)
			lavorazione_file_excel.scrivi_valore_cella(colonna, 'TERMINALE NON TROVATO')
			KeywordUtil.markFailedAndStop('Terminale NON Presente - error: ' + ex)


		}

		lavorazione_file_excel.scrivi_valore_cella(colonna, 'OK')

		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
				60)

		WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

	}




	/**
	 * Selezione mnp ed eventualmente la tcr
	 */
	@Keyword
	def Seleziona_mnp_tcr(RICHIESTA_TCR) {

		def click = new ClickUsingJS()

		WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_CONFIGURA_USIM'), 2, FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Open_Accordion_CONFIGURA_USIM'), 2)

		//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_STS'), FailureHandling.CONTINUE_ON_FAILURE)
		click.clickUsingJS(findTestObject('Object_MOBILE/Open_Accordion_CONFIGURA_USIM'), 60)
		//WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_CONFIGURA_USIM'), FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.focus(findTestObject('Object_MOBILE/Seleziona_Richiesta_MNP'))

		click.clickUsingJS(findTestObject('Object_MOBILE/Seleziona_Richiesta_MNP'), 60)
		//WebUI.click(findTestObject('Object_MOBILE/Seleziona_Richiesta_MNP'))

		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
				60)

		if (RICHIESTA_TCR == 'SI') {
			WebUI.focus(findTestObject('Object_MOBILE/Seleziona_Richiesta_TCR'))

			//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_STS'), FailureHandling.CONTINUE_ON_FAILURE)
			click.clickUsingJS(findTestObject('Object_MOBILE/Seleziona_Richiesta_TCR'), 60)
			//WebUI.click(findTestObject('Object_MOBILE/Seleziona_Richiesta_TCR'), FailureHandling.CONTINUE_ON_FAILURE)

			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style',
					'display: none;', 60)
		}

		WebUI.delay(1)

	}


	/**
	 * inserimento dati ICCID
	 */
	@Keyword
	def Inserimento_ICCID(iccid_3A_3C, FILE_ICCID) {

		def row_iccid

		WebUI.waitForElementVisible(findTestObject('Object_MOBILE/input_ICCID'), 60)

		def max_row_num_iccid = findTestData(FILE_ICCID).getRowNumbers()

		for (def rowNum_iccid = 1; rowNum_iccid <= max_row_num_iccid; rowNum_iccid++) {
			def iccid = findTestData(FILE_ICCID).getValue(iccid_3A_3C, rowNum_iccid)

			if (iccid != 'X') {
				row_iccid = rowNum_iccid

				def ICCID = findTestData(FILE_ICCID).getValue(iccid_3A_3C, row_iccid)

				WebUI.setText(findTestObject('Object_MOBILE/input_ICCID'), ICCID)

				WebUI.waitForElementAttributeValue(findTestObject('Object_MOBILE/WAIT-ICCID'), 'class', 'loading-div ng-hide',
						60)

				WebUI.delay(1)

				def msisdn = WebUI.getAttribute(findTestObject('Object_MOBILE/input_MSISDN'), 'value')

				if (msisdn != '') {
					break
				} else {
					def URL_iccd = findTestData(FILE_ICCID).getSourceUrl()

					String filePath = URL_iccd

					Workbook workbook

					FileInputStream fis

					FileOutputStream fos

					fis = new FileInputStream(filePath)

					workbook = WorkbookFactory.create(fis)

					def sheet = workbook.getSheetAt(0)

					Row row = sheet.getRow(row_iccid)

					Cell cell = row.getCell(iccid_3A_3C - 1)

					cell.setCellValue('X')

					fis.close()

					fos = new FileOutputStream(filePath)

					workbook.write(fos)

					fos.close()
				}
			}
		}

		return row_iccid

	}


	/**
	 * Configurazione dati mnp
	 */
	@Keyword
	def Configura_MNP(NOME, COGNOME, CODICE_FISCALE, NUMERO_DOCUMENTO, MNP_MSISDN_PORTATO, MNP_CONTRATTO_ORIGINE, MNP_OPERATORE_TELEFONICO, MNP_ICCID_DONATING_13_cifre) {

		WebUI.waitForElementVisible(findTestObject('Object_MOBILE/input_MNP_Nome'), 60)

		WebUI.setText(findTestObject('Object_MOBILE/input_MNP_Nome'), NOME)

		WebUI.setText(findTestObject('Object_MOBILE/input_MNP_Cognome'), COGNOME)

		WebUI.setText(findTestObject('Object_MOBILE/input_MNP_CF-PIVA'), CODICE_FISCALE)

		WebUI.selectOptionByLabel(findTestObject('Object_MOBILE/select_MNP_Tipo_Documento'), 'Carta di identità', true)

		WebUI.setText(findTestObject('Object_MOBILE/input_MNP_Numero_Documento'), NUMERO_DOCUMENTO)

		WebUI.setText(findTestObject('Object_MOBILE/input_MNP_MSISDN_Portato'), MNP_MSISDN_PORTATO)

		WebUI.selectOptionByLabel(findTestObject('Object_MOBILE/select_MNP_Tipo_Contratto_Origine'), MNP_CONTRATTO_ORIGINE,
				true, FailureHandling.OPTIONAL)

		WebUI.selectOptionByLabel(findTestObject('Object_MOBILE/select_MNP_Operatore_Telefonico'), MNP_OPERATORE_TELEFONICO,
				true)

		WebUI.sendKeys(findTestObject('Object_MOBILE/input_MNP_ICCID_Donating_13_cifre'), MNP_ICCID_DONATING_13_cifre)

		WebUI.delay(1)
	}


	/**
	 * Configurazione imei
	 */
	@Keyword
	def Configura_mei(IMEI, FILE_IMEI, TERMINALE ) {

		def URL_imei = findTestData(FILE_IMEI).getSourceUrl()
		String filePath = URL_imei
		Workbook workbook
		FileInputStream fis
		FileOutputStream fos
		fis = new FileInputStream(filePath)
		workbook = WorkbookFactory.create(fis)
		def sheet = workbook.getSheetAt(0)

		if (IMEI == '') {

			def row_terminale
			int col_imei
			def imei

			def max_row_num_terminale = findTestData(FILE_IMEI).getRowNumbers()

			for (def rowNum_terminale = 1; rowNum_terminale <= max_row_num_terminale; rowNum_terminale++) {
				def terminale = findTestData(FILE_IMEI).getValue(1, rowNum_terminale)

				if (terminale == TERMINALE) {
					row_terminale = rowNum_terminale

					break
				}
			}

			def colNum_imei_partenza = findTestData(FILE_IMEI).getValue(5, row_terminale)

			int colNum_imei_partenza_intero = Integer.parseInt(colNum_imei_partenza)

			def tasto_procedi

			for (col_imei = colNum_imei_partenza_intero; col_imei <= 4; col_imei++) {
				if (tasto_procedi == 'disabled') {
					break
				}

				imei = findTestData(FILE_IMEI).getValue(col_imei, row_terminale)

				if (imei == 'X') {
					//
				} else {
					WebUI.setText(findTestObject('Object_MOBILE/input_IMEI'), imei)

					WebUI.waitForElementAttributeValue(findTestObject('Object_MOBILE/WAIT-IMEI'), 'class', 'loading-div ng-hide',
							60)

					tasto_procedi = WebUI.getAttribute(findTestObject('Object_MOBILE/input_IMEI'), 'disabled')

					if (tasto_procedi) {
						break
					} else {

						Row row = sheet.getRow(row_terminale)

						Cell cell = row.getCell(col_imei - 1)

						cell.setCellValue('X')

					}
				}
			}

			// aggiorno la colonna di partenza imei per il prossimo test

			Row row = sheet.getRow(row_terminale)

			Cell cell = row.getCell(4)

			cell.setCellValue(col_imei + 1)

		} else {

			WebUI.setText(findTestObject('Object_MOBILE/input_IMEI'), IMEI)

			WebUI.waitForElementAttributeValue(findTestObject('Object_MOBILE/WAIT-IMEI'), 'class', 'loading-div ng-hide', 60)
		}



		fis.close()
		fos = new FileOutputStream(filePath)
		workbook.write(fos)
		fos.close()

	}





}