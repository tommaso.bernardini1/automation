package com.at.util
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable



import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class Completamento_Dati_Documento {
	/**
	 * completamento dei dati in Fase 2 del documento
	 */
	@Keyword
	def Inserimento_Dati_Documento(NAZIONALITA_DOCUMENTO, NUMERO_DOCUMENTO, CITTA_RILASCIO_DOCUMENTO, DATA_RILASCIO_DOCUMENTO) {

		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
				60)

		def Seleziona_new

		def click = new ClickUsingJS()

		WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_Documenti'), 5)

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Nazionalit Documento_sec (1)'), NAZIONALITA_DOCUMENTO)

		Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', NAZIONALITA_DOCUMENTO, true)

		click.clickUsingJS(Seleziona_new, 60)

		WebUI.selectOptionByValue(findTestObject('Page_Fastweb - Abbonamento Online/select_Carta di identita (1)'), 'CARTA DI IDENTITA', true)

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero Documento_section (1)'), NUMERO_DOCUMENTO)

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Localit di Rilascio Docu (1)'), CITTA_RILASCIO_DOCUMENTO)

		Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
				CITTA_RILASCIO_DOCUMENTO, true)

		click.clickUsingJS(Seleziona_new, 60)

		WebUI.selectOptionByValue(findTestObject('Page_Fastweb - Abbonamento Online/select_Comune (1)'), 'COMUNE', true)

		WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Data di Rilascio Documen (1)'), DATA_RILASCIO_DOCUMENTO)

		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
				60)

		WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Documento (1)'), 5)

		WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Documento (1)'))

		WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Documento (1)'))

		WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
				60)
	}
}