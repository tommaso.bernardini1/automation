package com.at.util
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


class Verifica_Configurazione_Offerta {
	/**
	 * verifica la presenza delle opzioni all'interno dell'acordion passato in input
	 */
	@Keyword
	def Verifica_Opzioni(accordion) {

		WebDriver driver = DriverFactory.getWebDriver()
		KeywordLogger log = new KeywordLogger()
		def string_xpath = "//div[text()[normalize-space() ='$accordion']]/following-sibling::*[1]//label[@class='attributeNameLabel']"
		def numero = driver.findElements(By.xpath(string_xpath)).size()
		def nome
		def flag
		def string_accordion = "//div[text()[normalize-space() ='$accordion']]/img"
		TestObject acc_OBJ = new TestObject('acc_OBJ').addProperty('xpath', ConditionType.EQUALS, string_accordion, true)
		WebUI.click(acc_OBJ, FailureHandling.OPTIONAL)
		def OBJ_xpath
		def att_OBJ_xpath
		def attributo
		def OBJ_car_string_recurring
		def prezzo_recurring
		def OBJ_car_string_oneshot
		def prezzo_oneshot
		def OBJ_car_string_upfront
		def prezzo_upfront
		def OBJ_car_string_rata
		def prezzo_rata
		def OBJ_xpath_costounatantum
		def costo_una_tantum_vetrina
		def OBJ_xpath_costoricorrente
		def costo_ricorrente_vetrina

		if (numero > 0) {
			for (int numberOfRows = 1; numberOfRows <= numero; numberOfRows++) {

				OBJ_xpath = "//div[text()[normalize-space() ='$accordion']]/following-sibling::*[1]/div[@class='section'][$numberOfRows]/div/label"
				TestObject OBJ = new TestObject('OBJ').addProperty('xpath', ConditionType.EQUALS, OBJ_xpath, true)
				nome = WebUI.getText(OBJ)

				att_OBJ_xpath = "//div[text()[normalize-space() ='$accordion']]/following-sibling::*[1]/div[@class='section'][$numberOfRows]/table//input[@class='img-add hideArea']"
				TestObject att_OBJ = new TestObject('att_OBJ').addProperty('xpath', ConditionType.EQUALS, att_OBJ_xpath, true)
				attributo = WebUI.getAttribute(att_OBJ, 'onchange')
				flag = attributo.contains(nome)

				log.logInfo((nome + ' - FLAG: ') + flag)

				OBJ_xpath_costounatantum = "//div[text()[normalize-space() ='$accordion']]/following-sibling::*[1]/div[@class='section'][$numberOfRows]/table//div[@class='itemPricesAndInfo']//label[text()[normalize-space() ='Costo una tantum:']]//following-sibling::*[1]"
				TestObject OBJ_costounatantum = new TestObject('OBJ_costounatantum').addProperty('xpath', ConditionType.EQUALS, OBJ_xpath_costounatantum, true)
				costo_una_tantum_vetrina = WebUI.getText(OBJ_costounatantum)
				log.logInfo((nome + ' - Costo una tantum vetrina: ') + costo_una_tantum_vetrina)

				OBJ_xpath_costoricorrente = "//div[text()[normalize-space() ='$accordion']]/following-sibling::*[1]/div[@class='section'][$numberOfRows]/table//div[@class='itemPricesAndInfo']//label[text()[normalize-space() ='Costo ricorrente:']]//following-sibling::*[1]"
				TestObject OBJ_costoricorrente = new TestObject('OBJ_costoricorrente').addProperty('xpath', ConditionType.EQUALS, OBJ_xpath_costoricorrente, true)
				costo_ricorrente_vetrina = WebUI.getText(OBJ_costoricorrente)
				log.logInfo((nome + ' - Costo ricorrente vetrina: ') + costo_ricorrente_vetrina)

				if (flag) {

					if (!costo_ricorrente_vetrina.contains('0,00')) {

						OBJ_car_string_recurring="//table[@class='summary-table' and @id='recurring-table']//tr[@class[not(contains(., 'hide-area'))]]//td[text()[normalize-space() ='$nome']]/following-sibling::*[1]/span[1]"
						TestObject OBJ_car_rec = new TestObject('OBJ_car_rec').addProperty('xpath', ConditionType.EQUALS, OBJ_car_string_recurring, true)
						prezzo_recurring = WebUI.getText(OBJ_car_rec, FailureHandling.OPTIONAL)
						log.logInfo(nome + ' - Prezzo ricorrente carrello: ' + prezzo_recurring)

						if (costo_ricorrente_vetrina==prezzo_recurring) {
							KeywordUtil.markPassed('Prezzo ricorrente carrello uguale a prezzo ricorrente vetrina')
						}
						else {
							KeywordUtil.markError("Prezzo ricorrente carrello NON uguale a prezzo ricorrente vetrina")
						}
					}

					if (!costo_una_tantum_vetrina.contains('0,00')) {

						OBJ_car_string_oneshot="//table[@class='summary-table' and @id='oneTime-table']//tr[@class[not(contains(., 'hide-area'))]]//td[text()[normalize-space() ='$nome']]/following-sibling::*[1]/span[1]"
						TestObject OBJ_car_one = new TestObject('OBJ_car_one').addProperty('xpath', ConditionType.EQUALS, OBJ_car_string_oneshot, true)
						prezzo_oneshot = WebUI.getText(OBJ_car_one, FailureHandling.OPTIONAL)
						log.logInfo(nome + ' - Prezzo OneShot: ' + prezzo_oneshot)

						if (costo_una_tantum_vetrina==prezzo_oneshot) {
							KeywordUtil.markPassed('One Shot carrello uguale a costo una tantum vetrina')
						}
						else {
							KeywordUtil.markError("One Shot carrello NON uguale a costo una tantum vetrina")
						}
					}
				}
			}
		} else {
			KeywordUtil.markWarning("$accordion NON Presenti")
		}
	}


	/**
	 * verifica la presenza delle opzioni all'interno dell'acordion passato in input
	 */
	@Keyword
	def Verifica_Terminali(accordion_terminali) {

		WebDriver driver = DriverFactory.getWebDriver()
		KeywordLogger log = new KeywordLogger()
		def string_xpath = "//div[text()[normalize-space() ='$accordion_terminali']]/following-sibling::*[1]//label[@class='attributeNameLabel']"
		def numero = driver.findElements(By.xpath(string_xpath)).size()
		def nome_terminale
		def flag
		def string_accordion = "//div[text()[normalize-space() ='$accordion_terminali']]/img"
		TestObject acc_OBJ = new TestObject('acc_OBJ').addProperty('xpath', ConditionType.EQUALS, string_accordion, true)
		WebUI.click(acc_OBJ, FailureHandling.OPTIONAL)
		def OBJ_xpath
		def att_OBJ_xpath
		def attributo
		def OBJ_car_string_recurring
		def prezzo_recurring
		def OBJ_car_string_oneshot
		def prezzo_oneshot
		def terminale_rateale
		def OBJ_car_string_upfront
		def prezzo_upfront
		def OBJ_car_string_rata
		def prezzo_rata
		def OBJ_xpath_costounatantum
		def costo_una_tantum_vetrina
		def OBJ_xpath_costoricorrente
		def costo_ricorrente_vetrina

		if (numero > 0) {
			for (int numberOfRows = 1; numberOfRows <= numero; numberOfRows++) {

				OBJ_xpath = "//div[text()[normalize-space() ='$accordion_terminali']]/following-sibling::*[1]/div[@class='section'][$numberOfRows]/div/label"
				TestObject OBJ = new TestObject('OBJ').addProperty('xpath', ConditionType.EQUALS, OBJ_xpath, true)
				nome_terminale = WebUI.getText(OBJ)

				att_OBJ_xpath = "//div[text()[normalize-space() ='$accordion_terminali']]/following-sibling::*[1]/div[@class='section'][$numberOfRows]/table//input[@class[contains(., 'img-add')] and @type='radio']"
				//forse meglio usare "//div[text()[normalize-space() ='$accordion_terminali']]/following-sibling::*[1]/div[@class='section'][$numberOfRows]/table//input[@class[contains(., 'img-add')] and @type='radio']"
				TestObject att_OBJ = new TestObject('att_OBJ').addProperty('xpath', ConditionType.EQUALS, att_OBJ_xpath, true)
				attributo = WebUI.getAttribute(att_OBJ, 'onchange')
				flag = attributo.contains(nome_terminale)

				log.logInfo((nome_terminale + ' - FLAG: ') + flag)

				OBJ_xpath_costounatantum = "//div[text()[normalize-space() ='$accordion_terminali']]/following-sibling::*[1]/div[@class='section'][$numberOfRows]/table//div[@class='itemPricesAndInfo']//label[text()[normalize-space() ='Costo una tantum:']]//following-sibling::*[1]"
				TestObject OBJ_costounatantum = new TestObject('OBJ_costounatantum').addProperty('xpath', ConditionType.EQUALS, OBJ_xpath_costounatantum, true)
				costo_una_tantum_vetrina = WebUI.getText(OBJ_costounatantum)
				log.logInfo((nome_terminale + ' - Costo una tantum vetrina: ') + costo_una_tantum_vetrina)

				OBJ_xpath_costoricorrente = "//div[text()[normalize-space() ='$accordion_terminali']]/following-sibling::*[1]/div[@class='section'][$numberOfRows]/table//div[@class='itemPricesAndInfo']//label[text()[normalize-space() ='Costo ricorrente:']]//following-sibling::*[1]"
				TestObject OBJ_costoricorrente = new TestObject('OBJ_costoricorrente').addProperty('xpath', ConditionType.EQUALS, OBJ_xpath_costoricorrente, true)
				costo_ricorrente_vetrina = WebUI.getText(OBJ_costoricorrente)
				log.logInfo((nome_terminale + ' - Costo ricorrente vetrina: ') + costo_ricorrente_vetrina)



				if (flag) {

					terminale_rateale=nome_terminale.contains('rata')

					if (terminale_rateale) {

						OBJ_car_string_upfront="//table[@class='summary-table' and @id='oneTime-table']//tr[@class[not(contains(., 'hide-area'))]]//td[text()[contains(.,'Contributo iniziale')]]/following-sibling::*[1]/span[1]"
						TestObject OBJ_car_upf = new TestObject('OBJ_car_upf').addProperty('xpath', ConditionType.EQUALS, OBJ_car_string_upfront, true)
						prezzo_upfront = WebUI.getText(OBJ_car_upf)
						log.logInfo(nome_terminale + ' - Prezzo UpFront carrello: ' + prezzo_upfront)

						if (costo_una_tantum_vetrina==prezzo_upfront) {
							KeywordUtil.markPassed("Upfront carrello uguale a costo una tantum vetrina")
						}
						else {
							KeywordUtil.markFailed("Upfront carrello NON uguale al costo una tantum vetrina")
						}

						OBJ_car_string_rata="//table[@class='summary-table' and @id='oneTime-table']//tr[@class[not(contains(., 'hide-area'))]]//td[text()[contains(.,'Contributo ricorrente')]]/following-sibling::*[1]/span[1]"
						TestObject OBJ_car_rata = new TestObject('OBJ_car_rata').addProperty('xpath', ConditionType.EQUALS, OBJ_car_string_rata, true)
						prezzo_rata = WebUI.getText(OBJ_car_rata)
						log.logInfo(nome_terminale + ' - Prezzo Rata carrello: ' + prezzo_rata)

						if (costo_ricorrente_vetrina==prezzo_rata) {
							KeywordUtil.markPassed("Rata carrello uguale a costo ricorrente vetrina")
						}
						else {
							KeywordUtil.markFailed("Rata carrello NON uguale a costo ricorrente vetrina")
						}

					}

					else {

						/*
						 OBJ_car_string_recurring="//table[@class='summary-table' and @id='recurring-table']//tr[@class[not(contains(., 'hide-area'))]]//td[text()[normalize-space() ='$nome_terminale']]/following-sibling::*[1]/span[1]"
						 TestObject OBJ_car_rec = new TestObject('OBJ_car_rec').addProperty('xpath', ConditionType.EQUALS, OBJ_car_string_recurring, true)
						 prezzo_recurring = WebUI.getText(OBJ_car_rec, FailureHandling.CONTINUE_ON_FAILURE)
						 log.logInfo(nome_terminale + ' Prezzo ricorrente: ' + prezzo_recurring)
						 */

						OBJ_car_string_oneshot="//table[@class='summary-table' and @id='oneTime-table']//tr[@class[not(contains(., 'hide-area'))]]//td[text()[normalize-space() ='$nome_terminale']]/following-sibling::*[1]/span[1]"
						TestObject OBJ_car_one = new TestObject('OBJ_car_one').addProperty('xpath', ConditionType.EQUALS, OBJ_car_string_oneshot, true)
						prezzo_oneshot = WebUI.getText(OBJ_car_one, FailureHandling.CONTINUE_ON_FAILURE)
						log.logInfo(nome_terminale + ' - Prezzo OneShot carrello: ' + prezzo_oneshot)

						if (costo_una_tantum_vetrina==prezzo_oneshot) {
							KeywordUtil.markPassed('One Shot carrello uguale a costo una tantum vetrina')
						}
						else {
							KeywordUtil.markError("One Shot carrello NON uguale a costo una tantum vetrina")
						}

					}




				}
			}


		} else {
			KeywordUtil.markWarning("$accordion_terminali NON Presenti")
		}


	}
}