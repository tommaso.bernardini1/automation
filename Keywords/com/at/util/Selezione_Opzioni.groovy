package com.at.util
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable



import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class Selezione_Opzioni {
	/**
	 * Inserimento anagrafica Soho Professional
	 */
	@Keyword
	def Seleziona_Opzione(OPZIONE_1, OPZIONE_2, OPZIONE_3, OPZIONE_4, OPZIONE_5, colonna_1, colonna_2, colonna_3, colonna_4, colonna_5) {
		
		def lavorazione_file_excel = new Lavorazione_file_excel()

		if (OPZIONE_1 != '') {
			def opzione_string = "//span[text()='$OPZIONE_1']/preceding::input[1]"

			def Seleziona_opzione_new = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath',
					'equals', opzione_string, true)

			def open_accordion_string = "//span[text()='$OPZIONE_1']/preceding::input[1]/preceding::div[contains(concat(' ', normalize-space(@class), ' '), ' main-content-box-header main-content-box-header-complex ')][1]/img"

			def open_accordion = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 'equals',
					open_accordion_string, true)

			WebUI.click(open_accordion, FailureHandling.OPTIONAL)

			WebUI.waitForElementVisible(Seleziona_opzione_new, 5, FailureHandling.OPTIONAL)

			WebUI.delay(1)

			try {
				WebUI.click(Seleziona_opzione_new, FailureHandling.OPTIONAL //This will fail as locator is wrong
						)
			}
			catch (Exception ex) {
				WebUI.delay(1)
				lavorazione_file_excel.scrivi_valore_cella(colonna_1, 'OPZIONE NON TROVATA')
				KeywordUtil.markFailedAndStop('Opzione NON Trovata')
			}

			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
					60)
			lavorazione_file_excel.scrivi_valore_cella(colonna_1, 'OK')
		}

		if (OPZIONE_2 != '') {
			def opzione_string = "//span[text()='$OPZIONE_2']/preceding::input[1]"

			def Seleziona_opzione_new = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath',
					'equals', opzione_string, true)

			def open_accordion_string = "//span[text()='$OPZIONE_2']/preceding::input[1]/preceding::div[contains(concat(' ', normalize-space(@class), ' '), ' main-content-box-header main-content-box-header-complex ')][1]/img"

			def open_accordion = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 'equals',
					open_accordion_string, true)

			WebUI.click(open_accordion, FailureHandling.OPTIONAL)

			WebUI.waitForElementVisible(Seleziona_opzione_new, 5, FailureHandling.OPTIONAL)

			WebUI.delay(1)

			try {
				WebUI.click(Seleziona_opzione_new, FailureHandling.OPTIONAL //This will fail as locator is wrong
						)
			}
			catch (Exception ex) {
				WebUI.delay(1)
				lavorazione_file_excel.scrivi_valore_cella(colonna_2, 'OPZIONE NON TROVATA')
				KeywordUtil.markFailedAndStop('Opzione NON Trovata')
			}

			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
					60)
			lavorazione_file_excel.scrivi_valore_cella(colonna_2, 'OK')
		}

		if (OPZIONE_3 != '') {
			def opzione_string = "//span[text()='$OPZIONE_3']/preceding::input[1]"

			def Seleziona_opzione_new = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath',
					'equals', opzione_string, true)

			def open_accordion_string = "//span[text()='$OPZIONE_3']/preceding::input[1]/preceding::div[contains(concat(' ', normalize-space(@class), ' '), ' main-content-box-header main-content-box-header-complex ')][1]/img"

			def open_accordion = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 'equals',
					open_accordion_string, true)

			WebUI.click(open_accordion, FailureHandling.OPTIONAL)

			WebUI.waitForElementVisible(Seleziona_opzione_new, 5, FailureHandling.OPTIONAL)

			WebUI.delay(1)

			try {
				WebUI.click(Seleziona_opzione_new, FailureHandling.OPTIONAL //This will fail as locator is wrong
						)
			}
			catch (Exception ex) {
				WebUI.delay(1)
				lavorazione_file_excel.scrivi_valore_cella(colonna_3, 'OPZIONE NON TROVATA')
				KeywordUtil.markFailedAndStop('Opzione NON Trovata')
			}

			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
					60)
			lavorazione_file_excel.scrivi_valore_cella(colonna_3, 'OK')
		}

		if (OPZIONE_4 != '') {
			def opzione_string = "//span[text()='$OPZIONE_4']/preceding::input[1]"

			def Seleziona_opzione_new = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath',
					'equals', opzione_string, true)

			def open_accordion_string = "//span[text()='$OPZIONE_4']/preceding::input[1]/preceding::div[contains(concat(' ', normalize-space(@class), ' '), ' main-content-box-header main-content-box-header-complex ')][1]/img"

			def open_accordion = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 'equals',
					open_accordion_string, true)

			WebUI.click(open_accordion, FailureHandling.OPTIONAL)

			WebUI.waitForElementVisible(Seleziona_opzione_new, 5, FailureHandling.OPTIONAL)

			WebUI.delay(1)

			try {
				WebUI.click(Seleziona_opzione_new, FailureHandling.OPTIONAL //This will fail as locator is wrong
						)
			}
			catch (Exception ex) {
				WebUI.delay(1)
				lavorazione_file_excel.scrivi_valore_cella(colonna_4, 'OPZIONE NON TROVATA')
				KeywordUtil.markFailedAndStop('Opzione NON Trovata')
			}

			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
					60)
			lavorazione_file_excel.scrivi_valore_cella(colonna_4, 'OK')
		}

		if (OPZIONE_5 != '') {
			def opzione_string = "//span[text()='$OPZIONE_5']/preceding::input[1]"

			def Seleziona_opzione_new = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath',
					'equals', opzione_string, true)

			def open_accordion_string = "//span[text()='$OPZIONE_5']/preceding::input[1]/preceding::div[contains(concat(' ', normalize-space(@class), ' '), ' main-content-box-header main-content-box-header-complex ')][1]/img"

			def open_accordion = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 'equals',
					open_accordion_string, true)

			WebUI.click(open_accordion, FailureHandling.OPTIONAL)

			WebUI.waitForElementVisible(Seleziona_opzione_new, 5, FailureHandling.OPTIONAL)

			WebUI.delay(1)

			try {
				WebUI.click(Seleziona_opzione_new, FailureHandling.OPTIONAL //This will fail as locator is wrong
						)
			}
			catch (Exception ex) {
				WebUI.delay(1)
				lavorazione_file_excel.scrivi_valore_cella(colonna_5, 'OPZIONE NON TROVATA')
				KeywordUtil.markFailedAndStop('Opzione NON Trovata')
			}

			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
					60)
			lavorazione_file_excel.scrivi_valore_cella(colonna_5, 'OK')
		}
	}
}