package com.at.util
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable



import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class Canale {
	/**
	 * seleziona il canale
	 */
	@Keyword
	def Seleziona_Canale(CANALE) {

		def row_canale
		def string_id_file_canali='Data Files/FILE_EXCEL_SUPPORTO/CANALI'
		def max_row_num_canale = findTestData(string_id_file_canali).getRowNumbers()

		for (def rowNum_canale = 1; rowNum_canale <= max_row_num_canale; rowNum_canale++) {

			def can = findTestData(string_id_file_canali).getValue(1, rowNum_canale)
			if (can == CANALE) {

				row_canale = rowNum_canale
				break
			}
		}

		return row_canale
	}

	/**
	 * verifica se il canale è a distanza o meno
	 */
	@Keyword
	def Canale_a_distanza(CANALE) {

		def canale_distanza
		def string_id_file_canali='Data Files/FILE_EXCEL_SUPPORTO/CANALI'
		def max_row_num_canale = findTestData(string_id_file_canali).getRowNumbers()

		for (def rowNum_canale = 1; rowNum_canale <= max_row_num_canale; rowNum_canale++) {

			def can = findTestData(string_id_file_canali).getValue(1, rowNum_canale)
			if (can == CANALE) {

				canale_distanza=findTestData(string_id_file_canali).getValue(8, rowNum_canale)
				break
			}
		}

		return canale_distanza
	}
}