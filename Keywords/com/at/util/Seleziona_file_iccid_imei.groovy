package com.at.util
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable



import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class Seleziona_file_iccid_imei {
	/**
	 * seleziona i file relativi agli iccid e imei da usare
	 */
	@Keyword
	def List<String> recupera_file_iccid_imei(CATENA, SEGMENTO) {

		def iccid_3A_3C
		def FILE_IMEI
		def FILE_ICCID
		def List<String> file

		switch (CATENA) {
			case '3A':
				iccid_3A_3C = 1

				switch (SEGMENTO) {
					case 'RES':
						FILE_IMEI = 'Data Files/FILE_EXCEL_SUPPORTO/IMEI_RES_3A'
						FILE_ICCID= 'Data Files/FILE_EXCEL_SUPPORTO/ICCID_RES'
						break

					case 'SHP':
						FILE_IMEI = 'Data Files/FILE_EXCEL_SUPPORTO/IMEI_SHP_3A'
						FILE_ICCID= 'Data Files/FILE_EXCEL_SUPPORTO/ICCID_SHP'
						break
				}
				break

			case '3C':
				iccid_3A_3C = 2

				switch (SEGMENTO) {
					case 'RES':
						FILE_IMEI = 'Data Files/FILE_EXCEL_SUPPORTO/IMEI_RES_3C'
						FILE_ICCID= 'Data Files/FILE_EXCEL_SUPPORTO/ICCID_RES'
						break

					case 'SHP':
						FILE_IMEI = 'Data Files/FILE_EXCEL_SUPPORTO/IMEI_SHP_3C'
						FILE_ICCID= 'Data Files/FILE_EXCEL_SUPPORTO/ICCID_SHP'
						break
				}
				break
		}

		file = [
			iccid_3A_3C,
			FILE_IMEI,
			FILE_ICCID
		]
		return file
	}
}