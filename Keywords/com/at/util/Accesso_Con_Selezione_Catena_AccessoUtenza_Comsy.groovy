package com.at.util
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable


import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class Accesso_Con_Selezione_Catena_AccessoUtenza_Comsy {
	/**
	 * 
	 */
	@Keyword
	def Selezione_Catena(CATENA,row_canale) {

		def string_id_file_canali ='Data Files/FILE_EXCEL_SUPPORTO/CANALI'

		switch (CATENA) {
			case '3A':
				WebUI.navigateToUrl('https://test.salesforce.com/?locale=it')

				WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Nome utente_username'), 'm.candiani@reply.it.qafull3a')

				WebUI.setEncryptedText(findTestObject('Page_Accedi  Salesforce/input_Password_pw'), 'iBox6NXRWvUTXBq0Xh7Cnw==')

				WebUI.click(findTestObject('Page_Accedi  Salesforce/input_Password_Login'))

				WebUI.delay(2)

				WebUI.navigateToUrl(findTestData(string_id_file_canali).getValue(3, row_canale))

				break
			case '3C':
				WebUI.navigateToUrl('https://test.salesforce.com/?locale=it')

				WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Nome utente_username'), 'm.candiani@reply.it.qafull3c')

				WebUI.setEncryptedText(findTestObject('Page_Accedi  Salesforce/input_Password_pw'), 'iBox6NXRWvUTXBq0Xh7Cnw==')

				WebUI.click(findTestObject('Page_Accedi  Salesforce/input_Password_Login'))

				WebUI.delay(2)

				WebUI.navigateToUrl(findTestData(string_id_file_canali).getValue(4, row_canale))

				break
		}
	}

	@Keyword
	def Selezione_AccessoUtenza(row_canale) {

		def string_id_file_canali ='Data Files/FILE_EXCEL_SUPPORTO/CANALI'
		def ACCESSO_UTENZA = findTestData(string_id_file_canali).getValue(7, row_canale)

		switch (ACCESSO_UTENZA) {
			case 'COMMUNITY':
				WebUI.click(findTestObject('Page_Salesforce - Performance Editi/div_Manage External User'))

				WebUI.click(findTestObject('Page_Salesforce - Performance Editi/a_Log in to Community as User'))

				WebUI.click(findTestObject('Page_Partner Community  Partner Sal/a_Inserisci Ordine'))

				break
			case 'NO_COMMUNITY':
				WebUI.click(findTestObject('Page_Salesforce - Performance Editi/CLICK_Login_Utenza_NO_COMMUNITY'))

				WebUI.click(findTestObject('Page_Salesforce - Performance Editi/CLICK_Utenza_NO_COMMUNITY_Inserisci_Ordine'))

				break
		}
	}



	@Keyword
	def Selezione_Comsy(SEGMENTO,COMSY,row_canale) {

		def string_id_file_canali ='Data Files/FILE_EXCEL_SUPPORTO/CANALI'
		WebUI.waitForElementVisible(findTestObject('Object Repository/Object_MOBILE/WAIT-COMSY'), 60)
		WebUI.delay(1, FailureHandling.STOP_ON_FAILURE)

		switch (SEGMENTO) {
			case 'RES':

				if (COMSY == '') {
					WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), findTestData(string_id_file_canali).getValue(5, row_canale),
							FailureHandling.OPTIONAL)
				} else {

					WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), COMSY, FailureHandling.OPTIONAL)
				}

				break
			case 'SHP':

				if (COMSY == '') {
					WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), findTestData(string_id_file_canali).getValue(6, row_canale),
							FailureHandling.OPTIONAL)
				} else {

					WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), COMSY, FailureHandling.OPTIONAL)
				}

				break
		}

		WebUI.delay(1, FailureHandling.STOP_ON_FAILURE)
		WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/Selezione_comsy'), FailureHandling.OPTIONAL)
	}
}
