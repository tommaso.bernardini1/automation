package com.at.util

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable as GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

import java.io.FileInputStream as FileInputStream
import java.io.FileOutputStream as FileOutputStream
import java.io.FileNotFoundException as FileNotFoundException
import java.io.IOException as IOException
import java.util.Date as Date
import org.apache.poi.hssf.usermodel.HSSFCell as HSSFCell
import org.apache.poi.hssf.usermodel.HSSFCellStyle as HSSFCellStyle
import org.apache.poi.hssf.usermodel.HSSFDataFormat as HSSFDataFormat
import org.apache.poi.hssf.usermodel.HSSFRow as HSSFRow
import org.apache.poi.hssf.usermodel.HSSFSheet as HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook as HSSFWorkbook
import org.apache.poi.hssf.util.HSSFColor as HSSFColor
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import org.apache.poi.openxml4j.exceptions.InvalidFormatException as InvalidFormatException
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.ss.usermodel.Workbook as Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory as WorkbookFactory
import org.apache.poi.ss.usermodel.Sheet as Sheet
import com.kms.katalon.core.exception.StepErrorException as StepErrorException
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.logging.KeywordLogger




class Lavorazione_file_excel {
	/**
	 * Seleziona riga da aggiornare nell'excel
	 */
	@Keyword
	def calcola_riga_cliente(CODICE_FISCALE_PARTITA_IVA, max_row_num_cliente, column) {

		for (def rowNum_cliente = 1; rowNum_cliente <= max_row_num_cliente; rowNum_cliente++) {
			def cliente = findTestData(GlobalVariable.STRING_FILE).getValue(column, rowNum_cliente)
			if (cliente == CODICE_FISCALE_PARTITA_IVA) {
				GlobalVariable.ROW_CLIENTE = rowNum_cliente
				break
			}
		}
	}



	/**
	 * Seleziona il terminale
	 */
	@Keyword
	def scrivi_valore_cella(colonna, testo) {

		def URL_EXCEL_INPUT = findTestData(GlobalVariable.STRING_FILE).getSourceUrl()
		def fis_excel_input = new FileInputStream(URL_EXCEL_INPUT)
		def workbook_excel_input = WorkbookFactory.create(fis_excel_input)
		def sheet = workbook_excel_input.getSheetAt(0)
		def Row row = sheet.getRow(GlobalVariable.ROW_CLIENTE)
		def Cell cell = row.getCell(colonna-1)
		cell.setCellValue(testo)
		fis_excel_input.close()
		def fos_excel_input = new FileOutputStream(URL_EXCEL_INPUT)
		workbook_excel_input.write(fos_excel_input)
		fos_excel_input.close()
	}




	/**
	 * Selezione mnp ed eventualmente la tcr
	 */
	@Keyword
	def Seleziona_mnp_tcr(RICHIESTA_TCR) {
	}


	/**
	 * inserimento dati ICCID
	 */
	@Keyword
	def Inserimento_ICCID(iccid_3A_3C, FILE_ICCID) {

		def row_iccid

		WebUI.waitForElementVisible(findTestObject('Object_MOBILE/input_ICCID'), 60)

		def max_row_num_iccid = findTestData(FILE_ICCID).getRowNumbers()

		for (def rowNum_iccid = 1; rowNum_iccid <= max_row_num_iccid; rowNum_iccid++) {
			def iccid = findTestData(FILE_ICCID).getValue(iccid_3A_3C, rowNum_iccid)

			if (iccid != 'X') {
				row_iccid = rowNum_iccid

				def ICCID = findTestData(FILE_ICCID).getValue(iccid_3A_3C, row_iccid)

				WebUI.setText(findTestObject('Object_MOBILE/input_ICCID'), ICCID)

				WebUI.waitForElementAttributeValue(findTestObject('Object_MOBILE/WAIT-ICCID'), 'class', 'loading-div ng-hide',
						60)

				WebUI.delay(1)

				def msisdn = WebUI.getAttribute(findTestObject('Object_MOBILE/input_MSISDN'), 'value')

				if (msisdn != '') {
					break
				} else {
					def URL_iccd = findTestData(FILE_ICCID).getSourceUrl()

					String filePath = URL_iccd

					Workbook workbook

					FileInputStream fis

					FileOutputStream fos

					fis = new FileInputStream(filePath)

					workbook = WorkbookFactory.create(fis)

					def sheet = workbook.getSheetAt(0)

					Row row = sheet.getRow(row_iccid)

					Cell cell = row.getCell(iccid_3A_3C - 1)

					cell.setCellValue('X')

					fis.close()

					fos = new FileOutputStream(filePath)

					workbook.write(fos)

					fos.close()
				}
			}
		}

		return row_iccid
	}


	/**
	 * Configurazione dati mnp
	 */
	@Keyword
	def Configura_MNP(NOME, COGNOME, CODICE_FISCALE, NUMERO_DOCUMENTO, MNP_MSISDN_PORTATO, MNP_CONTRATTO_ORIGINE, MNP_OPERATORE_TELEFONICO, MNP_ICCID_DONATING_13_cifre) {
	}


	/**
	 * Configurazione imei
	 */
	@Keyword
	def Configura_mei(IMEI, FILE_IMEI, TERMINALE ) {

		def URL_imei = findTestData(FILE_IMEI).getSourceUrl()
		String filePath = URL_imei
		Workbook workbook
		FileInputStream fis
		FileOutputStream fos
		fis = new FileInputStream(filePath)
		workbook = WorkbookFactory.create(fis)
		def sheet = workbook.getSheetAt(0)

		if (IMEI == '') {

			def row_terminale
			int col_imei
			def imei

			def max_row_num_terminale = findTestData(FILE_IMEI).getRowNumbers()

			for (def rowNum_terminale = 1; rowNum_terminale <= max_row_num_terminale; rowNum_terminale++) {
				def terminale = findTestData(FILE_IMEI).getValue(1, rowNum_terminale)

				if (terminale == TERMINALE) {
					row_terminale = rowNum_terminale

					break
				}
			}

			def colNum_imei_partenza = findTestData(FILE_IMEI).getValue(5, row_terminale)

			int colNum_imei_partenza_intero = Integer.parseInt(colNum_imei_partenza)

			def tasto_procedi

			for (col_imei = colNum_imei_partenza_intero; col_imei <= 4; col_imei++) {
				if (tasto_procedi == 'disabled') {
					break
				}

				imei = findTestData(FILE_IMEI).getValue(col_imei, row_terminale)

				if (imei == 'X') {
					//
				} else {
					WebUI.setText(findTestObject('Object_MOBILE/input_IMEI'), imei)

					WebUI.waitForElementAttributeValue(findTestObject('Object_MOBILE/WAIT-IMEI'), 'class', 'loading-div ng-hide',
							60)

					tasto_procedi = WebUI.getAttribute(findTestObject('Object_MOBILE/input_IMEI'), 'disabled')

					if (tasto_procedi) {
						break
					} else {

						Row row = sheet.getRow(row_terminale)

						Cell cell = row.getCell(col_imei - 1)

						cell.setCellValue('X')
					}
				}
			}

			// aggiorno la colonna di partenza imei per il prossimo test

			Row row = sheet.getRow(row_terminale)

			Cell cell = row.getCell(4)

			cell.setCellValue(col_imei + 1)

		} else {

			WebUI.setText(findTestObject('Object_MOBILE/input_IMEI'), IMEI)

			WebUI.waitForElementAttributeValue(findTestObject('Object_MOBILE/WAIT-IMEI'), 'class', 'loading-div ng-hide', 60)
		}



		fis.close()
		fos = new FileOutputStream(filePath)
		workbook.write(fos)
		fos.close()
	}
}