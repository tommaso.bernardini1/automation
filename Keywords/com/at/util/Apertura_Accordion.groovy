package com.at.util
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable



import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.at.util.ScollElement as ScrollElement


class Apertura_Accordion {
	/**
	 * Apertura in sequenz adi tutti gli accordion
	 */
	@Keyword
	def AperturaTuttigliAccordion() {

		WebDriver driver = DriverFactory.getWebDriver()

		def click = new ClickUsingJS()

		def total_accordion = (driver.findElements(By.xpath("//div[contains(@class,'main-content-box-header main-content-box-header-complex')]")).size())

		def title_accordion = WebUI.modifyObjectProperty(findTestObject('Object Repository/Page_Coupon/Open_Accordion_Scroll_By_Index'), 'xpath', 'equals',
				"//a[@class='box-header-title']", true)

		//WebUI.scrollToElement(title_accordion, 2, FailureHandling.CONTINUE_ON_FAILURE)

		ScrollElement.scrolltoElement(title_accordion, 10)

		WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

		//WebUI.scrollToElement(findTestObject('Page_Coupon/Open_Accordion_COUPON'), 2, FailureHandling.CONTINUE_ON_FAILURE)

		//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_COUPON'), FailureHandling.CONTINUE_ON_FAILURE)
		click.clickUsingJS(findTestObject('Page_Coupon/Open_Accordion_COUPON'), 60)
		//WebUI.click(findTestObject('Page_Coupon/Open_Accordion_COUPON'), FailureHandling.CONTINUE_ON_FAILURE)

		WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)



		for (def num_accordion = 1; num_accordion <= total_accordion; num_accordion++) {

			//def accordion_scroll_string = "//div[contains(@class,'main-content-box-header main-content-box-header-complex')][$num_accordion]"

			//def accordion_scroll = WebUI.modifyObjectProperty(findTestObject('Object Repository/Page_Coupon/Open_Accordion_Scroll_By_Index'), 'xpath', 'equals',
			//	accordion_scroll_string, true)

			def accordion_string = "//div[contains(@class,'main-content-box-header main-content-box-header-complex')][$num_accordion]/img"

			def Seleziona_accordion = WebUI.modifyObjectProperty(findTestObject('Object Repository/Page_Coupon/Open_Accordion_By_Index'), 'xpath', 'equals',
					accordion_string, true)

			WebUI.waitForElementClickable(Seleziona_accordion, 60)

			ScrollElement.scrolltoElement(Seleziona_accordion, 2)

			click.clickUsingJS(Seleziona_accordion, 60)

			//WebUI.click(Seleziona_accordion, FailureHandling.CONTINUE_ON_FAILURE)

			WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

		}

	}
}