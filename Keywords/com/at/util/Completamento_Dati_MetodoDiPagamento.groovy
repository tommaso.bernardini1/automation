package com.at.util
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable


import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException


class Completamento_Dati_MetodoDiPagamento {
	/**
	 * completamento dei dati in Fase 2 relativi al metodo di pagamento inserito in Fase 1
	 */
	@Keyword
	def Inserimento_Dati_Pagamento_RES(METODO_PAGAMENTO, CIRCUITO, NUMERO_CARTA_CONTO) {

		switch (METODO_PAGAMENTO) {
			case 'CARTA DI CREDITO':
				WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/select_Visa (1)'), 5)

				WebUI.selectOptionByValue(findTestObject('Page_Fastweb - Abbonamento Online/select_Visa (1)'), CIRCUITO, true)

				WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero Carta_section3_nu'), NUMERO_CARTA_CONTO)

				WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Data di Scadenza_section (1)'), '12/22')

				WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Pagamento (1)'),
						60)

				WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Pagamento (1)'), 5)

				WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Pagamento (1)'))

				break

			case 'CONTO CORRENTE':
				WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Dati Clienti'), 5)

				WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Dati Clienti'))

				WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Dati Clienti'))

				WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
						60)

			// scroll su Completamento Dati Banca
				WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero conto corrente'), 5)

				WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero conto corrente'))

				WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero conto corrente'), NUMERO_CARTA_CONTO)

				break

			case 'BOLLETTINO POSTALE':

				break
		}
	}


	@Keyword
	def Inserimento_Dati_Pagamento_SHP(METODO_PAGAMENTO, CIRCUITO, NUMERO_CARTA_CONTO, IBAN_CDC_INTESTATO_AZIENDA) {

		def click = new ClickUsingJS()

		switch (METODO_PAGAMENTO) {
			case 'CARTA DI CREDITO':

				switch (IBAN_CDC_INTESTATO_AZIENDA) {
					case 'SI':
					click.clickUsingJS(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_SI_Carta di Credito Intesta Azienda'), 60)
					break

					case 'NO':
					click.clickUsingJS(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_NO_Carta di Credito Intesta Azienda'), 60)
					break
				}
				WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/select_Visa_SHP'), 5)

				WebUI.selectOptionByValue(findTestObject('Page_Fastweb - Abbonamento Online/select_Visa_SHP'), CIRCUITO, true)

				WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero Carta_SHP'), NUMERO_CARTA_CONTO)

				WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Data di Scadenza_SHP'), '12/22')

				WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Pagamento_SHP'),
						60)

				WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Pagamento_SHP'), 5)

				click.clickUsingJS(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Pagamento_SHP'), 60)

				WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
						60)

				break

			case 'CONTO CORRENTE':

				switch (IBAN_CDC_INTESTATO_AZIENDA) {
					case 'SI':
					click.clickUsingJS(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_SI_Conto Corrente Intestato Azienda'), 60)
					break

					case 'NO':
					click.clickUsingJS(findTestObject('Object Repository/Page_Fastweb - Abbonamento Online/input_NO_Conto Corrente Intestato Azienda'), 60)
					break
				}

				WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Dati Firmatario'), 5)

				WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Dati Firmatario'))

				click.clickUsingJS(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Dati Firmatario'), 60)

				WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
						60)

			// scroll su Completamento Dati Banca
				WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero conto corrente'), 5)

				WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero conto corrente'))

				WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero conto corrente'), NUMERO_CARTA_CONTO)

				WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
						60)

				break

			case 'BOLLETTINO POSTALE':

				break
		}
	}
}