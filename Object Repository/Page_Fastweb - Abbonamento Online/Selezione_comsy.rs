<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Selezione_comsy</name>
   <tag></tag>
   <elementGuidId>eda392ac-84fe-41e2-8a32-2370e0c2459c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[(text() = 'DR.0563.0001HQ' or . = 'DR.0563.0001HQ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@class='table table-striped table-condensed table-hover']/tbody/tr[1]/td[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-center ng-binding</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>DR.0563.0001HQ</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;accordiongroup-215-1424-panel&quot;)/div[@class=&quot;panel-body&quot;]/div[@class=&quot;modal-body ng-scope&quot;]/table[@class=&quot;table table-striped table-condensed table-hover&quot;]/tbody[1]/tr[@class=&quot;ng-scope&quot;]/td[@class=&quot;text-center ng-binding&quot;]</value>
   </webElementProperties>
</WebElementEntity>
