<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Copia Dati Firmatario</name>
   <tag></tag>
   <elementGuidId>f258bc0b-2b38-4834-a943-17ac9d32d072</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='btn btn-primary' and @ng-click = 'copiaDatiFirmatario()' and @type = 'button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>verificaCartaDiCredito('section3', 'Form2', 'section3_numeroCarta', 'section3_dataDiScadenza', 'section3_circuitoCarta')</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-disabled</name>
      <type>Main</type>
      <value>cpqOrderContext.pageDataWrapper.pagamentoVerificato &amp;&amp; !hasToChangePaymentMethod()</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-show</name>
      <type>Main</type>
      <value>!onlyCheckMopSky()</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            Verifica Pagamento
                        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;accordiongroup-103-396-panel&quot;)/div[@class=&quot;panel-body&quot;]/fieldset[@id=&quot;phase2&quot;]/div[@id=&quot;datiDiCompletamentoSDD&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-4&quot;]/button[@class=&quot;btn btn-primary&quot;]</value>
   </webElementProperties>
</WebElementEntity>
