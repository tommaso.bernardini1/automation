<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Open_Accordion_Scroll_By_Index</name>
   <tag></tag>
   <elementGuidId>eb563988-5279-4517-a2d5-ce385fbd376c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='main-content-box-header main-content-box-header-complex'][1]/img</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class,'main-content-box-header main-content-box-header-complex')][1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class,'main-content-box-header main-content-box-header-complex')][1]</value>
   </webElementProperties>
</WebElementEntity>
