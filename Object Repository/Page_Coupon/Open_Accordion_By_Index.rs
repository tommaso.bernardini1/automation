<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Open_Accordion_By_Index</name>
   <tag></tag>
   <elementGuidId>ea19d8fc-3ede-4cfa-a3e5-a7c5b4bed1b6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='main-content-box-header main-content-box-header-complex'][1]/img</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class,'main-content-box-header main-content-box-header-complex')][1]/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class,'main-content-box-header main-content-box-header-complex')][1]/img</value>
   </webElementProperties>
</WebElementEntity>
