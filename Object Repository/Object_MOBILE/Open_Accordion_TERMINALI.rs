<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Open_Accordion_TERMINALI</name>
   <tag></tag>
   <elementGuidId>8197ad5a-4664-4743-a43c-7f2678a3d32e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[text()[normalize-space() ='Terminali']]/img</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//img[(text() = '
    
                                        Opzioni
                                    ' or . = '
    
                                        Opzioni
                                    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
                                        Opzioni
                                    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
</WebElementEntity>
