import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.io.FileInputStream as FileInputStream
import java.io.FileOutputStream as FileOutputStream
import java.io.FileNotFoundException as FileNotFoundException
import java.io.IOException as IOException
import java.util.Date as Date
import org.apache.poi.hssf.usermodel.HSSFCell as HSSFCell
import org.apache.poi.hssf.usermodel.HSSFCellStyle as HSSFCellStyle
import org.apache.poi.hssf.usermodel.HSSFDataFormat as HSSFDataFormat
import org.apache.poi.hssf.usermodel.HSSFRow as HSSFRow
import org.apache.poi.hssf.usermodel.HSSFSheet as HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook as HSSFWorkbook
import org.apache.poi.hssf.util.HSSFColor as HSSFColor
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import org.apache.poi.openxml4j.exceptions.InvalidFormatException as InvalidFormatException
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.ss.usermodel.Workbook as Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory as WorkbookFactory
import org.apache.poi.ss.usermodel.Sheet as Sheet
import com.kms.katalon.core.exception.StepErrorException as StepErrorException
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.By as By
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject


WebUI.openBrowser('')

//WebUI.maximizeWindow()
def row_canale

def max_row_num_canale = findTestData('CANALI').getRowNumbers()

for (def rowNum_canale = 1; rowNum_canale <= max_row_num_canale; rowNum_canale++) {
    def can = findTestData('CANALI').getValue(1, rowNum_canale)

    if (can == CANALE) {
        row_canale = rowNum_canale

        break
    }
}

def CANALE_DISTANZA = findTestData('CANALI').getValue(8, row_canale)

//---------------------------------------
def iccid_3A_3C
def FILE_IMEI

switch (CATENA) {
    case '3A':
        iccid_3A_3C = 1
		FILE_IMEI = 'IMEI_3A'

        break
    case '3C':
        iccid_3A_3C = 2
		iccid_3A_3C = 'IMEI_3C'

        break
}


//------------------------------------------
switch (CATENA) {
    case '3A':
        WebUI.navigateToUrl('https://test.salesforce.com/?locale=it')

        WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Nome utente_username'), 'm.candiani@reply.com.qafull3a')

        WebUI.setEncryptedText(findTestObject('Page_Accedi  Salesforce/input_Password_pw'), 'iBox6NXRWvUTXBq0Xh7Cnw==')

        WebUI.click(findTestObject('Page_Accedi  Salesforce/input_Password_Login'))

        WebUI.delay(2)

        WebUI.navigateToUrl(findTestData('CANALI').getValue(3, row_canale))

        break
    case '3C':
        WebUI.navigateToUrl('https://test.salesforce.com/?locale=it')

        WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Nome utente_username'), 'm.candiani@reply.it.qafull3c')

        WebUI.setEncryptedText(findTestObject('Page_Accedi  Salesforce/input_Password_pw'), 'iBox6NXRWvUTXBq0Xh7Cnw==')

        WebUI.click(findTestObject('Page_Accedi  Salesforce/input_Password_Login'))

        WebUI.delay(2)

        WebUI.navigateToUrl(findTestData('CANALI').getValue(4, row_canale))

        break
}

def ACCESSO_UTENZA = findTestData('CANALI').getValue(7, row_canale)

switch (ACCESSO_UTENZA) {
    case 'COMMUNITY':
        WebUI.click(findTestObject('Page_Salesforce - Performance Editi/div_Manage External User'))

        WebUI.click(findTestObject('Page_Salesforce - Performance Editi/a_Log in to Community as User'))

        WebUI.click(findTestObject('Page_Partner Community  Partner Sal/a_Inserisci Ordine'))

        break
    case 'NO_COMMUNITY':
        WebUI.click(findTestObject('Page_Salesforce - Performance Editi/CLICK_Login_Utenza_NO_COMMUNITY'))

        WebUI.click(findTestObject('Page_Salesforce - Performance Editi/CLICK_Utenza_NO_COMMUNITY_Inserisci_Ordine'))

        break
}

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

switch (SEGMENTO) {
    case 'RES':
        WebUI.waitForElementPresent(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), 60, FailureHandling.STOP_ON_FAILURE)

		if (COMSY == '') {
			        WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), findTestData('CANALI').getValue(5, row_canale), 
            FailureHandling.OPTIONAL)
		} else {
		
        WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), COMSY, FailureHandling.OPTIONAL)

		}
		
        break
    case 'SHP':
        WebUI.waitForElementPresent(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), 60, FailureHandling.STOP_ON_FAILURE)

		if (COMSY == '') {
			        WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), findTestData('CANALI').getValue(6, row_canale), 
            FailureHandling.OPTIONAL)
		} else {
		
        WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), COMSY, FailureHandling.OPTIONAL)

		}

        break
}

WebUI.delay(1, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/Selezione_comsy'), FailureHandling.OPTIONAL)

//--------------------------------------------------------INIZIO PARTE ANAGRAFICA------------------------------------------------


switch (SEGMENTO) {
    case 'RES':

	CustomKeywords.'com.at.util.Anagrafica_RES.Inserimento_Anagrafica_RES'(NOME, COGNOME, SESSO, DATA_DI_NASCITA, NAZIONE_DI_NASCITA, COMUNE_DI_NASCITA, NUMERO_MOBILE, EMAIL, CODICE_FISCALE, TIPOLOGIA_OFFERTA)
	
        break
    case 'SHP':
	
	CustomKeywords.'com.at.util.Anagrafica_RES.Inserimento_Anagrafica_SHP'(RAGIONE_SOCIALE, FORMA_GIURIDICA, NOME_RAPPRESENTANTE_LEGALE, COGNOME_RAPPRESENTANTE_LEGALE, SESSO_RAPPRESENTANTE_LEGALE, DATA_DI_NASCITA_RAPPRESENTANTE_LEGALE,
		NAZIONE_DI_NASCITA_RAPPRESENTANTE_LEGALE, COMUNE_DI_NASCITA_RAPPRESENTANTE_LEGALE, NUMERO_MOBILE, EMAIL, CODICE_FISCALE_RAPPRESENTANTE_LEGALE, PARTITA_IVA, TIPOLOGIA_OFFERTA)

	   	break
}


//-----------------------------------------------FINE DATI ANAGRAFICI--------------------------------------------------------


//-----------------------------------------------INIZIO INDIRIZZO DI ATTIVAZIONE e RESIDENZA-----------------------------------------------


CustomKeywords.'com.at.util.Inserimento_IndirizzoAttivazione_e_IndirizzoResidenza.Inserimento_Ind_Attivazione_Ind_Residenza'(TIPOLOGIA_OFFERTA, TECNOLOGIA, SEGMENTO, CITTA_DI_RESIDENZA, INDIRIZZO_DI_RESIDENZA, CIVICO_DI_RESIDENZA, 
		CITTA_DI_RESIDENZA_SEDE_LEGALE, INDIRIZZO_DI_RESIDENZA_SEDE_LEGALE, CIVICO_DI_RESIDENZA_SEDE_LEGALE)


//-----------------------------------------------FINE INDIRIZZO DI ATTIVAZIONE e RESIDENZA-----------------------------------------------


//------------------------------------------------------------------INIZIO SEZIONE SELEZIONE DATI DI PAGAMENTO-------------------------------------------------------------


CustomKeywords.'com.at.util.Selezione_Metodo_di_Pagamento.Inserimento_Ind_Attivazione_Ind_Residenza'(TIPOLOGIA_OFFERTA, METODO_PAGAMENTO)


WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

//--------------------------------------------------------------------FINE SEZIONE SELEZIONE DATI DI PAGAMENTO-----------------------------------------------------------------


WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Vai al Carrello (1)'))


//--------------------------------------------------------------------INIZIO ESPLORA CATALOGO---------------------------------------------------

def row_cliente
def max_row_num_cliente = findTestData('EXPLORA_CATALOGO_OFFERTE').getRowNumbers()

for (def rowNum_cliente = 1; rowNum_cliente <= max_row_num_cliente; rowNum_cliente++) {
	def cliente = findTestData('EXPLORA_CATALOGO_OFFERTE').getValue(8, rowNum_cliente)

	if (cliente == CODICE_FISCALE) {
		row_cliente = rowNum_cliente

		break
	}
}

def int col_esplora
def max_col = findTestData('EXPLORA_CATALOGO_OFFERTE').getColumnNumbers()

for (def colNum = 1; colNum <= max_col; colNum++) {
	def name_col = findTestData('EXPLORA_CATALOGO_OFFERTE').getValue(colNum, 1)

	if (name_col == 'X') {
		col_esplora = colNum

		break
	}
}

	URL_ESPLORA_CATALOGO_MOBILE = findTestData('EXPLORA_CATALOGO_OFFERTE').getSourceUrl()

	String filePath = URL_ESPLORA_CATALOGO_MOBILE
	Workbook workbook
	FileInputStream fis
	FileOutputStream fos
	fis = new FileInputStream(filePath)
	workbook = WorkbookFactory.create(fis)
	sheet = workbook.getSheetAt(0)
	Row row = sheet.getRow(row_cliente)
	Cell cell
	
	def colonna=col_esplora
	def total_rows
	
	WebDriver driver = DriverFactory.getWebDriver()

switch (TIPOLOGIA_OFFERTA) {


		
	case 'MOBILE':
	//--------------------------------------prima le offerte mobile voce----------------------------------------	
	
			WebUI.waitForElementVisible(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'), 60)	
			WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'), 60)	
			WebUI.focus(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'))	
			WebUI.click(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'))
	
			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
				60)
			
			total_rows = driver.findElements(By.xpath("//table[@class='catalogItems-table productsTableOrd']/tbody/tr/td[2]/span")).size()
			
			def topolino=colonna

			cell = row.getCell(colonna)
			cell.setCellValue('OFFERTE MOBILE VOCE:')  
			
			if(total_rows>0) {
							
				for(int numberOfRows=1; numberOfRows<=total_rows; numberOfRows++) {
					def OFFERTA_vendibile=''				

					def desc_carrello_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[2]/span", true)				
					def desc_carrello= WebUI.getText(desc_carrello_OBJ)				

					def costo_una_tantum_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[3]/span[1]", true)			
					def costo_una_tantum= WebUI.getText(costo_una_tantum_OBJ)				

					def costo_ricorrente_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[4]/span[1]", true)				
					def costo_ricorrente= WebUI.getText(costo_ricorrente_OBJ)				
					
					def costo_ricorrente_frequenza_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[4]/span[2]", true)	
					def costo_ricorrente_frequenza= WebUI.getText(costo_ricorrente_frequenza_OBJ)
				
					OFFERTA_vendibile= desc_carrello + ' / ' + costo_una_tantum + ' / ' + costo_ricorrente + '/' + costo_ricorrente_frequenza
				
					colonna=colonna+1
					cell = row.getCell(colonna)
					cell.setCellValue(OFFERTA_vendibile)
							
				}
			
			} else {
			
			colonna=colonna+1
			cell = row.getCell(colonna)
			cell.setCellValue('NESSUNA OFFERTA TROVATA')
			
			}
			
			//--------------------------------------------------offerte mobili dati-----------------------------------
				
			WebUI.waitForElementVisible(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'), 60)			
			WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'), 60)
			WebUI.focus(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'))
			WebUI.click(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'))
			
			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
				60)
			
			total_rows = driver.findElements(By.xpath("//table[@class='catalogItems-table productsTableOrd']/tbody/tr/td[2]/span")).size()
			
			colonna=colonna+1
			cell = row.getCell(colonna)
			cell.setCellValue('OFFERTE MOBILE DATI:')
			
			if(total_rows>0) {
				
				for(int numberOfRows=1; numberOfRows<=total_rows; numberOfRows++) {
					def OFFERTA_vendibile=''

					def desc_carrello_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[2]/span", true)
					def desc_carrello= WebUI.getText(desc_carrello_OBJ)

					def costo_una_tantum_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[3]/span[1]", true)
					def costo_una_tantum= WebUI.getText(costo_una_tantum_OBJ)

					def costo_ricorrente_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[4]/span[1]", true)
					def costo_ricorrente= WebUI.getText(costo_ricorrente_OBJ)
		
					def costo_ricorrente_frequenza_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[4]/span[2]", true)
					def costo_ricorrente_frequenza= WebUI.getText(costo_ricorrente_frequenza_OBJ)
	
					OFFERTA_vendibile= desc_carrello + ' / ' + costo_una_tantum + ' / ' + costo_ricorrente + '/' + costo_ricorrente_frequenza
	
					colonna=colonna+1
					cell = row.getCell(colonna)
					cell.setCellValue(OFFERTA_vendibile)
				
				}

				} else {

				colonna=colonna+1
				cell = row.getCell(colonna)
				cell.setCellValue('NESSUNA OFFERTA TROVATA')

			}
			
			
			fis.close()			 
			fos = new FileOutputStream(filePath)			 
			workbook.write(fos)			 
			fos.close()
		
			break
			
//-------------------------------------------------------offerte fisse-------------------------------------------------------------		
		case 'FISSO':
		
			WebUI.waitForElementVisible(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_FISSO'), 60)
			WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_FISSO'), 60)
			WebUI.focus(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_FISSO'))
			WebUI.click(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_FISSO'))
			
			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
				60)
			
			total_rows = driver.findElements(By.xpath("//table[@class='catalogItems-table productsTableOrd']/tbody/tr/td[2]/span")).size()
			
			cell = row.getCell(colonna)
			cell.setCellValue('OFFERTE FISSE:')
			
			if(total_rows>0) {
				
				for(int numberOfRows=1; numberOfRows<=total_rows; numberOfRows++) {
					def OFFERTA_vendibile=''

					def desc_carrello_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[2]/span", true)
					def desc_carrello= WebUI.getText(desc_carrello_OBJ)

					def costo_una_tantum_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[3]/span[1]", true)
					def costo_una_tantum= WebUI.getText(costo_una_tantum_OBJ)

					def costo_ricorrente_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[4]/span[1]", true)
					def costo_ricorrente= WebUI.getText(costo_ricorrente_OBJ)
		
					def costo_ricorrente_frequenza_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[4]/span[2]", true)
					def costo_ricorrente_frequenza= WebUI.getText(costo_ricorrente_frequenza_OBJ)
	
					OFFERTA_vendibile= desc_carrello + ' / ' + costo_una_tantum + ' / ' + costo_ricorrente + '/' + costo_ricorrente_frequenza
	
					colonna=colonna+1
					cell = row.getCell(colonna)
					cell.setCellValue(OFFERTA_vendibile)
				
				}

				} else {

				colonna=colonna+1
				cell = row.getCell(colonna)
				cell.setCellValue('NESSUNA OFFERTA TROVATA')

			}
				
				fis.close()
				fos = new FileOutputStream(filePath)
				workbook.write(fos)
				fos.close()
			
				
				break

		
				
	case 'FISSO E MOBILE':
	
	//----------------------------prima il fisso----------------------------

			WebUI.waitForElementVisible(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_FISSO'), 60)
			WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_FISSO'), 60)
			WebUI.focus(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_FISSO'))
			WebUI.click(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_FISSO'))
	
			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
				60)
	
			total_rows = driver.findElements(By.xpath("//table[@class='catalogItems-table productsTableOrd']/tbody/tr/td[2]/span")).size()
	
			
			cell = row.getCell(colonna)
			cell.setCellValue('OFFERTE FISSE:')
	
			if(total_rows>0) {
		
				for(int numberOfRows=1; numberOfRows<=total_rows; numberOfRows++) {
					def OFFERTA_vendibile=''

					def desc_carrello_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[2]/span", true)
					def desc_carrello= WebUI.getText(desc_carrello_OBJ)

					def costo_una_tantum_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[3]/span[1]", true)
					def costo_una_tantum= WebUI.getText(costo_una_tantum_OBJ)

					def costo_ricorrente_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[4]/span[1]", true)
					def costo_ricorrente= WebUI.getText(costo_ricorrente_OBJ)

					def costo_ricorrente_frequenza_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
						"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[4]/span[2]", true)
					def costo_ricorrente_frequenza= WebUI.getText(costo_ricorrente_frequenza_OBJ)

					OFFERTA_vendibile= desc_carrello + ' / ' + costo_una_tantum + ' / ' + costo_ricorrente + '/' + costo_ricorrente_frequenza

					colonna=colonna+1
					cell = row.getCell(colonna)
					cell.setCellValue(OFFERTA_vendibile)
		
				}

			} else {

			colonna=colonna+1
			cell = row.getCell(colonna)
			cell.setCellValue('NESSUNA OFFERTA TROVATA')

			}
	
		//--------------------------------------va selezionata un'fferta fissa per accedere alle offerte mobili---------------------	
			
			
			def offerta_string
			
			switch (SEGMENTO) {
				case 'RES':
						
				 offerta_string = "//span[(text() = '$OFFERTA_FISSA')]/parent::td/parent::tr/td[5]"

					break
				case 'SHP':
				
				 offerta_string = "//span[(text() = '$OFFERTA_FISSA_SHP')]/parent::td/parent::tr/td[5]"
									
					break
					
				}

			def Seleziona_offerta_new = WebUI.modifyObjectProperty(findTestObject('Page_/Seleziona_Offerta_Fissa'), 'xpath', 'equals',
				offerta_string, true)
			
			WebUI.waitForElementClickable(Seleziona_offerta_new, 60)
			
			//properties= Seleziona_offerta_new.getProperties()
			//for (prop in properties) {
			//	prop.name;
			//	}
			WebUI.click(Seleziona_offerta_new)
			
			WebUI.delay(3)
			
			WebUI.switchToWindowIndex(1)
			
			//Pagina Coupon
			WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'), 60)
			
			WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'))
			
			WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'))
			
			WebUI.switchToWindowIndex(0)
			
			WebUI.waitForElementClickable(findTestObject('Page_Coupon/span_Procedi'), 60)
			
			WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello - Opzioni'), 'style',
				'display: none;', 60)
			
			WebUI.waitForElementClickable(findTestObject('Page_Coupon/span_Procedi'), 60)
			
			WebUI.click(findTestObject('Page_Coupon/span_Procedi'))		
			
			
			//WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
			//	60)
			
			
		//--------------------------------------poi l'offerta mobile, partemdo dalla mobile voce----------------------------------------
		
				WebUI.waitForElementVisible(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'), 60)
				WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'), 60)
				WebUI.focus(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'))
				WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 60)
				WebUI.click(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'))
		
				WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
					60)
				
				total_rows = driver.findElements(By.xpath("//table[@class='catalogItems-table productsTableOrd']/tbody/tr/td[2]/span")).size()
					
				colonna=colonna+1
				cell = row.getCell(colonna)
				cell.setCellValue('OFFERTE MOBILE VOCE:')
				
				if(total_rows>0) {
								
					for(int numberOfRows=1; numberOfRows<=total_rows; numberOfRows++) {
						def OFFERTA_vendibile=''
	
						def desc_carrello_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
							"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[2]/span", true)
						def desc_carrello= WebUI.getText(desc_carrello_OBJ)
	
						def costo_una_tantum_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
							"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[3]/span[1]", true)
						def costo_una_tantum= WebUI.getText(costo_una_tantum_OBJ)
	
						def costo_ricorrente_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
							"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[4]/span[1]", true)
						def costo_ricorrente= WebUI.getText(costo_ricorrente_OBJ)
						
						def costo_ricorrente_frequenza_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
							"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[4]/span[2]", true)
						def costo_ricorrente_frequenza= WebUI.getText(costo_ricorrente_frequenza_OBJ)
					
						OFFERTA_vendibile= desc_carrello + ' / ' + costo_una_tantum + ' / ' + costo_ricorrente + '/' + costo_ricorrente_frequenza
					
						colonna=colonna+1
						cell = row.getCell(colonna)
						cell.setCellValue(OFFERTA_vendibile)
								
					}
				
				} else {
				
				colonna=colonna+1
				cell = row.getCell(colonna)
				cell.setCellValue('NESSUNA OFFERTA TROVATA')
				
				}
				
				//--------------------------------------------------offerte mobili dati-----------------------------------
					
				WebUI.waitForElementVisible(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'), 60)
				WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'), 60)
				WebUI.focus(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'))
				WebUI.click(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'))
				
				WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
					60)
				
				total_rows = driver.findElements(By.xpath("//table[@class='catalogItems-table productsTableOrd']/tbody/tr/td[2]/span")).size()
					
				colonna=colonna+1
				cell = row.getCell(colonna)
				cell.setCellValue('OFFERTE MOBILE DATI:')
				
				if(total_rows>0) {
					
					for(int numberOfRows=1; numberOfRows<=total_rows; numberOfRows++) {
						def OFFERTA_vendibile=''
	
						def desc_carrello_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
							"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[2]/span", true)
						def desc_carrello= WebUI.getText(desc_carrello_OBJ)
	
						def costo_una_tantum_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
							"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[3]/span[1]", true)
						def costo_una_tantum= WebUI.getText(costo_una_tantum_OBJ)
	
						def costo_ricorrente_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
							"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[4]/span[1]", true)
						def costo_ricorrente= WebUI.getText(costo_ricorrente_OBJ)
			
						def costo_ricorrente_frequenza_OBJ = WebUI.modifyObjectProperty(findTestObject('ESPLORA_CATALOGO/Offerta'), 'xpath', 'equals',
							"//table[@class='catalogItems-table productsTableOrd']/tbody/tr[$numberOfRows]/td[4]/span[2]", true)
						def costo_ricorrente_frequenza= WebUI.getText(costo_ricorrente_frequenza_OBJ)
		
						OFFERTA_vendibile= desc_carrello + ' / ' + costo_una_tantum + ' / ' + costo_ricorrente + '/' + costo_ricorrente_frequenza
		
						colonna=colonna+1
						cell = row.getCell(colonna)
						cell.setCellValue(OFFERTA_vendibile)
					
					}
	
					} else {
	
					colonna=colonna+1
					cell = row.getCell(colonna)
					cell.setCellValue('NESSUNA OFFERTA TROVATA')
	
				}
				
//------------------------------------------------------offerta fissa------------------------------------------------------					
					

						
					fis.close()
					fos = new FileOutputStream(filePath)
					workbook.write(fos)
					fos.close()

				break
}




	WebUI.delay(1)
	
	WebUI.closeBrowser()
	
	

/*


//seleziona offerta
def offerta_string = "//span[(text() = '$OFFERTA_MOBILE')]/parent::td/parent::tr/td[5]"

def Seleziona_offerta_new = WebUI.modifyObjectProperty(findTestObject('Page_/Seleziona_Offerta_Fissa'), 'xpath', 'equals', 
    offerta_string, true)

WebUI.waitForElementClickable(Seleziona_offerta_new, 20)

//properties= Seleziona_offerta_new.getProperties()
//for (prop in properties) {
//	prop.name;
//	}
try {
    WebUI.click(Seleziona_offerta_new, FailureHandling.STOP_ON_FAILURE //This will fail as locator is wrong
        )
}
catch (Exception ex) {
    WebUI.delay(1)

    KeywordUtil.markFailedAndStop('Offerta NON Trovata')
} 

WebUI.delay(3)

WebUI.switchToWindowIndex(1)

//Pagina Coupon
WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'), 60)

WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'))

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'))

WebUI.switchToWindowIndex(0)

WebUI.waitForElementClickable(findTestObject('Page_Coupon/span_Procedi'), 60)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello - Opzioni'), 'style', 
    'display: none;', 60)

//----------------------------------------------INIZIO ACCORDION--------------------------------------------------------
//----------------------------------------------CONFIGURA OFFERTA MOBILE E TERMINALE----------------------------
WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_TIPO_MOBILE'), 2, FailureHandling.OPTIONAL)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_COUPON'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_TIPO_MOBILE'), FailureHandling.OPTIONAL)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

try {
	WebUI.selectOptionByValue(findTestObject('Object_MOBILE/select_Tipo_Famiglia_Mobile'), TIPO_FAMIGLIA_MOBILE, true) //This will fail as locator is wrong
}
catch (Exception ex) {
	WebUI.delay(1)

	KeywordUtil.markFailedAndStop('Tipo di Famiglia Mobile NON Presente')
}

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
    60)

try {
	WebUI.selectOptionByValue(findTestObject('Object_MOBILE/select_Tipo_Vendita_Terminale'), TIPO_VENDITA_TERMINALE, true) //This will fail as locator is wrong
}
catch (Exception ex) {
	WebUI.delay(1)

	KeywordUtil.markFailedAndStop('Tipo Vendita Terminale NON Presente')
}


WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
    60)


try {
	WebUI.selectOptionByValue(findTestObject('Object_MOBILE/select_Tipo_Modello_Terminale'), TIPO_MODELLO_TERMINALE, true) //This will fail as locator is wrong
}
catch (Exception ex) {
	WebUI.delay(1)

	KeywordUtil.markFailedAndStop('Modello Terminale NON Presente')
}


WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
    60)

//----------------------------------------------FINE CONFIGURA OFFERTA MOBILE E TERMINALE----------------------------
WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_COUPON'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_OFFERTA'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_COUPON'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_OPZIONI'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_OPZIONI'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_OPZIONI'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

//----------------------------------------------INIZIO SELEZIONE TERMINALE-------------------------------------------
WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_TERMINALI'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_APPARATI'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_TERMINALI'), FailureHandling.CONTINUE_ON_FAILURE)

//def terminale_string = "//span[text()='$TERMINALE']/preceding::input[@type='radio'][1]"
def terminale_string = "//span[text()[contains(.,'$TERMINALE')]]/preceding::input[@type='radio'][1]"

def Seleziona_terminale_new = WebUI.modifyObjectProperty(findTestObject('/Object_MOBILE/Seleziona_Terminale'), 'xpath', 
    'equals', terminale_string, true)

WebUI.click(Seleziona_terminale_new, FailureHandling.OPTIONAL)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
    60)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

//----------------------------------------------FINE SELEZIONE TERMINALE-------------------------------------------
WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_CONFIGURA_USIM'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_STS'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_CONFIGURA_USIM'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_OFFERTA'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SOFTWARE'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_OFFERTA'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_OPZIONE_DATI'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_RSTP'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_OPZIONE_DATI'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

Presenza_accordion = WebUI.verifyElementPresent(findTestObject('Object_MOBILE/Open_Accordion_SCONTI'), 2, FailureHandling.OPTIONAL)

if (Presenza_accordion == true) {
    WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_SCONTI'), 2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_SCONTI'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(1)
}

//---------------------------------------------FINE CARRELLATA ACCORDION---------------------------------------------------------
//----------------------------------------------SELEZIONA MNP-TCR-------------------------------------------------------------
if (RICHIESTA_MNP == 'SI') {
    WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_CONFIGURA_USIM'), 2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Open_Accordion_CONFIGURA_USIM'), 2)

    //WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_STS'), FailureHandling.CONTINUE_ON_FAILURE)
    WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_CONFIGURA_USIM'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.focus(findTestObject('Object_MOBILE/Seleziona_Richiesta_MNP'))

    WebUI.click(findTestObject('Object_MOBILE/Seleziona_Richiesta_MNP'))

    WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
        60)

    if (RICHIESTA_TCR == 'SI') {
        WebUI.focus(findTestObject('Object_MOBILE/Seleziona_Richiesta_TCR'))

        //WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_STS'), FailureHandling.CONTINUE_ON_FAILURE)
        WebUI.click(findTestObject('Object_MOBILE/Seleziona_Richiesta_TCR'), FailureHandling.CONTINUE_ON_FAILURE)

        WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 
            'display: none;', 60)
    }
    
    WebUI.delay(1)
}

//----------------------------------------------FINE SELEZIONA MNP-TCR-------------------------------------------------------------
//----------------------------------------------INIZIO SELEZIONA OPZIONI------------------------------------------------------------------------
/*
if (OPZIONE_F_1 != '') {
    def opzione_string = "//span[text()='$OPZIONE_F_1']/preceding::input[1]"

    def Seleziona_opzione_new = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 
        'equals', opzione_string, true)

    def open_accordion_string = "//span[text()='$OPZIONE_F_1']/preceding::input[1]/preceding::div[contains(concat(' ', normalize-space(@class), ' '), ' main-content-box-header main-content-box-header-complex ')][1]/img"

    def open_accordion = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 'equals', 
        open_accordion_string, true)

    WebUI.click(open_accordion, FailureHandling.OPTIONAL)

    WebUI.waitForElementVisible(Seleziona_opzione_new, 5, FailureHandling.OPTIONAL)

    WebUI.delay(1)

    WebUI.click(Seleziona_opzione_new, FailureHandling.OPTIONAL)

    WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
        60)
}
*/
//----------------------------------------------------------------FINE SELEZIONA OPZIONI--------------------------------------------






