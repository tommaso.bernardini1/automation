import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.*
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.thoughtworks.selenium.webdriven.Windows as Windows
import groovy.transform.Field as Field
import selenium.webdriver.common.*
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import org.openqa.selenium.WebElement
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.apache.poi.ss.usermodel.Workbook as Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory as WorkbookFactory
import org.apache.poi.hssf.usermodel.HSSFCell as HSSFCell
import org.apache.poi.hssf.usermodel.HSSFCellStyle as HSSFCellStyle
import org.apache.poi.hssf.usermodel.HSSFDataFormat as HSSFDataFormat
import org.apache.poi.hssf.usermodel.HSSFRow as HSSFRow
import org.apache.poi.hssf.usermodel.HSSFSheet as HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook as HSSFWorkbook
import org.apache.poi.hssf.util.HSSFColor as HSSFColor
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.ss.usermodel.Sheet as Sheet

if (SKIP=='Y') {
	
	KeywordUtil.markErrorAndStop('Il record è stato skyppato')
	
}

WebUI.openBrowser('')

WebUI.maximizeWindow()

if (CONTRATTO_TV=='') {
	
	KeywordUtil.markErrorAndStop('Il contratto inserito è nullo')
	
}

@Field def int COLONNA_CONTRATTO_BB_EXCEL=2
@Field def int COLONNA_NOTE_EXCEL=3
@Field def int COLONNA_NOTE_NP_EXCEL=20
@Field def int COLONNA_NOTE_APPUNTAMENTO=31


@Field def row_file_excel
@Field KeywordLogger log = new KeywordLogger()

def max_row_num_input = findTestData('Data Files/SKY/INPUT/INPUT_SKY').getRowNumbers()

for (def rowNum_excel = 1; rowNum_excel <= max_row_num_input; rowNum_excel++) {
	def contrattoTV = findTestData('Data Files/SKY/INPUT/INPUT_SKY').getValue(1, rowNum_excel)

	if (contrattoTV == CONTRATTO_TV) {
		row_file_excel = rowNum_excel

		break
	}
}

void part_ACCESSSO_SFDC() {

WebUI.navigateToUrl('https://arcadia2015--fullam.cs80.my.salesforce.com')

WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Nome utente_username'), 'veronica.paci.reply@sales.sky.it.fullam')

WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Password_pw'), 'Aggiornati$8')

WebUI.click(findTestObject('Page_Accedi  Salesforce/input_Password_Login'))

WebUI.delay(2)

}

part_ACCESSSO_SFDC()

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////FUNZIONI/////////////////////////////////////////////////////////

void part_SCRIVI_SU_FILE(String PercorsoFile, String StringaDaScrivere, int Colonna) {
	
String URL_INPUT_SKY = findTestData(PercorsoFile).getSourceUrl()
	
		String filePath = URL_INPUT_SKY
		Workbook workbook
		FileInputStream fis
		FileOutputStream fos
		fis = new FileInputStream(filePath)
		workbook = WorkbookFactory.create(fis)
		Sheet sheet = workbook.getSheetAt(0)
	
		Row row = sheet.getRow(row_file_excel)
		Cell cell = row.getCell(Colonna)
		cell.setCellValue(StringaDaScrivere)
	
		fis.close()
		fos = new FileOutputStream(filePath)
	
		workbook.write(fos)
		fos.close()

}



void part_WAIT_SPINNER() {
	
TestObject SPINNER = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='slds-spinner_container ng-scope']/..")
if (WebUI.verifyElementPresent(SPINNER, 2,FailureHandling.OPTIONAL)){
	WebUI.waitForElementAttributeValue(SPINNER, 'aria-hidden', 'true',75)
	}
WebUI.delay(2)
if (WebUI.verifyElementPresent(SPINNER, 2,FailureHandling.OPTIONAL)){
	WebUI.waitForElementAttributeValue(SPINNER, 'class', 'mask vlc-slds-mask ng-hide',75)
	}
}

void part_WAIT_SPINNER_MEDIUM() {
	
TestObject SPINNER_MEDIUM = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='slds-spinner--brand slds-spinner slds-spinner--medium']/..")

//while (WebUI.waitForElementAttributeValue(SPINNER_MEDIUM, 'class', 'slds-spinner_container',75)
//WebUI.delay(2)

// class slds-spinner_container ng-hide
}





void part_CHIUDI_I_TAB() {
	
	WebUI.delay(1)
	
	WebUI.waitForElementPresent(FRECCIA_DESTRA, 60)
	WebUI.waitForElementClickable(FRECCIA_DESTRA, 60)
	CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(FRECCIA_DESTRA, 10)
	
	WebUI.delay(1)
	
	WebUI.click(CLOSE_ALL_PRIMARY_TABS)
	
	WebUI.delay(1)
	
	TestObject MODIFICHE_NON_SALVATE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//span[contains(text(),'Unsaved Changes')]")
	if (WebUI.verifyElementPresent(MODIFICHE_NON_SALVATE, 2,FailureHandling.OPTIONAL)) {
		TestObject PULSANTE_CONTINUE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//button[contains(text(),'Continue')]")
		WebUI.click(PULSANTE_CONTINUE,FailureHandling.OPTIONAL)
		WebUI.delay(1)
				
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

TestObject CREA_ITERAZIONE_MANUALE = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[contains(text(),\'Crea Interazione manuale\')]')
WebUI.waitForElementVisible(CREA_ITERAZIONE_MANUALE, 45, FailureHandling.OPTIONAL)

if (!WebUI.verifyElementPresent(CREA_ITERAZIONE_MANUALE, 3, FailureHandling.OPTIONAL)) {
	
	TestObject MENU_SFDC = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[@class="menuButtonLabel"]')
	WebUI.waitForElementVisible(MENU_SFDC, 5)
	WebUI.waitForElementPresent(MENU_SFDC, 5)
	WebUI.click(MENU_SFDC)
	
	WebUI.delay(1)
	
	TestObject MENU_SFDC_NewCRM = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//a[@class="menuButtonMenuLink" and text()="NewCRM" ]')
	WebUI.waitForElementVisible(MENU_SFDC_NewCRM, 5)
	WebUI.waitForElementPresent(MENU_SFDC_NewCRM, 5)
	WebUI.click(MENU_SFDC_NewCRM)
	
	WebUI.waitForElementVisible(CREA_ITERAZIONE_MANUALE, 45, FailureHandling.OPTIONAL)
	
	
}




WebUI.delay(2)

@Field TestObject FRECCIA_DESTRA = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//div[@id=\'ext-gen36\']')
WebUI.waitForElementPresent(FRECCIA_DESTRA, 60)
WebUI.waitForElementClickable(FRECCIA_DESTRA, 60)
CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(FRECCIA_DESTRA, 10)

WebUI.delay(2)

WebUI.waitForElementPresent(FRECCIA_DESTRA, 60)
WebUI.waitForElementClickable(FRECCIA_DESTRA, 60)
CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(FRECCIA_DESTRA, 10)

@Field TestObject CLOSE_ALL_PRIMARY_TABS = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[contains(text(),\'Close all primary tabs\')]')

TestObject VERIFICA_CLOSE_ALL_PRIMARY_TABS = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//span[contains(text(),\'Close all primary tabs\')]/../..')
String attributo_class = WebUI.getAttribute(VERIFICA_CLOSE_ALL_PRIMARY_TABS, 'class')

if (attributo_class.toString()!='x-menu-list-item  x-item-disabled') {
	
	part_CHIUDI_I_TAB()
	
}

WebUI.delay(2)

WebUI.switchToDefaultContent()

void part_CREA_ITERAZIONE_RICERCA_CONTRATTO() {

TestObject FRAME_PULSANTE_CREA_ITERAZIONE = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//iframe[@id=\'ext-comp-1005\']')
WebUI.switchToFrame(FRAME_PULSANTE_CREA_ITERAZIONE, 5)

TestObject PULSANTE_CREA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@class='manualIntBtn' and @value='CREA']")

def Presenza_pulsante_crea_iterazione = WebUI.verifyElementPresent(PULSANTE_CREA, 5, FailureHandling.OPTIONAL)
KeywordUtil.logInfo(Presenza_pulsante_crea_iterazione.toString())

WebUI.waitForElementClickable(PULSANTE_CREA, 60)
WebUI.click(PULSANTE_CREA)

WebUI.delay(2)

WebUI.switchToDefaultContent()

TestObject FRAME_CERCA_CONTRATTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div/div[3]/div/div[2]/div[2]/div[2]/div/div/div/iframe')
WebUI.waitForElementPresent(FRAME_CERCA_CONTRATTO, 60)
WebUI.switchToFrame(FRAME_CERCA_CONTRATTO, 5)

TestObject INSERIMENTO_CONTRATTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//tr[@class="ricerStyle"][1]/td/label[contains(text(),"Codice Contratto")]/parent::td/following-sibling::td[1]/input')
WebUI.waitForElementPresent(INSERIMENTO_CONTRATTO, 5)
WebUI.scrollToElement(INSERIMENTO_CONTRATTO, 5)
WebUI.waitForElementVisible(INSERIMENTO_CONTRATTO, 5)
WebUI.setText(INSERIMENTO_CONTRATTO, CONTRATTO_TV)

TestObject CERCA_CONTRATTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@class="btn standardSearchButton"]')
WebUI.scrollToElement(CERCA_CONTRATTO, 5)
WebUI.waitForElementVisible(CERCA_CONTRATTO, 5)
WebUI.click(CERCA_CONTRATTO)

TestObject CLIK_CONTRATTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//a[contains(text(),'$CONTRATTO_TV')]")
WebUI.waitForElementPresent(CLIK_CONTRATTO, 5)
WebUI.scrollToElement(CLIK_CONTRATTO, 5)
WebUI.waitForElementVisible(CLIK_CONTRATTO, 5)
WebUI.click(CLIK_CONTRATTO)

WebUI.waitForPageLoad(60)

}

part_CREA_ITERAZIONE_RICERCA_CONTRATTO()

WebUI.switchToDefaultContent()

TestObject FRAME_PORTALE = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div/div[3]/div/div[2]/div[2]/div[2]/div/div[2]/div/iframe')
WebUI.waitForElementPresent(FRAME_PORTALE, 60)
WebUI.switchToFrame(FRAME_PORTALE, 5)

TestObject ICONA_PULSANTE_BB = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//button[contains(@class,'iconeConsistenzaContainer')]")
WebUI.waitForElementPresent(ICONA_PULSANTE_BB, 60, FailureHandling.OPTIONAL)


if (!WebUI.verifyElementPresent(ICONA_PULSANTE_BB, 120, FailureHandling.OPTIONAL)) {
	
	part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'TIME OUT - non è stato caricato il portale', COLONNA_NOTE_EXCEL)
	KeywordUtil.markFailedAndStop('TIME OUT - non è stato caricato il portale')
	
}



TestObject PULSANTE_UPSELL_BB = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//button[@title='Contratto BB - NON PRESENTE']")
WebUI.waitForElementPresent(PULSANTE_UPSELL_BB, 5, FailureHandling.OPTIONAL)


if (!WebUI.verifyElementPresent(PULSANTE_UPSELL_BB, 3, FailureHandling.OPTIONAL)) {
	
	part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'ERRORE - upsell non possibile, è presente gia un ordine o contratto BB', COLONNA_NOTE_EXCEL)
	KeywordUtil.markFailedAndStop('ERRORE - upsell non possibile, è presente gia un ordine o contratto BB')
	
}

WebUI.scrollToElement(PULSANTE_UPSELL_BB, 5, FailureHandling.OPTIONAL)
WebUI.waitForElementVisible(PULSANTE_UPSELL_BB, 5, FailureHandling.OPTIONAL)


void part_CONTROLLO_CONSISTENZA() {
	
	WebUI.switchToDefaultContent()
	
	TestObject FRAME_CONSISTENZA_TV = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[contains(@class,'sd_primary_container x-border-layout-ct') and not(contains(@class,'x-hide-display'))]//iframe[contains(@src,'NewCrm_Hybrid_HomeCliente?')]")
	WebUI.waitForElementPresent(FRAME_CONSISTENZA_TV, 30, FailureHandling.OPTIONAL)
	WebUI.switchToFrame(FRAME_CONSISTENZA_TV, 5)
	
	TestObject PACCHETTI_CONSISTENZA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//span[@class='spanInfoConsistenza']/b[contains(text(),'Descrizione:')]")
	
	
	if (!WebUI.verifyElementPresent(PACCHETTI_CONSISTENZA, 5, FailureHandling.OPTIONAL)) {
		
		part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'ERRORE - consistenza presente in maniera non corretta - possibile migrazione non corretta', COLONNA_NOTE_EXCEL)
		KeywordUtil.markFailedAndStop('ERRORE - consistenza presente in maniera non corretta - possibile migrazione non corretta')
		
	}
	
	
}

WebUI.delay(1)

part_CONTROLLO_CONSISTENZA()

WebUI.switchToDefaultContent()
WebUI.switchToFrame(FRAME_PORTALE, 5)


////////////////////////////// MODIFICA INDIRIZZO ///////////////////////////////////////

@Field TestObject WAIT = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='portals']/../..//div[@class='loadingBox overlay']")


void part_MODIFICA_INDIRIZZO_MANUALE() {

TestObject PULSANTE_INDIRIZZI = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//h3[contains(text(),'INDIRIZZI')]/span")
WebUI.waitForElementPresent(PULSANTE_INDIRIZZI, 5)
WebUI.scrollToElement(PULSANTE_INDIRIZZI, 5)
WebUI.waitForElementVisible(PULSANTE_INDIRIZZI, 5)
WebUI.click(PULSANTE_INDIRIZZI)

WebUI.delay(1)

TestObject PULSANTE_MODIFICA_INDIRIZZO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[2]/div[1]/div[1]/span/img")
WebUI.waitForElementPresent(PULSANTE_MODIFICA_INDIRIZZO, 5)

if (!WebUI.verifyElementPresent(PULSANTE_MODIFICA_INDIRIZZO, 5, FailureHandling.OPTIONAL)) {
	
	part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'ERRORE - non è presente il pulsante modifica indirizzo', COLONNA_NOTE_EXCEL)
	KeywordUtil.markFailedAndStop('ERRORE - non è presente il pulsante modifica indirizzo')
	
}

WebUI.scrollToElement(PULSANTE_MODIFICA_INDIRIZZO, 5)
WebUI.waitForElementVisible(PULSANTE_MODIFICA_INDIRIZZO, 5)
WebUI.click(PULSANTE_MODIFICA_INDIRIZZO)

WebUI.delay(1)

TestObject PULSANTE_FRECCIA_LOCALITA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='inputColumn ubicazione selected']//span[@class='select2-arrow']/b")
WebUI.waitForElementPresent(PULSANTE_FRECCIA_LOCALITA, 5)
WebUI.waitForElementVisible(PULSANTE_FRECCIA_LOCALITA, 5)
WebUI.waitForElementClickable(PULSANTE_FRECCIA_LOCALITA, 5)
WebUI.delay(3)
WebUI.click(PULSANTE_FRECCIA_LOCALITA)

TestObject BOX_LOCALITA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[5]/div/input")
WebUI.waitForElementPresent(BOX_LOCALITA, 5)
WebUI.scrollToElement(BOX_LOCALITA, 5)
WebUI.waitForElementVisible(BOX_LOCALITA, 5)
WebUI.setText(BOX_LOCALITA, LOCALITA)

TestObject SELEZIONA_LOCALITA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//span[@class='select2-match' and contains(text(),'$LOCALITA')]/parent::div[contains(text(),'$PROVINCIA')]/span")
WebUI.waitForElementClickable(SELEZIONA_LOCALITA, 10)

WebUI.delay(1)

WebUI.click(SELEZIONA_LOCALITA)

WebUI.delay(1)

TestObject BOX_TOPONOMASTICA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='inputColumn ubicazione selected']//input[@class='form-control inputIndirizzoForm indPartTopo']")
WebUI.waitForElementPresent(BOX_TOPONOMASTICA, 5)
WebUI.scrollToElement(BOX_TOPONOMASTICA, 5)
WebUI.waitForElementVisible(BOX_TOPONOMASTICA, 5)
WebUI.setText(BOX_TOPONOMASTICA, PARTICELLA_TOPONOMASTICA)

TestObject BOX_INDIRIZZO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='inputColumn ubicazione selected']//input[@class='form-control inputIndirizzoForm indVia']")
WebUI.waitForElementPresent(BOX_INDIRIZZO, 5)
WebUI.scrollToElement(BOX_INDIRIZZO, 5)
WebUI.waitForElementVisible(BOX_INDIRIZZO, 5)
WebUI.setText(BOX_INDIRIZZO, INDIRIZZO)

TestObject BOX_CIVICO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='inputColumn ubicazione selected']//input[@class='form-control inputIndirizzoForm indCiv']")
WebUI.waitForElementPresent(BOX_CIVICO, 5)
WebUI.scrollToElement(BOX_CIVICO, 5)
WebUI.waitForElementVisible(BOX_CIVICO, 5)
WebUI.setText(BOX_CIVICO, NUMERO_CIVICO)

WebUI.delay(1)


WebUI.waitForElementAttributeValue(WAIT, 'style', 'display: none;',	60)

WebUI.delay(1)

WebUI.click(BOX_INDIRIZZO)

WebUI.delay(1)

WebUI.waitForElementAttributeValue(WAIT, 'style', 'display: none;',	60)

WebUI.delay(1)

TestObject PULSANTE_SALVA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//button[@class='saveAddrButton saveDeleteBtns']")
WebUI.scrollToElement(PULSANTE_SALVA, 5)
WebUI.waitForElementClickable(PULSANTE_SALVA, 10)
CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(PULSANTE_SALVA, 10)

WebUI.delay(1)

WebUI.waitForElementAttributeValue(WAIT, 'style', 'display: none;',	60)

WebUI.delay(1)

}


String part_MODIFICA_INDIRIZZO_AUTOMATICO(){
	
	def max_row_num_tec = findTestData('Data Files/SKY/FILE_SUPPORTO/INDIRIZZI_TECNOLOGIE_SKY').getRowNumbers()
	def row_tec
	
	for (def rowNum_tec = 1; rowNum_tec <= max_row_num_tec; rowNum_tec++) {
		def tec = findTestData('Data Files/SKY/FILE_SUPPORTO/INDIRIZZI_TECNOLOGIE_SKY').getValue(1, rowNum_tec)
	
		if (tec == TECNOLOGIA_AUTOMATICA) {
			row_tec = rowNum_tec
			break
		}
	}
	
	def LOCALITA_AUTO=findTestData('Data Files/SKY/FILE_SUPPORTO/INDIRIZZI_TECNOLOGIE_SKY').getValue(2, row_tec)
	def PROVINCIA_AUTO=findTestData('Data Files/SKY/FILE_SUPPORTO/INDIRIZZI_TECNOLOGIE_SKY').getValue(3, row_tec)
	def PARTICELLA_TOPONOMASTICA_AUTO=findTestData('Data Files/SKY/FILE_SUPPORTO/INDIRIZZI_TECNOLOGIE_SKY').getValue(4, row_tec)
	def INDIRIZZO_AUTO=findTestData('Data Files/SKY/FILE_SUPPORTO/INDIRIZZI_TECNOLOGIE_SKY').getValue(5, row_tec)
	def NUMERO_CIVICO_AUTO=findTestData('Data Files/SKY/FILE_SUPPORTO/INDIRIZZI_TECNOLOGIE_SKY').getValue(6, row_tec)
	def SCALA_AUTO=findTestData('Data Files/SKY/FILE_SUPPORTO/INDIRIZZI_TECNOLOGIE_SKY').getValue(7, row_tec)
	
	TestObject PULSANTE_INDIRIZZI_AUTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//h3[contains(text(),'INDIRIZZI')]/span")
	WebUI.waitForElementPresent(PULSANTE_INDIRIZZI_AUTO, 5)
	WebUI.scrollToElement(PULSANTE_INDIRIZZI_AUTO, 5)
	WebUI.waitForElementVisible(PULSANTE_INDIRIZZI_AUTO, 5)
	WebUI.click(PULSANTE_INDIRIZZI_AUTO)
	
	WebUI.delay(1)
	
	TestObject PULSANTE_MODIFICA_INDIRIZZO_AUTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[2]/div[1]/div[1]/span/img")
	WebUI.waitForElementPresent(PULSANTE_MODIFICA_INDIRIZZO_AUTO, 5)
	
	if (!WebUI.verifyElementPresent(PULSANTE_MODIFICA_INDIRIZZO_AUTO, 5, FailureHandling.OPTIONAL)) {
		
		part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'ERRORE - non è presente il pulsante modifica indirizzo', COLONNA_NOTE_EXCEL)
		KeywordUtil.markFailedAndStop('ERRORE - non è presente il pulsante modifica indirizzo')
		
	}
	
	WebUI.scrollToElement(PULSANTE_MODIFICA_INDIRIZZO_AUTO, 5)
	WebUI.waitForElementVisible(PULSANTE_MODIFICA_INDIRIZZO_AUTO, 5)
	WebUI.click(PULSANTE_MODIFICA_INDIRIZZO_AUTO)
	
	WebUI.delay(1)
	
	TestObject PULSANTE_FRECCIA_LOCALITA_AUTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='inputColumn ubicazione selected']//span[@class='select2-arrow']/b")
	WebUI.waitForElementPresent(PULSANTE_FRECCIA_LOCALITA_AUTO, 5)
	WebUI.waitForElementVisible(PULSANTE_FRECCIA_LOCALITA_AUTO, 5)
	WebUI.waitForElementClickable(PULSANTE_FRECCIA_LOCALITA_AUTO, 5)
	WebUI.delay(3)
	WebUI.click(PULSANTE_FRECCIA_LOCALITA_AUTO)
	
	TestObject BOX_LOCALITA_AUTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[5]/div/input")
	WebUI.waitForElementPresent(BOX_LOCALITA_AUTO, 5)
	WebUI.scrollToElement(BOX_LOCALITA_AUTO, 5)
	WebUI.waitForElementVisible(BOX_LOCALITA_AUTO, 5)
	WebUI.setText(BOX_LOCALITA_AUTO, LOCALITA_AUTO)
	
	TestObject SELEZIONA_LOCALITA_AUTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//span[@class='select2-match' and contains(text(),'$LOCALITA_AUTO')]/parent::div[contains(text(),'$PROVINCIA_AUTO')]/span")
	WebUI.waitForElementClickable(SELEZIONA_LOCALITA_AUTO, 10)
	
	WebUI.delay(1)
	
	WebUI.click(SELEZIONA_LOCALITA_AUTO)
	
	WebUI.delay(1)
	
	TestObject BOX_TOPONOMASTICA_AUTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='inputColumn ubicazione selected']//input[@class='form-control inputIndirizzoForm indPartTopo']")
	WebUI.waitForElementPresent(BOX_TOPONOMASTICA_AUTO, 5)
	WebUI.scrollToElement(BOX_TOPONOMASTICA_AUTO, 5)
	WebUI.waitForElementVisible(BOX_TOPONOMASTICA_AUTO, 5)
	WebUI.setText(BOX_TOPONOMASTICA_AUTO, PARTICELLA_TOPONOMASTICA_AUTO)
	
	TestObject BOX_INDIRIZZO_AUTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='inputColumn ubicazione selected']//input[@class='form-control inputIndirizzoForm indVia']")
	WebUI.waitForElementPresent(BOX_INDIRIZZO_AUTO, 5)
	WebUI.scrollToElement(BOX_INDIRIZZO_AUTO, 5)
	WebUI.waitForElementVisible(BOX_INDIRIZZO_AUTO, 5)
	WebUI.setText(BOX_INDIRIZZO_AUTO, INDIRIZZO_AUTO)
	
	TestObject BOX_CIVICO_AUTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='inputColumn ubicazione selected']//input[@class='form-control inputIndirizzoForm indCiv']")
	WebUI.waitForElementPresent(BOX_CIVICO_AUTO, 5)
	WebUI.scrollToElement(BOX_CIVICO_AUTO, 5)
	WebUI.waitForElementVisible(BOX_CIVICO_AUTO, 5)
	WebUI.setText(BOX_CIVICO_AUTO, NUMERO_CIVICO_AUTO)
	
	WebUI.delay(1)
	
	
	WebUI.waitForElementAttributeValue(WAIT, 'style', 'display: none;',	60)
	
	WebUI.delay(1)
	
	WebUI.click(BOX_INDIRIZZO_AUTO)
	
	WebUI.delay(1)
	
	WebUI.waitForElementAttributeValue(WAIT, 'style', 'display: none;',	60)
	
	WebUI.delay(1)
	
	TestObject PULSANTE_SALVA_AUTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//button[@class='saveAddrButton saveDeleteBtns']")
	WebUI.scrollToElement(PULSANTE_SALVA_AUTO, 5)
	WebUI.waitForElementClickable(PULSANTE_SALVA_AUTO, 10)
	CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(PULSANTE_SALVA_AUTO, 10)
	
	WebUI.delay(1)
	
	WebUI.waitForElementAttributeValue(WAIT, 'style', 'display: none;',	60)
	
	WebUI.delay(1)
		
	return SCALA_AUTO
}


def String TECNOLOGIA_SCENARIO=''
if (TECNOLOGIA_MANUALE!=''){
	TECNOLOGIA_SCENARIO=TECNOLOGIA_MANUALE
	if (TECNOLOGIA_SCENARIO=='FIBRA_Attivo' || TECNOLOGIA_SCENARIO=='FIBRA_Passivo'){
		TECNOLOGIA_SCENARIO='FIBRA'
	} else {
	TECNOLOGIA_SCENARIO='NON FIBRA'
	}
	
} else {
	TECNOLOGIA_SCENARIO=TECNOLOGIA_AUTOMATICA
	if (TECNOLOGIA_SCENARIO=='FIBRA_Attivo' || TECNOLOGIA_SCENARIO=='FIBRA_Passivo'){
		TECNOLOGIA_SCENARIO='FIBRA'
	} else {
	TECNOLOGIA_SCENARIO='NON FIBRA'
	}
}

def String SCALA_AUTOMATICA=''

if (TECNOLOGIA_MANUALE!=''){
	part_MODIFICA_INDIRIZZO_MANUALE()
} else {
	SCALA_AUTOMATICA=part_MODIFICA_INDIRIZZO_AUTOMATICO()
}

@Field String SCALA_SCENARIO=''

if (TECNOLOGIA_MANUALE!=''){
	SCALA_SCENARIO=SCALA
} else {
	SCALA_SCENARIO=SCALA_AUTOMATICA
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WebUI.scrollToElement(PULSANTE_UPSELL_BB, 5)
WebUI.waitForElementVisible(PULSANTE_UPSELL_BB, 5)

CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(PULSANTE_UPSELL_BB, 30)

WebUI.waitForElementAttributeValue(WAIT, 'style', 'display: none;',	60)

WebUI.delay(1)

WebUI.switchToDefaultContent()

@Field int a=1

/////////////////////////////////////////NORMALIZZAZIONE INDIRIZZO/////////////////////////////////////////////////////////////

def void part_NORMALIZZAZIONE_INDIRIZZO() {
	
TestObject FRAME_NORMALIZZAZIONE_INDIRIZZO = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div/div[3]/div/div[2]/div[2]/div[2]/div/div[3]/div/iframe')
WebUI.waitForElementPresent(FRAME_NORMALIZZAZIONE_INDIRIZZO, 60)

if (!WebUI.verifyElementPresent(FRAME_NORMALIZZAZIONE_INDIRIZZO, 10, FailureHandling.OPTIONAL)) {
	
	part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'TIME OUT - non è stata caricata la pagina normalizzaizone indirizzo (siamo rimasti sul portale)', COLONNA_NOTE_EXCEL)
	KeywordUtil.markFailedAndStop('TIME OUT - non è stata caricata la pagina normalizzaizone indirizzo (siamo rimasti sul portale)')
	
}

WebUI.switchToFrame(FRAME_NORMALIZZAZIONE_INDIRIZZO, 5)

part_WAIT_SPINNER()

TestObject PULSANTE_SALVA_FRECCIA_VERDE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='slds-form-element__control']/p/p/img")

WebUI.waitForElementPresent(PULSANTE_SALVA_FRECCIA_VERDE, 30, FailureHandling.OPTIONAL)
WebUI.waitForElementVisible(PULSANTE_SALVA_FRECCIA_VERDE, 30, FailureHandling.OPTIONAL)

TestObject LAVORAZIONE_NON_ESEGUIBILE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[contains(text(),'Attenzione! Lavorazione non eseguibile')]")

TestObject LAVORAZIONE_TIME_OUT = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//p[contains(text(),'Unable to connect to the server (transaction aborted: timeout)')]")

TestObject LAVORAZIONE_EXCEED_LIMIT = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//p[contains(text(),'Query of LOB fields caused heap usage to exceed limit')]")

TestObject ERRORE_CoverageDataToOrder = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//p[contains(text(),'CoverageDataToOrder')]")


if (WebUI.verifyElementPresent(LAVORAZIONE_NON_ESEGUIBILE, 2, FailureHandling.OPTIONAL) || WebUI.verifyElementPresent(LAVORAZIONE_TIME_OUT, 2, FailureHandling.OPTIONAL) || WebUI.verifyElementPresent(LAVORAZIONE_EXCEED_LIMIT, 2, FailureHandling.OPTIONAL) || WebUI.verifyElementPresent(ERRORE_CoverageDataToOrder, 2, FailureHandling.OPTIONAL)) {
	
	WebUI.switchToDefaultContent()
	
	WebUI.delay(1)
	
	TestObject CLOSE_TAB_1 = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//li[contains(@class,'x-tab-strip-closable x-tab-strip-active')]/a/em/span/span/span[contains(text(),'Omniscript')]/../../../../../a[@class='x-tab-strip-close']")
	
	WebUI.delay(1)
	
	WebUI.waitForElementPresent(CLOSE_TAB_1, 60)
	WebUI.scrollToElement(CLOSE_TAB_1, 5)
	WebUI.waitForElementClickable(CLOSE_TAB_1, 10)
	
	CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(CLOSE_TAB_1, 30)
		
	WebUI.delay(2)
	
	WebUI.switchToDefaultContent()
	
	WebUI.refresh()
	
	TestObject FRAME_PORTALE_2 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div/div[3]/div/div[2]/div[2]/div[2]/div/div[2]/div/iframe')
	WebUI.waitForElementPresent(FRAME_PORTALE_2, 60)
	WebUI.switchToFrame(FRAME_PORTALE_2, 5)
	
	TestObject PULSANTE_UPSELL_BB_2 = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//button[@title='Contratto BB - NON PRESENTE']")
	WebUI.scrollToElement(PULSANTE_UPSELL_BB_2, 5)
	WebUI.waitForElementVisible(PULSANTE_UPSELL_BB_2, 5)
	
	CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(PULSANTE_UPSELL_BB_2, 30)

	WebUI.waitForElementAttributeValue(WAIT, 'style', 'display: none;',	60)
	
	WebUI.delay(1)
	
	WebUI.switchToDefaultContent()
	
	log.logInfo(Integer.toString(a))
	
	if (a>MAX_RETRY_CONTROLLI_INIZIALI){
		
		part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'ERRORE - non si passa controlli iniziali', COLONNA_NOTE_EXCEL)
		KeywordUtil.markFailedAndStop('ERRORE - non si passa controlli iniziali')
		
	} else {
	 
	a=a+1
	part_NORMALIZZAZIONE_INDIRIZZO()
	}
	
	
	
}  else {

WebUI.delay(1)

TestObject PULSANTE_AVANTI_NORMALIZZAZIONE_INDIRIZZO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='street-view_nextBtn']/p")

WebUI.waitForElementPresent(PULSANTE_AVANTI_NORMALIZZAZIONE_INDIRIZZO, 60)
WebUI.scrollToElement(PULSANTE_AVANTI_NORMALIZZAZIONE_INDIRIZZO, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_NORMALIZZAZIONE_INDIRIZZO, 10)

WebUI.click(PULSANTE_AVANTI_NORMALIZZAZIONE_INDIRIZZO)

WebUI.delay(1)

} 

//////////////////////////////


}



part_NORMALIZZAZIONE_INDIRIZZO()


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WebUI.switchToDefaultContent()

/////////////////////////////////////////VERIFICA COPERTURA//////////////////////////////////////////////////////////

void part_VERIFICA_COPERTURA() {

TestObject FRAME_VERIFICA_COPERTURA = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div/div[3]/div/div[2]/div[2]/div[2]/div/div[3]/div/iframe')
WebUI.waitForElementPresent(FRAME_VERIFICA_COPERTURA, 60)									
WebUI.switchToFrame(FRAME_VERIFICA_COPERTURA, 5)

part_WAIT_SPINNER()

TestObject ALLERT_NON_COPERTURA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//span[contains(text(),'indirizzo inserito non è ancora coperto dal servizio')]")

if (WebUI.verifyElementPresent(ALLERT_NON_COPERTURA, 5, FailureHandling.OPTIONAL)) {
	
	part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'ERRORE - indirizzo inserito non è ancora coperto dal servizio', COLONNA_NOTE_EXCEL)
	KeywordUtil.markFailedAndStop('ERRORE - indirizzo inserito non è ancora coperto dal servizio')
	
}


TestObject BOX_SCALA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='StaircaseElement']")
WebUI.waitForElementPresent(BOX_SCALA, 5, FailureHandling.OPTIONAL)
WebUI.waitForElementClickable(BOX_SCALA, 5, FailureHandling.OPTIONAL)

if (WebUI.verifyElementPresent(BOX_SCALA,5 , FailureHandling.OPTIONAL)){
	
	

	if (SCALA_SCENARIO==''){
		
		//TestObject OPZIONI_POSSIBILI = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='vlc-control-wrapper']/ul/li")		
		
		//WebDriver driver = DriverFactory.getWebDriver()
		//KeywordLogger log = new KeywordLogger()
		
		//WebUI.click(BOX_SCALA)
		
		//List<WebElement> links = driver.findElements(By.xpath("//div[@class='vlc-control-wrapper']/ul//li[@role='button']"))
		//int NUMERO_OPZIONI= links.size()
		//log.logInfo(Integer.toString(NUMERO_OPZIONI))
		
		//WebUI.click(BOX_SCALA)
		
		for ( int i = 1; i <= 10; i++) {
			
			WebUI.click(BOX_SCALA)
			
			WebUI.delay(1)
			
			TestObject SELEZIONA_SCALA_PER_NUMERO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='vlc-control-wrapper']/ul/li[$i]")
			
			if (!WebUI.verifyElementPresent(SELEZIONA_SCALA_PER_NUMERO, 5, FailureHandling.OPTIONAL)){
				break
			}
			
			WebUI.waitForElementPresent(SELEZIONA_SCALA_PER_NUMERO, 60)
			WebUI.waitForElementClickable(SELEZIONA_SCALA_PER_NUMERO, 10)
			WebUI.scrollToElement(SELEZIONA_SCALA_PER_NUMERO, 5)
			CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(SELEZIONA_SCALA_PER_NUMERO, 10)
						
			TestObject VERIFICA_PRESENZA_SERVIZIO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='slds-box service-box vlc-slds-selectableItem']")
			
			if (WebUI.verifyElementPresent(VERIFICA_PRESENZA_SERVIZIO, 5, FailureHandling.OPTIONAL)){
				break
			}			
		}	
		
	} else {
	
	WebUI.click(BOX_SCALA)
	
	WebUI.delay(1)
	
	TestObject SELEZIONA_SCALA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='vlc-control-wrapper']/ul/li[contains(text(),'$SCALA_SCENARIO')]")
	WebUI.waitForElementPresent(SELEZIONA_SCALA, 60)
	WebUI.waitForElementClickable(SELEZIONA_SCALA, 10)
	WebUI.scrollToElement(SELEZIONA_SCALA, 5)
	WebUI.click(SELEZIONA_SCALA)
	
	}
}


TestObject TIMEOUT_VERIFICA_COPERTURA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//p[@class='ng-binding' and contains(text(),'Unable to connect to the server (transaction aborted: timeout)')]")

if (WebUI.verifyElementPresent(TIMEOUT_VERIFICA_COPERTURA, 10, FailureHandling.OPTIONAL) ) {
	
	part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'ERRORE - TimeOut verifica copertura', COLONNA_NOTE_EXCEL)
	KeywordUtil.markFailedAndStop('ERRORE - TimeOut verifica copertura')
	
}


WebUI.delay(1)

TestObject SELEZIONA_SERVIZIO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='slds-box service-box vlc-slds-selectableItem']/div/img[contains(@ng-src,'/resource/')]")
WebUI.waitForElementPresent(SELEZIONA_SERVIZIO, 60)
WebUI.waitForElementClickable(SELEZIONA_SERVIZIO, 10)
CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(SELEZIONA_SERVIZIO, 30)

WebUI.delay(1)

TestObject PULSANTE_AVANTI_VERIFICA_COPERTURA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='CoverageCheck_nextBtn']/p")
WebUI.waitForElementPresent(PULSANTE_AVANTI_VERIFICA_COPERTURA, 60)
WebUI.scrollToElement(PULSANTE_AVANTI_VERIFICA_COPERTURA, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_VERIFICA_COPERTURA, 10)
WebUI.click(PULSANTE_AVANTI_VERIFICA_COPERTURA)

}

part_VERIFICA_COPERTURA()

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WebUI.switchToDefaultContent()

///////////////////////////////////////////COMPOSIZIONE OFFERTA/////////////////////////////////////////////////////////

void part_COMPOSIZIONE_OFFERTA() {      

TestObject FRAME_COMPOSIZIONE_OFFERTA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//iframe[contains(@src,'OmniConsoleDisplayScriptLightning?')]")																								
WebUI.waitForElementPresent(FRAME_COMPOSIZIONE_OFFERTA, 200)										
WebUI.switchToFrame(FRAME_COMPOSIZIONE_OFFERTA, 5)

part_WAIT_SPINNER()

TestObject FRAME_GESTIONE_CONSISTENZA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//iframe[contains(@src,'c__skyHybridCPQ?')]")  
WebUI.waitForElementPresent(FRAME_GESTIONE_CONSISTENZA, 200)										
WebUI.switchToFrame(FRAME_GESTIONE_CONSISTENZA, 5)

TestObject BB_LINE_SKY_WIFI_HUB = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//h3[contains(text(),'Gestione Consistenza')]/../parent::div[@ng-controller='skyCOCpqProductCart']/div[@class='slds-col cpq-product-cart-items js-cpq-cart-scroll-container scroll']/descendant::div[@id='tab-default-1']/descendant::img[contains(@src,'CPE_BB.png')][1]")
WebUI.waitForElementPresent(BB_LINE_SKY_WIFI_HUB, 100)
//WebUI.waitForElementVisible(BB_LINE_SKY_WIFI_HUB, 200)

TestObject BB_LINE_SKY_WIFI_HUB_CARRELLO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//h3[contains(text(),'Carrello')]/../parent::div[@ng-controller='skyCOCpqProductCartCART']//img[contains(@src,'CPE_BB.png')]")
WebUI.waitForElementPresent(BB_LINE_SKY_WIFI_HUB_CARRELLO, 100)
WebUI.waitForElementVisible(BB_LINE_SKY_WIFI_HUB_CARRELLO, 5, FailureHandling.OPTIONAL)


//chiudere tab e nuovo inizio ordine

if (!WebUI.verifyElementPresent(BB_LINE_SKY_WIFI_HUB_CARRELLO, 10, FailureHandling.OPTIONAL)) {
	
	WebUI.switchToDefaultContent()
	
	WebUI.delay(1)
	
	TestObject CLOSE_TAB = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//li[contains(@class,'x-tab-strip-closable x-tab-strip-active')]/a/em/span/span/span[contains(text(),'Omniscript')]/../../../../../a[@class='x-tab-strip-close']")
	
	WebUI.delay(1)
	
	WebUI.waitForElementPresent(CLOSE_TAB, 60)
	WebUI.scrollToElement(CLOSE_TAB, 5)
	WebUI.waitForElementClickable(CLOSE_TAB, 10)
	
	CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(CLOSE_TAB, 30)
		
	WebUI.delay(2)
	
	WebUI.switchToDefaultContent()
	
	WebUI.refresh()
	
	TestObject FRAME_PORTALE_1 = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div/div[3]/div/div[2]/div[2]/div[2]/div/div[2]/div/iframe')
	WebUI.waitForElementPresent(FRAME_PORTALE_1, 60)
	WebUI.switchToFrame(FRAME_PORTALE_1, 5)
	
	TestObject PULSANTE_UPSELL_BB_1 = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//button[@title='Contratto BB - NON PRESENTE']")
	WebUI.scrollToElement(PULSANTE_UPSELL_BB_1, 5)
	WebUI.waitForElementVisible(PULSANTE_UPSELL_BB_1, 5)
	
	WebUI.click(PULSANTE_UPSELL_BB_1)
	
	WebUI.waitForElementAttributeValue(WAIT, 'style', 'display: none;',	60)
	
	WebUI.delay(1)
	
	WebUI.switchToDefaultContent()
	
	part_NORMALIZZAZIONE_INDIRIZZO()
	
	WebUI.switchToDefaultContent()
	
	part_VERIFICA_COPERTURA()
	
	WebUI.switchToDefaultContent()
	
	WebUI.waitForElementPresent(FRAME_COMPOSIZIONE_OFFERTA, 200)
	WebUI.switchToFrame(FRAME_COMPOSIZIONE_OFFERTA, 5)
	
	part_WAIT_SPINNER()
	
	WebUI.waitForElementPresent(FRAME_GESTIONE_CONSISTENZA, 200)
	WebUI.switchToFrame(FRAME_GESTIONE_CONSISTENZA, 5)
	
	WebUI.waitForElementPresent(BB_LINE_SKY_WIFI_HUB, 100)
	//WebUI.waitForElementVisible(BB_LINE_SKY_WIFI_HUB, 200)
	
	WebUI.waitForElementPresent(BB_LINE_SKY_WIFI_HUB_CARRELLO, 100, FailureHandling.STOP_ON_FAILURE)
	
	if (!WebUI.verifyElementPresent(BB_LINE_SKY_WIFI_HUB_CARRELLO, 10, FailureHandling.OPTIONAL)) {
		
		part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'Il carrello non è stato caricato correttamente anche dopo il retry', COLONNA_NOTE_EXCEL)
		KeywordUtil.markFailedAndStop('Il carrello non è stato caricato correttamente anche dopo il retry')
		
	}
	
	WebUI.scrollToElement(BB_LINE_SKY_WIFI_HUB_CARRELLO, 20)
	WebUI.waitForElementVisible(BB_LINE_SKY_WIFI_HUB_CARRELLO, 20)
		
}

WebUI.switchToDefaultContent()

WebUI.waitForElementPresent(FRAME_COMPOSIZIONE_OFFERTA, 200)
WebUI.switchToFrame(FRAME_COMPOSIZIONE_OFFERTA, 5)

WebUI.delay(3)

TestObject PULSANTE_AVANTI_COMPOSIZIONE_OFFERTA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='HybridCPQ_Step_nextBtn']/p")
WebUI.waitForElementPresent(PULSANTE_AVANTI_COMPOSIZIONE_OFFERTA, 60)
WebUI.scrollToElement(PULSANTE_AVANTI_COMPOSIZIONE_OFFERTA, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_COMPOSIZIONE_OFFERTA, 10)


//scegliere se l'offerta è BB o BB+Voce
if (TIPO_OFFERTA=='BB') {
	
	WebUI.switchToFrame(FRAME_GESTIONE_CONSISTENZA, 5)
		
	TestObject PULSANTE_RIMOZIONE_VOCE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "(//span[contains(text(),'VOICE SERVICE')]/../../button[contains(text(),'Rimuovi')])[1]")
	WebUI.waitForElementPresent(PULSANTE_RIMOZIONE_VOCE, 30)
	WebUI.scrollToElement(PULSANTE_RIMOZIONE_VOCE, 5)
	WebUI.waitForElementClickable(PULSANTE_RIMOZIONE_VOCE, 10)
	WebUI.click(PULSANTE_RIMOZIONE_VOCE)
	
	WebUI.switchToDefaultContent()
	
	WebUI.waitForElementPresent(FRAME_COMPOSIZIONE_OFFERTA, 200)
	WebUI.switchToFrame(FRAME_COMPOSIZIONE_OFFERTA, 5)
	
	TestObject FRAME_POP_UP_RIMOZIONE_VOCE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//iframe[contains(@src,'skyHybridCPQ')]")
	WebUI.waitForElementPresent(FRAME_POP_UP_RIMOZIONE_VOCE, 20)
	WebUI.switchToFrame(FRAME_POP_UP_RIMOZIONE_VOCE, 5)
	
	TestObject POP_UP_DELETE_ITEM = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//p[contains(text(),'Sei sicuro di voler eliminare questo elemento?')]")
	WebUI.waitForElementPresent(POP_UP_DELETE_ITEM, 60)
	WebUI.scrollToElement(POP_UP_DELETE_ITEM, 5)
	WebUI.waitForElementVisible(POP_UP_DELETE_ITEM, 60)
	
	WebUI.delay(1)
	
	TestObject PULSANTE_SI = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//button[contains(text(),'Si')]")
	WebUI.waitForElementPresent(PULSANTE_SI, 60)
	WebUI.scrollToElement(PULSANTE_SI, 5)
	WebUI.waitForElementClickable(PULSANTE_SI, 10)
	WebUI.click(PULSANTE_SI)
	
	}

//aggiugere opzioni
if (OPZIONE_1!='') {
	

//inserire comandi per aggiungere l'opzione selezionata
	
	// introdurre comandi per wait spinner interno

	}

//aggiugere opzioni
if (OPZIONE_2!='') {
	

//inserire comandi per aggiungere l'opzione selezionata
	
	// introdurre comandi per wait spinner interno

	}



WebUI.switchToDefaultContent()

WebUI.waitForElementPresent(FRAME_COMPOSIZIONE_OFFERTA, 200)
WebUI.switchToFrame(FRAME_COMPOSIZIONE_OFFERTA, 5)

WebUI.delay(3)

part_WAIT_SPINNER()

WebUI.waitForElementPresent(PULSANTE_AVANTI_COMPOSIZIONE_OFFERTA, 60)
WebUI.scrollToElement(PULSANTE_AVANTI_COMPOSIZIONE_OFFERTA, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_COMPOSIZIONE_OFFERTA, 10)
CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(PULSANTE_AVANTI_COMPOSIZIONE_OFFERTA, 30)

}

part_COMPOSIZIONE_OFFERTA()


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WebUI.switchToDefaultContent()

/////////////////////////////////////////////METODO DI PAGAMENTO///////////////////////////////////////////////////////////

void part_METODO_DI_PAGAMENTO() {

TestObject FRAME_METODO_DI_PAGAMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div[1]/div[3]/div/div[2]/div[2]/div[2]/div/div[3]/div/iframe')
WebUI.waitForElementPresent(FRAME_METODO_DI_PAGAMENTO, 200)
WebUI.switchToFrame(FRAME_METODO_DI_PAGAMENTO, 5)

part_WAIT_SPINNER()

WebUI.delay(2)

TestObject PULSANTE_AVANTI_METODO_DI_PAGAMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='MopTemplate_nextBtn']/p")
WebUI.waitForElementPresent(PULSANTE_AVANTI_METODO_DI_PAGAMENTO, 200)
WebUI.scrollToElement(PULSANTE_AVANTI_METODO_DI_PAGAMENTO, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_METODO_DI_PAGAMENTO, 200)
WebUI.waitForElementVisible(PULSANTE_AVANTI_METODO_DI_PAGAMENTO, 200)
CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(PULSANTE_AVANTI_METODO_DI_PAGAMENTO, 30)

WebUI.delay(1)

}

part_METODO_DI_PAGAMENTO()

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WebUI.switchToDefaultContent()

///////////////////////////////////////////ATTIVAZIONE NUOVA LINEA O MIGRAZIONE/////////////////////////////////////////////////

void part_ATTIVAZIONE_NUOVA_LINEA_MIGRAZIONE() {

TestObject FRAME_ATTIVAZIONE_NUOVA_LINEA = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div[1]/div[3]/div/div[2]/div[2]/div[2]/div/div[3]/div/iframe')
WebUI.waitForElementPresent(FRAME_ATTIVAZIONE_NUOVA_LINEA, 200)
WebUI.switchToFrame(FRAME_ATTIVAZIONE_NUOVA_LINEA, 5)

part_WAIT_SPINNER()

TestObject ETICHETTA_RADIO_BUTTON_LINEA_ATTIVA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='slds-form-element__control']/label[contains(text(),'Hai già una linea attiva dati o voce diversa da Sky?')]")
WebUI.waitForElementPresent(ETICHETTA_RADIO_BUTTON_LINEA_ATTIVA, 300)
WebUI.scrollToElement(ETICHETTA_RADIO_BUTTON_LINEA_ATTIVA, 200)
WebUI.waitForElementVisible(ETICHETTA_RADIO_BUTTON_LINEA_ATTIVA, 300)

if (LINEA_ATTIVA=='SI') {
	
	TestObject RADIO_BUTTON_LINEA_ATTIVA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='LineaDiversa' and @value='Si']")
	WebUI.waitForElementPresent(RADIO_BUTTON_LINEA_ATTIVA, 30)
	WebUI.scrollToElement(RADIO_BUTTON_LINEA_ATTIVA, 30)
	WebUI.waitForElementVisible(RADIO_BUTTON_LINEA_ATTIVA, 30)
	CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(RADIO_BUTTON_LINEA_ATTIVA, 30)
	
	WebUI.delay(1)
	
	TestObject RADIO_BUTTON_SERVIZIO_ATTIVO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='VoceDati' and @value='$SERVIZIO_ATTIVO']")
	WebUI.waitForElementPresent(RADIO_BUTTON_SERVIZIO_ATTIVO, 30)
	WebUI.scrollToElement(RADIO_BUTTON_SERVIZIO_ATTIVO, 30)
	WebUI.waitForElementVisible(RADIO_BUTTON_SERVIZIO_ATTIVO, 30)
	CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(RADIO_BUTTON_SERVIZIO_ATTIVO, 30)
	

	
	switch(SERVIZIO_ATTIVO)
	{
	case('Dati'):

	break;
	case('Voce'):
			
		if (TIPO_OFFERTA=='BB+VOCE') {
		WebUI.delay(1)
		TestObject RADIO_BUTTON_NUOVO_NUMERO_SKY = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='NuovoNumeroSky' and @value='$VUOI_NUOVO_NUMERO_SKY']")
		WebUI.delay(1)
		CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(RADIO_BUTTON_NUOVO_NUMERO_SKY, 30)
		WebUI.delay(1)
		}
	
	break;
	case('Dati + Voce'):
		
		WebUI.delay(1)
		
		if (OPERATORI_DIVERSI_DATI_VOCE=='SI') {
		
		TestObject OPERATORI_DIVERSI = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='GestoreUnicDatiVoce']/../span")
		WebUI.waitForElementPresent(OPERATORI_DIVERSI, 30)
		WebUI.scrollToElement(OPERATORI_DIVERSI, 30)
		WebUI.waitForElementVisible(OPERATORI_DIVERSI, 30)
		CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(OPERATORI_DIVERSI, 30)
		
		}
		
		if (TIPO_OFFERTA=='BB+VOCE') {
		WebUI.delay(1)
		TestObject RADIO_BUTTON_NUOVO_NUMERO_SKY = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='NuovoNumeroSky' and @value='$VUOI_NUOVO_NUMERO_SKY']")
		WebUI.delay(1)
		CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(RADIO_BUTTON_NUOVO_NUMERO_SKY, 30)
		WebUI.delay(1)
		}
	
	
	break;
	}
	
}

WebUI.delay(1)

TestObject PULSANTE_AVANTI_LINEA_ATTIVA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='AttivazioneNuovaLinea_nextBtn']/p")
WebUI.waitForElementPresent(PULSANTE_AVANTI_LINEA_ATTIVA, 30)
WebUI.scrollToElement(PULSANTE_AVANTI_LINEA_ATTIVA, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_LINEA_ATTIVA, 30)
WebUI.delay(1)


///////////////////////DA INSERIRE PARTE CON MODEM LIBERO/////////////////////////////////////////
WebUI.delay(1)
TestObject RADIO_BUTTON_MODEM = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='ModemLibero' and @value='S']")
WebUI.delay(1)
CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(RADIO_BUTTON_MODEM, 30)
WebUI.delay(1)
/////////////////////////////////////////////////////////////////////

WebUI.waitForElementPresent(PULSANTE_AVANTI_LINEA_ATTIVA, 30)
WebUI.scrollToElement(PULSANTE_AVANTI_LINEA_ATTIVA, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_LINEA_ATTIVA, 30)
WebUI.delay(1)
CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(PULSANTE_AVANTI_LINEA_ATTIVA, 30)

}

part_ATTIVAZIONE_NUOVA_LINEA_MIGRAZIONE()


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WebUI.switchToDefaultContent()

///////////////////////////////////////////MIGRAZIONE///////////////////////////////////////////////////////////////////


void part_MIGRAZIONE() {
	
TestObject FRAME_MIGRAZIONE = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div/div[3]/div/div[2]/div[2]/div[2]/div/div[3]/div/iframe')
WebUI.waitForElementPresent(FRAME_MIGRAZIONE, 200)
WebUI.switchToFrame(FRAME_MIGRAZIONE, 5)

part_WAIT_SPINNER()

switch(SERVIZIO_ATTIVO)
{
case('Dati'):


TestObject BOX_CODICE_MIGRAZIONE_DATI = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='MigrationCodeDati']")
WebUI.waitForElementPresent(BOX_CODICE_MIGRAZIONE_DATI, 30)
WebUI.scrollToElement(BOX_CODICE_MIGRAZIONE_DATI, 5)
WebUI.waitForElementClickable(BOX_CODICE_MIGRAZIONE_DATI, 30)
WebUI.setText(BOX_CODICE_MIGRAZIONE_DATI, CODICE_MIGRAZIONE_DATI)

WebUI.delay(1)

break;
case('Voce'):
		
		TestObject BOX_CODICE_MIGRAZIONE_VOCE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[contains(@id,'MigrationCodeVoce') and not(contains(@id,'Warning'))]")
		WebUI.waitForElementPresent(BOX_CODICE_MIGRAZIONE_VOCE, 30)
		WebUI.scrollToElement(BOX_CODICE_MIGRAZIONE_VOCE, 5)
		WebUI.waitForElementClickable(BOX_CODICE_MIGRAZIONE_VOCE, 30)
		WebUI.setText(BOX_CODICE_MIGRAZIONE_VOCE, CODICE_MIGRAZIONE_VOCE)
		
		WebUI.delay(1)
		
		TestObject BOX_PREFISSO_NUMERO_PORTATO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[contains(@id,'PrefissoFW')]")
		TestObject BOX_NUMERO_PORTATO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[contains(@id,'PhoneNumberFW')]")
		WebUI.waitForElementPresent(BOX_PREFISSO_NUMERO_PORTATO, 30)
		WebUI.scrollToElement(BOX_PREFISSO_NUMERO_PORTATO, 5)
		WebUI.waitForElementClickable(BOX_PREFISSO_NUMERO_PORTATO, 30)
		WebUI.setText(BOX_PREFISSO_NUMERO_PORTATO, PREFISSO_NUMERO_PORTATO)
		WebUI.setText(BOX_NUMERO_PORTATO, NUMERO_PORTATO)	

break;
case('Dati + Voce'):
	
	TestObject BOX_CODICE_MIGRAZIONE_DATI = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='MigrationCodeDati']")
	WebUI.waitForElementPresent(BOX_CODICE_MIGRAZIONE_DATI, 30)
	WebUI.scrollToElement(BOX_CODICE_MIGRAZIONE_DATI, 5)
	WebUI.waitForElementClickable(BOX_CODICE_MIGRAZIONE_DATI, 30)
	WebUI.setText(BOX_CODICE_MIGRAZIONE_DATI, CODICE_MIGRAZIONE_DATI)
	
	WebUI.delay(1)	
	
	TestObject BOX_PREFISSO_NUMERO_PORTATO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[contains(@id,'PrefissoFW')]")
	TestObject BOX_NUMERO_PORTATO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[contains(@id,'PhoneNumberFW')]")
	WebUI.waitForElementPresent(BOX_PREFISSO_NUMERO_PORTATO, 30)
	WebUI.scrollToElement(BOX_PREFISSO_NUMERO_PORTATO, 5)
	WebUI.waitForElementClickable(BOX_PREFISSO_NUMERO_PORTATO, 30)
	WebUI.setText(BOX_PREFISSO_NUMERO_PORTATO, PREFISSO_NUMERO_PORTATO)
	WebUI.setText(BOX_NUMERO_PORTATO, NUMERO_PORTATO)
	
	
	if (OPERATORI_DIVERSI_DATI_VOCE=='SI') {
		
		TestObject BOX_CODICE_MIGRAZIONE_VOCE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[contains(@id,'MigrationCodeVoce') and not(contains(@id,'Warning'))]")  
		WebUI.waitForElementPresent(BOX_CODICE_MIGRAZIONE_VOCE, 30)
		WebUI.scrollToElement(BOX_CODICE_MIGRAZIONE_VOCE, 5)
		WebUI.waitForElementClickable(BOX_CODICE_MIGRAZIONE_VOCE, 30)
		WebUI.setText(BOX_CODICE_MIGRAZIONE_VOCE, CODICE_MIGRAZIONE_VOCE)
	
	}

break;


}

TestObject PULSANTE_VERIFICA_MIGRAZIONE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='CheckMC_Button']/p")
WebUI.waitForElementPresent(PULSANTE_VERIFICA_MIGRAZIONE, 30)
WebUI.scrollToElement(PULSANTE_VERIFICA_MIGRAZIONE, 5)
WebUI.waitForElementClickable(PULSANTE_VERIFICA_MIGRAZIONE, 30)
WebUI.delay(1)
CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(PULSANTE_VERIFICA_MIGRAZIONE, 30)


part_WAIT_SPINNER()

TestObject ETICHETTA_CODICE_RICONOSCIUTO_2= new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='message description ng-binding ng-scope' and contains(text(),'Codice riconosciuto')]")
WebUI.waitForElementPresent(ETICHETTA_CODICE_RICONOSCIUTO_2, 15)


if (!WebUI.verifyElementPresent(ETICHETTA_CODICE_RICONOSCIUTO_2, 10, FailureHandling.OPTIONAL)) {
	
	part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'ERRORE - Codice di migrazione non valido', COLONNA_NOTE_NP_EXCEL)
	part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'ERRORE - MIGRAZIONE', COLONNA_NOTE_EXCEL)
	KeywordUtil.markFailedAndStop('ERRORE - Codice di migrazione non valido')
	
}

WebUI.scrollToElement(ETICHETTA_CODICE_RICONOSCIUTO_2, 5)
WebUI.waitForElementVisible(ETICHETTA_CODICE_RICONOSCIUTO_2, 15)


WebUI.delay(1)

TestObject PULSANTE_AVANTI_MIGRAZIONE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='NewLine/Migration_nextBtn']/p")
WebUI.waitForElementPresent(PULSANTE_AVANTI_MIGRAZIONE, 30)
WebUI.scrollToElement(PULSANTE_AVANTI_MIGRAZIONE, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_MIGRAZIONE, 30)
WebUI.delay(1)
CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(PULSANTE_AVANTI_MIGRAZIONE, 30)

}


if ((LINEA_ATTIVA=='SI' & SERVIZIO_ATTIVO!='Voce' & TECNOLOGIA_SCENARIO=='NON FIBRA' ) || (LINEA_ATTIVA=='SI' & SERVIZIO_ATTIVO=='Voce' & VUOI_NUOVO_NUMERO_SKY=='No' & TIPO_OFFERTA=='BB+VOCE') ) {

part_MIGRAZIONE()

}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////NUMBER PORTABILITY///////////////////////////////////////////////////////////////////


void part_NUMBER_PORTABILITY() {
	
		WebUI.switchToDefaultContent()
	
		WebUI.delay(1)

		TestObject FRAME_NUMBER_PORTABILITY = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//iframe[contains(@src,'OmniConsoleDisplayScriptLightning?')]")
		WebUI.waitForElementPresent(FRAME_NUMBER_PORTABILITY, 20)
		WebUI.switchToFrame(FRAME_NUMBER_PORTABILITY, 5)
						
		part_WAIT_SPINNER()
	
		TestObject PULSANTE_VERIFICA_NUMERO_DA_PORTARE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='VerificaNumero']/p")
		WebUI.waitForElementPresent(PULSANTE_VERIFICA_NUMERO_DA_PORTARE, 30)
		WebUI.scrollToElement(PULSANTE_VERIFICA_NUMERO_DA_PORTARE, 5)
		WebUI.waitForElementClickable(PULSANTE_VERIFICA_NUMERO_DA_PORTARE, 30)
		CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(PULSANTE_VERIFICA_NUMERO_DA_PORTARE, 30)

		
		WebUI.delay(1)
		
		part_WAIT_SPINNER()
		
		
		TestObject ETICHETTA_POSSIBILITA_NP= new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='message description ng-binding ng-scope' and contains(text(),'possibile effettuare la portabilità sul numero inserito')]")

		
		if (!WebUI.verifyElementPresent(ETICHETTA_POSSIBILITA_NP, 10, FailureHandling.OPTIONAL)) {
			
			part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'Impossibile procede con la portabilità di questo numero all’indirizzo richiesto', COLONNA_NOTE_NP_EXCEL)
			part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'ERRORE - NP', COLONNA_NOTE_EXCEL)
			KeywordUtil.markFailedAndStop('ERRORE - Impossibile procede con la portabilità di questo numero all’indirizzo richiesto')
			
		} else {
			
		part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', "E' possibile effettuare la portabilità sul numero inserito", COLONNA_NOTE_NP_EXCEL)
		log.logInfo("E' possibile effettuare la portabilità sul numero inserito")
		
		WebUI.waitForElementPresent(ETICHETTA_POSSIBILITA_NP, 15)
		WebUI.scrollToElement(ETICHETTA_POSSIBILITA_NP, 5)
		WebUI.waitForElementVisible(ETICHETTA_POSSIBILITA_NP, 15)
		
		}
	
	
	WebUI.delay(1)
	
	TestObject PULSANTE_AVANTI_PORTABILITA_NUMERO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='PortabiNumero_nextBtn']/p")
	WebUI.waitForElementPresent(PULSANTE_AVANTI_PORTABILITA_NUMERO, 30)
	WebUI.scrollToElement(PULSANTE_AVANTI_PORTABILITA_NUMERO, 5)
	WebUI.waitForElementClickable(PULSANTE_AVANTI_PORTABILITA_NUMERO, 30)
	WebUI.delay(1)
	CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(PULSANTE_AVANTI_PORTABILITA_NUMERO, 30)
	
}



if (VUOI_NUOVO_NUMERO_SKY=='No' & TIPO_OFFERTA=='BB+VOCE') {
	
	part_NUMBER_PORTABILITY()
	
	}


/////////////////////////////////////////////////////////////////////////////////////////////


WebUI.switchToDefaultContent()

///////////////////////////////////////////NUMERO DI CONTATTO///////////////////////////////////////////////////////////////////

void part_NUMERO_DI_CONTATTO() {

TestObject FRAME_NUMERO_DI_CONTATTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div[1]/div[3]/div/div[2]/div[2]/div[2]/div/div[3]/div/iframe')
WebUI.waitForElementPresent(FRAME_NUMERO_DI_CONTATTO, 200)
WebUI.switchToFrame(FRAME_NUMERO_DI_CONTATTO, 5)

part_WAIT_SPINNER()

TestObject RADIO_BUTTON_CONTATTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//label[text()='$TIPO_DI_CONTATTO']/../input")
WebUI.waitForElementPresent(RADIO_BUTTON_CONTATTO, 200)
WebUI.scrollToElement(RADIO_BUTTON_CONTATTO, 5)
WebUI.waitForElementClickable(RADIO_BUTTON_CONTATTO, 200)

CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(RADIO_BUTTON_CONTATTO, 30)

WebUI.delay(1)

TestObject INSERT_NUMERO_CONTATTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//label[text()='$TIPO_DI_CONTATTO']/../following-sibling::td/div/input")
WebUI.waitForElementPresent(INSERT_NUMERO_CONTATTO, 200)
WebUI.scrollToElement(INSERT_NUMERO_CONTATTO, 5)
WebUI.waitForElementVisible(INSERT_NUMERO_CONTATTO, 300)
WebUI.waitForElementClickable(INSERT_NUMERO_CONTATTO, 200)
WebUI.setText(INSERT_NUMERO_CONTATTO, NUMERO_CONTATTO)

WebUI.delay(1)
           
TestObject PULSANTE_AVANTI_CONTATTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='NumeroContatto_nextBtn']/p")
WebUI.waitForElementPresent(PULSANTE_AVANTI_CONTATTO, 200)
WebUI.scrollToElement(PULSANTE_AVANTI_CONTATTO, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_CONTATTO, 200)
WebUI.click(PULSANTE_AVANTI_CONTATTO)

}

part_NUMERO_DI_CONTATTO()

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WebUI.switchToDefaultContent()

///////////////////////////////////////////VERIFICA DOCUMENTO///////////////////////////////////////////////////////////////////

void part_VERIFICA_DOCUMENTO() {

TestObject FRAME_DOCUMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div[1]/div[3]/div/div[2]/div[2]/div[2]/div/div[3]/div/iframe')
WebUI.waitForElementPresent(FRAME_DOCUMENTO, 200)
WebUI.switchToFrame(FRAME_DOCUMENTO, 5)

part_WAIT_SPINNER()

TestObject BOX_TIPO_DOCUMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//select[@id='DocumentType']")
WebUI.waitForElementPresent(BOX_TIPO_DOCUMENTO, 5)
WebUI.selectOptionByValue(BOX_TIPO_DOCUMENTO, TIPO_DOCUMENTO, true)

WebUI.delay(1)

TestObject BOX_NUMERO_DOCUMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='DocumentNumber']")
WebUI.setText(BOX_NUMERO_DOCUMENTO, NUMERO_DOCUMENTO)

WebUI.delay(1)

TestObject BOX_DATA_SCADENZA_DOCUMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='DocumentExpDate']")
WebUI.setText(BOX_DATA_SCADENZA_DOCUMENTO, DATA_SCADENZA_DOCUMENTO)

WebUI.delay(1)

TestObject BOX_RILASCIATO_DA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//select[@id='DocumentReleasedBy']")
WebUI.waitForElementPresent(BOX_RILASCIATO_DA, 5)
WebUI.selectOptionByValue(BOX_RILASCIATO_DA, LUOGO_RILASCIO_DOCUMENTO, true)

WebUI.delay(1)

TestObject BOX_DATA_RILASCIO_DOCUMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='DocumentReleasedDate']")
WebUI.setText(BOX_DATA_RILASCIO_DOCUMENTO, DATA_RILASCIO_DOCUMENTO)

WebUI.delay(1)

TestObject PULSANTE_VERIFICA_DOCUMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='VerifyDocument_BTN']/p")
WebUI.waitForElementPresent(PULSANTE_VERIFICA_DOCUMENTO, 200)
WebUI.scrollToElement(PULSANTE_VERIFICA_DOCUMENTO, 5)
WebUI.waitForElementClickable(PULSANTE_VERIFICA_DOCUMENTO, 200)
WebUI.click(PULSANTE_VERIFICA_DOCUMENTO)

WebUI.delay(1)

part_WAIT_SPINNER()

WebUI.delay(1)

TestObject STRINGA_VERIFICA_DOCUMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//strong[text()='Documento valido o preso in carico per verifica.']")
WebUI.waitForElementPresent(STRINGA_VERIFICA_DOCUMENTO, 200)
WebUI.waitForElementVisible(STRINGA_VERIFICA_DOCUMENTO, 200)

WebUI.delay(1)

TestObject PULSANTE_AVANTI_DOCUMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='DocumentCheck_nextBtn']/p")
WebUI.waitForElementPresent(PULSANTE_AVANTI_DOCUMENTO, 200)
WebUI.scrollToElement(PULSANTE_AVANTI_DOCUMENTO, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_DOCUMENTO, 200)
WebUI.click(PULSANTE_AVANTI_DOCUMENTO)

}

part_VERIFICA_DOCUMENTO()


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WebUI.switchToDefaultContent()

///////////////////////////////////////////////////////PRIVACY///////////////////////////////////////////////////////////////////

void part_PRIVACY() {

TestObject FRAME_PRIVACY = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div/div[3]/div/div[2]/div[2]/div[2]/div/div[3]/div/iframe')
WebUI.waitForElementPresent(FRAME_PRIVACY, 200)
WebUI.switchToFrame(FRAME_PRIVACY, 5)

part_WAIT_SPINNER()

WebUI.delay(1)

TestObject CHECKBOX_CONSENSI_1 = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='FlagConsensi4']/../span")

if (WebUI.verifyElementPresent(CHECKBOX_CONSENSI_1, 30, FailureHandling.OPTIONAL)) {
	
	WebUI.waitForElementPresent(CHECKBOX_CONSENSI_1, 3)
	WebUI.scrollToElement(CHECKBOX_CONSENSI_1, 5)
	WebUI.waitForElementClickable(CHECKBOX_CONSENSI_1, 3)
	WebUI.click(CHECKBOX_CONSENSI_1)	
}


WebUI.delay(1)

TestObject CHECKBOX_CONSENSI_2 = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='FlagConsensi5']/../span")

if (WebUI.verifyElementPresent(CHECKBOX_CONSENSI_2, 3, FailureHandling.OPTIONAL)) {
	
WebUI.waitForElementPresent(CHECKBOX_CONSENSI_2, 3)
WebUI.scrollToElement(CHECKBOX_CONSENSI_2, 5)
WebUI.waitForElementClickable(CHECKBOX_CONSENSI_2, 3)
WebUI.click(CHECKBOX_CONSENSI_2)

}


WebUI.delay(1)

TestObject CHECKBOX_CONSENSI_3 = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='FlagConsensi6']/../span")

if (WebUI.verifyElementPresent(CHECKBOX_CONSENSI_3, 3, FailureHandling.OPTIONAL)) {
	
WebUI.waitForElementPresent(CHECKBOX_CONSENSI_3, 3)
WebUI.scrollToElement(CHECKBOX_CONSENSI_3, 5)
WebUI.waitForElementClickable(CHECKBOX_CONSENSI_3, 3)
WebUI.click(CHECKBOX_CONSENSI_3)

}


WebUI.delay(1)

TestObject PULSANTE_AVANTI_PRIVACY = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='Privacy_nextBtn']/p")
WebUI.waitForElementPresent(PULSANTE_AVANTI_PRIVACY, 50)
WebUI.scrollToElement(PULSANTE_AVANTI_PRIVACY, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_PRIVACY, 10)
WebUI.click(PULSANTE_AVANTI_PRIVACY)

}

part_PRIVACY()

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WebUI.switchToDefaultContent()

///////////////////////////////////////////////////////INDIRIZZO SPEDIZIONE///////////////////////////////////////////////////////////////////

void part_INDIRIZZO_SPEDIZIONE() {

TestObject FRAME_INDIRIZZO_SPEDIZIONE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//iframe[contains(@src,'OmniConsoleDisplayScriptLightning')]")
WebUI.waitForElementPresent(FRAME_INDIRIZZO_SPEDIZIONE, 200)
WebUI.switchToFrame(FRAME_INDIRIZZO_SPEDIZIONE, 5)

part_WAIT_SPINNER()

WebUI.delay(1)

TestObject PULSANTE_AVANTI_INDIRIZZO_SPEDIZIONE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='IndirizzoSpedizione_nextBtn']/p")
WebUI.waitForElementPresent(PULSANTE_AVANTI_INDIRIZZO_SPEDIZIONE, 120)
WebUI.scrollToElement(PULSANTE_AVANTI_INDIRIZZO_SPEDIZIONE, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_INDIRIZZO_SPEDIZIONE, 120)
WebUI.click(PULSANTE_AVANTI_INDIRIZZO_SPEDIZIONE)

}

if (TECNOLOGIA_SCENARIO=='NON FIBRA'){
	part_INDIRIZZO_SPEDIZIONE()
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WebUI.switchToDefaultContent()

///////////////////////////////////////////////////DIRITTO DI RECESSO///////////////////////////////////////////////////////////

void part_DIRITTO_DI_RECESSO() {

TestObject FRAME_DIRITTO_DI_RECESSO = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div/div[3]/div/div[2]/div[2]/div[2]/div/div[3]/div/iframe')
WebUI.waitForElementPresent(FRAME_DIRITTO_DI_RECESSO, 200)
WebUI.switchToFrame(FRAME_DIRITTO_DI_RECESSO, 5)

part_WAIT_SPINNER()

WebUI.delay(1)

if (ATTIVARE_SUBITO_SERVIZIO=='SI') {
	
	TestObject CHECKBOX_ATTIVARE_SUBITO_SERVIZIO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//input[@id='Withdrawal']/../span")
	WebUI.waitForElementPresent(CHECKBOX_ATTIVARE_SUBITO_SERVIZIO, 200)
	WebUI.scrollToElement(CHECKBOX_ATTIVARE_SUBITO_SERVIZIO, 5)
	WebUI.waitForElementClickable(CHECKBOX_ATTIVARE_SUBITO_SERVIZIO, 200)
	WebUI.click(CHECKBOX_ATTIVARE_SUBITO_SERVIZIO)

}

TestObject PULSANTE_DIRITTO_RIPENSAMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='Step3_nextBtn']/p")
WebUI.waitForElementPresent(PULSANTE_DIRITTO_RIPENSAMENTO, 200)
WebUI.scrollToElement(PULSANTE_DIRITTO_RIPENSAMENTO, 5)
WebUI.waitForElementClickable(PULSANTE_DIRITTO_RIPENSAMENTO, 200)
WebUI.click(PULSANTE_DIRITTO_RIPENSAMENTO)

}

part_DIRITTO_DI_RECESSO()

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WebUI.switchToDefaultContent()

///////////////////////////////////////////////////APPUNTAMENTO////////////////////////////////////////////////////////////////

void part_APPUNTAMENTO() {

TestObject FRAME_APPUNTAMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//iframe[contains(@src,'/apex/OmniConsoleDisplayScriptLightning?')]")
WebUI.waitForElementPresent(FRAME_APPUNTAMENTO, 100)										
WebUI.switchToFrame(FRAME_APPUNTAMENTO, 5)

part_WAIT_SPINNER()

WebUI.delay(1)

TestObject MANCATA_PRENOTAZIONE_APPUNTAMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//strong[contains(text(),'Non è possibile procedere con la prenotazione dell'appuntamento')]")
WebUI.waitForElementPresent(MANCATA_PRENOTAZIONE_APPUNTAMENTO, 5)

if (!WebUI.verifyElementPresent(MANCATA_PRENOTAZIONE_APPUNTAMENTO, 5, FailureHandling.OPTIONAL)) {
	
def String MancatoAppuntamento='NO'	

if (APP_GIORNO_DISPONIBILE=='') {
	
	if (APP_ORARIO_DALLE=='') {
		
		TestObject BOX_NO_GIORNO_NO_ORA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//article[@class='slds-card slds-size--1-of-6 ng-scope'][1]/footer/div[1]")
		WebUI.waitForElementPresent(BOX_NO_GIORNO_NO_ORA, 5)
		
		if (!WebUI.verifyElementPresent(BOX_NO_GIORNO_NO_ORA, 5, FailureHandling.OPTIONAL)) {
			part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', "L'appuntamento indicato non è presente", COLONNA_NOTE_APPUNTAMENTO)
			log.logInfo("Appuntamento non presente")		
			MancatoAppuntamento='SI'
			
		} else {
		WebUI.scrollToElement(BOX_NO_GIORNO_NO_ORA, 5)
		WebUI.waitForElementClickable(BOX_NO_GIORNO_NO_ORA, 5)
		WebUI.click(BOX_NO_GIORNO_NO_ORA)	
		}
		
	} else {
	
		TestObject BOX_NO_GIORNO_SI_ORA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//article[@class='slds-card slds-size--1-of-6 ng-scope'][1]/footer/div[contains(text(),'Dalle $APP_ORARIO_DALLE')]")
		WebUI.waitForElementPresent(BOX_NO_GIORNO_SI_ORA, 5)
		
		if (!WebUI.verifyElementPresent(BOX_NO_GIORNO_SI_ORA, 5, FailureHandling.OPTIONAL)) {
			part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', "L'appuntamento indicato non è presente", COLONNA_NOTE_APPUNTAMENTO)
			log.logInfo("Appuntamento non presente")
			MancatoAppuntamento='SI'
			
		} else {
		WebUI.scrollToElement(BOX_NO_GIORNO_SI_ORA, 5)
		WebUI.waitForElementClickable(BOX_NO_GIORNO_SI_ORA, 5)
		WebUI.click(BOX_NO_GIORNO_SI_ORA)
		}
	}	

} else {

	if (APP_ORARIO_DALLE=='') {
		
		TestObject BOX_SI_GIORNO_NO_ORA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//article[@class='slds-card slds-size--1-of-6 ng-scope'][$APP_GIORNO_DISPONIBILE]/footer/div[1]")
		WebUI.waitForElementPresent(BOX_SI_GIORNO_NO_ORA, 5)
		
		if (!WebUI.verifyElementPresent(BOX_SI_GIORNO_NO_ORA, 5, FailureHandling.OPTIONAL)) {
			part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', "L'appuntamento indicato non è presente", COLONNA_NOTE_APPUNTAMENTO)
			log.logInfo("Appuntamento non presente")
			MancatoAppuntamento='SI'
			
		} else {
		WebUI.scrollToElement(BOX_SI_GIORNO_NO_ORA, 5)
		WebUI.waitForElementClickable(BOX_SI_GIORNO_NO_ORA, 5)
		WebUI.click(BOX_SI_GIORNO_NO_ORA)		
		}
		
	} else {
	
		TestObject BOX_SI_GIORNO_SI_ORA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//article[@class='slds-card slds-size--1-of-6 ng-scope'][$APP_GIORNO_DISPONIBILE]/footer/div[contains(text(),'Dalle $APP_ORARIO_DALLE')]")
		WebUI.waitForElementPresent(BOX_SI_GIORNO_SI_ORA, 5)
		
		if (!WebUI.verifyElementPresent(BOX_SI_GIORNO_SI_ORA, 5, FailureHandling.OPTIONAL)) {
			part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', "L'appuntamento indicato non è presente", COLONNA_NOTE_APPUNTAMENTO)
			log.logInfo("Appuntamento non presente")
			MancatoAppuntamento='SI'
			
		} else {
		WebUI.scrollToElement(BOX_SI_GIORNO_SI_ORA, 5)
		WebUI.waitForElementClickable(BOX_SI_GIORNO_SI_ORA, 5)
		WebUI.click(BOX_SI_GIORNO_SI_ORA)
		}
	}

}

WebUI.delay(1)

if (MancatoAppuntamento=='NO') {

TestObject PULSANTE_SCEGLI_APPUNTAMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//button[contains(text(),'Scegli')]")
WebUI.waitForElementPresent(PULSANTE_SCEGLI_APPUNTAMENTO, 30)
WebUI.scrollToElement(PULSANTE_SCEGLI_APPUNTAMENTO, 5)
WebUI.waitForElementClickable(PULSANTE_SCEGLI_APPUNTAMENTO, 30)
WebUI.click(PULSANTE_SCEGLI_APPUNTAMENTO)

WebUI.delay(1)

TestObject PULSANTE_CONFERMA_APPUNTAMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='ReserveAppointment_RM']//p[contains(text(),'Conferma')]")
WebUI.waitForElementPresent(PULSANTE_CONFERMA_APPUNTAMENTO, 30)
WebUI.scrollToElement(PULSANTE_CONFERMA_APPUNTAMENTO, 5)
WebUI.waitForElementClickable(PULSANTE_CONFERMA_APPUNTAMENTO, 30)
WebUI.click(PULSANTE_CONFERMA_APPUNTAMENTO)


part_WAIT_SPINNER()

WebUI.delay(1)

TestObject SPUNTA_VERDE_APPUNTAMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//img[@src='https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Green_check.svg/480px-Green_check.svg.png']")
WebUI.waitForElementPresent(SPUNTA_VERDE_APPUNTAMENTO, 10)
WebUI.waitForElementVisible(SPUNTA_VERDE_APPUNTAMENTO, 10)

if (WebUI.verifyElementPresent(SPUNTA_VERDE_APPUNTAMENTO, 5, FailureHandling.OPTIONAL)) {
	
	WebUI.delay(1)
	
	TestObject SCRITTA_APPUNTAMENTO_CONFERMATO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//span[contains(text(),'Appuntamento confermato')]")
	TestObject SCRITTA_SLOT = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//span[contains(text(),'Lo slot')]")
	
	def String PRIMA_PARTE= WebUI.getText(SCRITTA_APPUNTAMENTO_CONFERMATO)
	def String SECONDA_PARTE= WebUI.getText(SCRITTA_SLOT)
	
	def String STRINGA_APPUNTAMENTO= PRIMA_PARTE + ' - ' + SECONDA_PARTE
	
	part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', STRINGA_APPUNTAMENTO, COLONNA_NOTE_APPUNTAMENTO)
	
	} else {
	
	part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'Appuntamento non confermato', COLONNA_NOTE_APPUNTAMENTO)
	log.logInfo('Appuntamento non confermato')
	
	}

}

} else {

part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', 'Appuntamento non preso - non sono stati restituiti gli appuntamenti', COLONNA_NOTE_APPUNTAMENTO)
log.logInfo('Appuntamento non preso')


}

TestObject PULSANTE_AVANTI_APPUNTAMENTO = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='Appuntamento_nextBtn']/p")
WebUI.waitForElementPresent(PULSANTE_AVANTI_APPUNTAMENTO, 30)
WebUI.scrollToElement(PULSANTE_AVANTI_APPUNTAMENTO, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_APPUNTAMENTO, 30)
WebUI.click(PULSANTE_AVANTI_APPUNTAMENTO)

}

if (TECNOLOGIA_SCENARIO=='FIBRA'){
	part_APPUNTAMENTO()
}




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WebUI.switchToDefaultContent()

///////////////////////////////////////////////////CONFERMA ORDINE////////////////////////////////////////////////////////////////

void part_CONFERMA_ORDINE() {
TestObject FRAME_CONFERMA_ORDINE = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div/div[3]/div/div[2]/div[2]/div[2]/div/div[3]/div/iframe')
WebUI.waitForElementPresent(FRAME_CONFERMA_ORDINE, 200)
WebUI.switchToFrame(FRAME_CONFERMA_ORDINE, 5)

part_WAIT_SPINNER()

WebUI.delay(2)

TestObject PULSANTE_AVANTI_RIEPILOGO_ORDINE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='ReviewOrder_nextBtn']/p")
WebUI.waitForElementPresent(PULSANTE_AVANTI_RIEPILOGO_ORDINE, 200)
WebUI.scrollToElement(PULSANTE_AVANTI_RIEPILOGO_ORDINE, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_RIEPILOGO_ORDINE, 200)
WebUI.click(PULSANTE_AVANTI_RIEPILOGO_ORDINE)

}
 
part_CONFERMA_ORDINE()


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WebUI.switchToDefaultContent()

///////////////////////////////////////////////////CANALE COMUNICAZIONE////////////////////////////////////////////////////////////////

void part_CANALE_COMUNICAZIONE() {

TestObject FRAME_CANALE_COMUNICAZIONI = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div/div[3]/div/div[2]/div[2]/div[2]/div/div[3]/div/iframe')
WebUI.waitForElementPresent(FRAME_CANALE_COMUNICAZIONI, 200)
WebUI.switchToFrame(FRAME_CANALE_COMUNICAZIONI, 5)

part_WAIT_SPINNER()

TestObject BOX_NOTIFICA_FATTURA = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//span[contains(text(),'$RICEVERE_NOTIFICA_FATTURA')]/../input")
WebUI.waitForElementPresent(BOX_NOTIFICA_FATTURA, 200)
WebUI.scrollToElement(BOX_NOTIFICA_FATTURA, 5)
WebUI.waitForElementClickable(BOX_NOTIFICA_FATTURA, 200)  
CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(BOX_NOTIFICA_FATTURA, 30)

WebUI.delay(1)

TestObject BOX_PREFERENZA_CANALE_COMUNICAZIONE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//select[@id='ChannelSelection']")
WebUI.waitForElementPresent(BOX_PREFERENZA_CANALE_COMUNICAZIONE, 5)
WebUI.selectOptionByLabel(BOX_PREFERENZA_CANALE_COMUNICAZIONE, CANALE_COMUNICAZIONE, true)

WebUI.delay(1)

TestObject PULSANTE_AVANTI_CANALE_DI_COMUNICAZIONE = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@id='CommunicationChannelSelection_nextBtn']/p")
WebUI.waitForElementPresent(PULSANTE_AVANTI_CANALE_DI_COMUNICAZIONE, 200)
WebUI.scrollToElement(PULSANTE_AVANTI_CANALE_DI_COMUNICAZIONE, 5)
WebUI.waitForElementClickable(PULSANTE_AVANTI_CANALE_DI_COMUNICAZIONE, 200)
WebUI.click(PULSANTE_AVANTI_CANALE_DI_COMUNICAZIONE)

TestObject SPINNER = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//div[@class='slds-spinner_container ng-scope']/..")
while (WebUI.verifyElementPresent(SPINNER, 2,FailureHandling.OPTIONAL))
{
	WebUI.delay(1)
}

WebUI.delay(1)


WebUI.switchToDefaultContent()

TestObject FRAME_PORTALE = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div/div[3]/div/div[2]/div[2]/div[2]/div/div[2]/div/iframe')
if (WebUI.verifyElementPresent(FRAME_PORTALE, 2,FailureHandling.OPTIONAL)){
	WebUI.waitForElementPresent(FRAME_PORTALE, 60)
	WebUI.switchToFrame(FRAME_PORTALE, 5)
}

WebUI.delay(2)

TestObject PULSANTE_UPSELL_BB_POST_UPSELL = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//button[contains(@title,'BB')]")
if (WebUI.verifyElementPresent(PULSANTE_UPSELL_BB_POST_UPSELL, 2,FailureHandling.OPTIONAL)){
	WebUI.waitForElementPresent(PULSANTE_UPSELL_BB_POST_UPSELL, 60, FailureHandling.OPTIONAL)
	WebUI.scrollToElement(PULSANTE_UPSELL_BB_POST_UPSELL, 5)
	}
}

part_CANALE_COMUNICAZIONE()

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

WebUI.switchToDefaultContent()

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

part_CHIUDI_I_TAB()


part_CREA_ITERAZIONE_RICERCA_CONTRATTO()


WebUI.switchToDefaultContent()

//TestObject FRAME_PORTALE = new TestObject().addProperty('xpath', ConditionType.EQUALS, '/html/body/div[4]/div/div[3]/div/div[2]/div[2]/div[2]/div/div[2]/div/iframe')
WebUI.waitForElementPresent(FRAME_PORTALE, 60)

if (!WebUI.verifyElementPresent(FRAME_PORTALE, 10, FailureHandling.OPTIONAL)) {
	
	part_CHIUDI_I_TAB()
	
	part_CREA_ITERAZIONE_RICERCA_CONTRATTO()
	
	WebUI.switchToDefaultContent()
	
	WebUI.waitForElementPresent(FRAME_PORTALE, 60)
}

WebUI.switchToFrame(FRAME_PORTALE, 5)

TestObject PULSANTE_CONTRATTO_BB = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//td[contains(text(),'TELCO')]/../td[@class='scndTableCell']/div")
WebUI.waitForElementPresent(PULSANTE_CONTRATTO_BB, 60)
WebUI.scrollToElement(PULSANTE_CONTRATTO_BB, 5)
WebUI.waitForElementVisible(PULSANTE_CONTRATTO_BB, 60)

CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(PULSANTE_CONTRATTO_BB, 30)

WebUI.waitForElementAttributeValue(WAIT, 'style', 'display: none;',	60)

TestObject CONTRATTO_BB = new TestObject().addProperty('xpath', ConditionType.EQUALS, "//li[@class='active']/a[@class='openCtrTab']")
WebUI.waitForElementPresent(CONTRATTO_BB, 60)
WebUI.scrollToElement(CONTRATTO_BB, 5)
WebUI.waitForElementVisible(CONTRATTO_BB, 60)

def String NUMERO_CONTRATTO_BB= WebUI.getText(CONTRATTO_BB)

part_SCRIVI_SU_FILE('Data Files/SKY/INPUT/INPUT_SKY', NUMERO_CONTRATTO_BB, COLONNA_CONTRATTO_BB_EXCEL)


WebUI.delay(1)

WebUI.closeBrowser()



























