import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

def row_canale

def max_row_num_canale = findTestData('CANALI').getRowNumbers()

for (def rowNum_canale = 1; rowNum_canale <= max_row_num_canale; rowNum_canale++) {
    def can = findTestData('CANALI').getValue(1, rowNum_canale)

    if (can == CANALE) {
        row_canale = rowNum_canale
    }
}

switch (CATENA) {
    case '3A':
        WebUI.navigateToUrl('https://test.salesforce.com/?locale=it')

        WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Nome utente_username'), 'm.candiani@reply.com.qafull3a')

        WebUI.setEncryptedText(findTestObject('Page_Accedi  Salesforce/input_Password_pw'), 'iBox6NXRWvUTXBq0Xh7Cnw==')

        WebUI.click(findTestObject('Page_Accedi  Salesforce/input_Password_Login'))

        WebUI.delay(2)

        WebUI.navigateToUrl(findTestData('CANALI').getValue(3, row_canale))

        break
    case '3C':
        WebUI.navigateToUrl('https://test.salesforce.com/?locale=it')

        WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Nome utente_username'), 'm.candiani@reply.it.qafull3c')

        WebUI.setEncryptedText(findTestObject('Page_Accedi  Salesforce/input_Password_pw'), 'iBox6NXRWvUTXBq0Xh7Cnw==')

        WebUI.click(findTestObject('Page_Accedi  Salesforce/input_Password_Login'))

        WebUI.delay(2)

        WebUI.navigateToUrl(findTestData('CANALI').getValue(4, row_canale))

        break
}

def ACCESSO_UTENZA = findTestData('CANALI').getValue(7, row_canale)

switch (ACCESSO_UTENZA) {
    case 'COMMUNITY':
        WebUI.click(findTestObject('Page_Salesforce - Performance Editi/div_Manage External User'))

        WebUI.click(findTestObject('Page_Salesforce - Performance Editi/a_Log in to Community as User'))

        WebUI.click(findTestObject('Page_Partner Community  Partner Sal/a_Inserisci Ordine'))

        break
    case 'NO_COMMUNITY':
        WebUI.click(findTestObject('Page_Salesforce - Performance Editi/CLICK_Login_Utenza_NO_COMMUNITY'))

        WebUI.click(findTestObject('Page_Salesforce - Performance Editi/CLICK_Utenza_NO_COMMUNITY_Inserisci_Ordine'))

        break
}

WebUI.delay(6, FailureHandling.STOP_ON_FAILURE)

switch (SEGMENTO) {
    case 'RES':
        WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), findTestData('CANALI').getValue(5, row_canale), 
            FailureHandling.OPTIONAL)

        break
    case 'SHP':
        WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), findTestData('CANALI').getValue(6, row_canale), 
            FailureHandling.OPTIONAL)

        break
}

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/Selezione_comsy'), FailureHandling.OPTIONAL)

//--------------------------------------INIZIO DATI ANAGRAFICI----------------------------------------------
WebUI.waitForElementVisible(findTestObject('Page_Fastweb - Abbonamento Online/input_Nome_section1_nome'), 60)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiFiscali'), 5)

//WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Nome_section1_nome'), 5)
WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Nome_section1_nome'), NOME)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Cognome_section1_cognome'), COGNOME)

switch (SESSO) {
    case 'M':
        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_M_section1_sesso'))

        break
    case 'F':
        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_F_section1_sesso'))

        break
}

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Data di Nascita_section1'), DATA_DI_NASCITA)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Stato di Nascita_section'), NAZIONE_DI_NASCITA)

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', 
    NAZIONE_DI_NASCITA, true)

WebUI.click(Seleziona_new)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Comune di Nascita_sectio'), COMUNE_DI_NASCITA)

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', 
    COMUNE_DI_NASCITA, true)

WebUI.click(Seleziona_new)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numerazione Mobile Rappr'), NUMERO_MOBILE)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Mail_section1_email'), EMAIL)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Conferma Mail_section1_e'), EMAIL)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Codice Fiscale_section1_'), CODICE_FISCALE)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Codice Fiscale'), 5)

WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Codice Fiscale'))

WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Codice Fiscale'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Codice Fiscale'))

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiFiscali'), 5)

WebUI.delay(2)

//-----------------------------------------------FINE DATI ANAGRAFICI--------------------------------------------------------
//-----------------------------------------------INIZIO INDIRIZZO DI ATTIVAZIONE-----------------------------------------------
WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiAttivazione'), 5)

//WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Citta di attivazione'), 5)
def row_tec

def max_row_num_tec = findTestData('INDIRIZZI_TECNOLOGIE').getRowNumbers()

for (def rowNum_tec = 1; rowNum_tec <= max_row_num_tec; rowNum_tec++) {
    def tec = findTestData('INDIRIZZI_TECNOLOGIE').getValue(1, rowNum_tec)

    if (tec == TECNOLOGIA) {
        row_tec = rowNum_tec
    }
}

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Citta di attivazione'), findTestData('INDIRIZZI_TECNOLOGIE').getValue(
        2, row_tec))

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', 
    findTestData('INDIRIZZI_TECNOLOGIE').getValue(2, row_tec), true)

WebUI.click(Seleziona_new)

//WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/conferma_citta'))
WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Indirizzo di Attivazione'), findTestData('INDIRIZZI_TECNOLOGIE').getValue(
        3, row_tec))

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', 
    findTestData('INDIRIZZI_TECNOLOGIE').getValue(3, row_tec), true)

WebUI.click(Seleziona_new)

//WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/seleziona_indirizzo_attivazione'))
WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Civico di Attivazione_se (1)'), findTestData('INDIRIZZI_TECNOLOGIE').getValue(
        4, row_tec))

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', 
    findTestData('INDIRIZZI_TECNOLOGIE').getValue(5, row_tec), true)

WebUI.click(Seleziona_new)

//WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/seleziona_civico_attivazione'))
WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

//si puo anche commentare
WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Copertura (1)'), 5)

WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Copertura (1)'))

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Copertura (1)'))

WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'), 60)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'), 5)

WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'))

WebUI.delay(1)

WebUI.takeScreenshot()

// qui si puo fare screen della tecnologia
WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'))

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiAttivazione'), 5)

WebUI.delay(1)

//------------------------------------------------------------FINE INDIRIZZO DI ATTIVAZIONE-------------------------------------------------------------------
//-------------------------------------------------------------INIZIO DATI DI RESIDENZA-----------------------------------------------------------------------
WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'), 5)

WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Indirizzo di Atti (1)'), 60)

//WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Indirizzo di Atti (1)'), 5)
WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Indirizzo di Atti (1)'))

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Indirizzo di Atti (1)'))

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Dati Fiscali (1)'))

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Dati Fiscali (1)'))

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'), 5)

WebUI.delay(1)

//------------------------------------------------------------------FINE DATI DI RESIDENZA-----------------------------------------------------------------------
//------------------------------------------------------------------INIZIO SEZIONE DATI DI PAGAMENTO-------------------------------------------------------------
switch (METODO_PAGAMENTO) {
    case 'CARTA DI CREDITO':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Carta di Credito_section (1)'), 5)

        WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/input_Carta di Credito_section (1)'))

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Carta di Credito_section (1)'))

        break
    case 'CONTO CORRENTE':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Conto Corrente_Section (1)'), 5)

        WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/input_Conto Corrente_Section (1)'))

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Conto Corrente_Section (1)'))

        break
}

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

//--------------------------------------------------------------------FINE SEZIONE DATI DI PAGAMENTO-----------------------------------------------------------------
WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Vai al Carrello (1)'))

//seleziona offerta
def offerta_string = "//span[(text() = '$OFFERTA_FISSA')]/parent::td/parent::tr/td[5]"

def Seleziona_offerta_new = WebUI.modifyObjectProperty(findTestObject('Page_/Seleziona_Offerta_Fissa'), 'xpath', 'equals', 
    offerta_string, true)

WebUI.waitForElementClickable(Seleziona_offerta_new, 60)

//properties= Seleziona_offerta_new.getProperties()
//for (prop in properties) {
//	prop.name;
//	}
WebUI.click(Seleziona_offerta_new)

WebUI.delay(3)

WebUI.switchToWindowIndex(1)

//Pagina Coupon
WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'), 60)

WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'))

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'))

WebUI.switchToWindowIndex(0)

WebUI.waitForElementClickable(findTestObject('Page_Coupon/span_Procedi'), 60)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello - Opzioni'), 'style', 
    'display: none;', 60)

//----------------------------------------------INIZIO ACCORDION OPZIONI--------------------------------------------------------
//verifica tutti gli accordion prima di selezionare le opzioni
WebUI.scrollToElement(findTestObject('Page_Coupon/Open_Accordion_COUPON'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_COUPON'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Page_Coupon/Open_Accordion_COUPON'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Page_Coupon/Open_Accordion_OFFERTA'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_OFFERTA'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Page_Coupon/Open_Accordion_OFFERTA'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Page_Coupon/Open_Accordion_OPZIONI'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_OPZIONI'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Page_Coupon/Open_Accordion_OPZIONI'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Page_Coupon/Open_Accordion_APPARATI'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_APPARATI'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Page_Coupon/Open_Accordion_APPARATI'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_STS'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_STS'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_STS'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Page_Coupon/Open_Accordion_SOFTWARE'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SOFTWARE'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Page_Coupon/Open_Accordion_SOFTWARE'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_RSTP'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_RSTP'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_RSTP'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

Presenza_accordion = WebUI.verifyElementPresent(findTestObject('Page_Coupon/Open_Accordion_SCONTI'), 2, FailureHandling.OPTIONAL)

if (Presenza_accordion == true) {
    WebUI.scrollToElement(findTestObject('Page_Coupon/Open_Accordion_SCONTI'), 2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('Page_Coupon/Open_Accordion_SCONTI'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(1)
}

//----------------------------------------------FINE ACORDION OPZIONI-----------------------------------------------------------------------
//----------------------------------------------INIZIO SELEZIONA OPZIONI------------------------------------------------------------------------
if (OPZIONE_F_1 != '') {
    def opzione_string = "//span[text()='$OPZIONE_F_1']/preceding::input[1]"

    def Seleziona_opzione_new = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 
        'equals', opzione_string, true)

    def open_accordion_string = "//span[text()='$OPZIONE_F_1']/preceding::input[1]/preceding::div[contains(concat(' ', normalize-space(@class), ' '), ' main-content-box-header main-content-box-header-complex ')][1]/img"

    def open_accordion = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 'equals', 
        open_accordion_string, true)

    WebUI.click(open_accordion, FailureHandling.OPTIONAL)

    WebUI.waitForElementVisible(Seleziona_opzione_new, 5, FailureHandling.OPTIONAL)

    WebUI.delay(1)

    WebUI.click(Seleziona_opzione_new, FailureHandling.OPTIONAL)

    WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
        60)
}

if (OPZIONE_F_2 != '') {
    def opzione_string = "//span[text()='$OPZIONE_F_2']/preceding::input[1]"

    def Seleziona_opzione_new = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 
        'equals', opzione_string, true)

    def open_accordion_string = "//span[text()='$OPZIONE_F_2']/preceding::input[1]/preceding::div[contains(concat(' ', normalize-space(@class), ' '), ' main-content-box-header main-content-box-header-complex ')][1]/img"

    def open_accordion = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 'equals', 
        open_accordion_string, true)

    WebUI.click(open_accordion, FailureHandling.OPTIONAL)

    WebUI.waitForElementVisible(Seleziona_opzione_new, 5, FailureHandling.OPTIONAL)

    WebUI.delay(1)

    WebUI.click(Seleziona_opzione_new, FailureHandling.OPTIONAL)

    WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
        60)
}

if (OPZIONE_F_3 != '') {
    def opzione_string = "//span[text()='$OPZIONE_F_3']/preceding::input[1]"

    def Seleziona_opzione_new = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 
        'equals', opzione_string, true)

    def open_accordion_string = "//span[text()='$OPZIONE_F_3']/preceding::input[1]/preceding::div[contains(concat(' ', normalize-space(@class), ' '), ' main-content-box-header main-content-box-header-complex ')][1]/img"

    def open_accordion = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 'equals', 
        open_accordion_string, true)

    WebUI.click(open_accordion, FailureHandling.OPTIONAL)

    WebUI.waitForElementVisible(Seleziona_opzione_new, 5, FailureHandling.OPTIONAL)

    WebUI.delay(1)

    WebUI.click(Seleziona_opzione_new, FailureHandling.OPTIONAL)

    WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
        60)
}

if (OPZIONE_F_4 != '') {
    def opzione_string = "//span[text()='$OPZIONE_F_4']/preceding::input[1]"

    def Seleziona_opzione_new = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 
        'equals', opzione_string, true)

    def open_accordion_string = "//span[text()='$OPZIONE_F_4']/preceding::input[1]/preceding::div[contains(concat(' ', normalize-space(@class), ' '), ' main-content-box-header main-content-box-header-complex ')][1]/img"

    def open_accordion = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 'equals', 
        open_accordion_string, true)

    WebUI.click(open_accordion, FailureHandling.OPTIONAL)

    WebUI.waitForElementVisible(Seleziona_opzione_new, 5, FailureHandling.OPTIONAL)

    WebUI.delay(1)

    WebUI.click(Seleziona_opzione_new, FailureHandling.OPTIONAL)

    WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
        60)
}

if (OPZIONE_F_5 != '') {
    def opzione_string = "//span[text()='$OPZIONE_F_5']/preceding::input[1]"

    def Seleziona_opzione_new = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 
        'equals', opzione_string, true)

    def open_accordion_string = "//span[text()='$OPZIONE_F_5']/preceding::input[1]/preceding::div[contains(concat(' ', normalize-space(@class), ' '), ' main-content-box-header main-content-box-header-complex ')][1]/img"

    def open_accordion = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 'equals', 
        open_accordion_string, true)

    WebUI.click(open_accordion, FailureHandling.OPTIONAL)

    WebUI.waitForElementVisible(Seleziona_opzione_new, 5, FailureHandling.OPTIONAL)

    WebUI.delay(1)

    WebUI.click(Seleziona_opzione_new, FailureHandling.OPTIONAL)

    WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
        60)
}

//----------------------------------------------------------------FINE SELEZIONA OPZIONI--------------------------------------------------------------------
WebUI.waitForElementClickable(findTestObject('Page_Coupon/span_Procedi'), 60)

WebUI.click(findTestObject('Page_Coupon/span_Procedi'))

// FUORI CARRELLO
WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/span_Successivo'), 60)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
    60)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/span_Successivo'))

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

// FASE 2
//scrol su Completamento Dati Attivazione e Portabilità
WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Scala_section2_scala (1)'), 5)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Scala_section2_scala (1)'), '1')

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Piano_section2_piano (1)'), '1')

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Interno_section2_interno (1)'), '1')

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_di_ativazione'), 5)

WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_di_ativazione'), 
    5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_di_ativazione'))

switch (METODO_PAGAMENTO) {
    case 'CARTA DI CREDITO':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/select_Visa (1)'), 5)

        WebUI.selectOptionByValue(findTestObject('Page_Fastweb - Abbonamento Online/select_Visa (1)'), CIRCUITO, true)

        WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero Carta_section3_nu'), NUMERO_CARTA_CONTO)

        WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Data di Scadenza_section (1)'), '12/22')

        WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Pagamento (1)'), 
            60)

        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Pagamento (1)'), 5)

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Pagamento (1)'))

        break
    case 'CONTO CORRENTE':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Dati Clienti'), 5)

        WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Dati Clienti'))

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Dati Clienti'))

        WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
            60)

        // scroll su Completamento Dati Banca
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero conto corrente'), 5)

        WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero conto corrente'))

        WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero conto corrente'), NUMERO_CARTA_CONTO)

        break
}

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

//scroll su Documenti
WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Nazionalit Documento_sec (1)'), NAZIONALITA_DOCUMENTO)

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', 
    NAZIONALITA_DOCUMENTO, true)

WebUI.click(Seleziona_new)

//WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/strong_ITALIA (1)'))
WebUI.selectOptionByValue(findTestObject('Page_Fastweb - Abbonamento Online/select_Carta di identita (1)'), 'CARTA DI IDENTITA', 
    true)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero Documento_section (1)'), NUMERO_DOCUMENTO)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Localit di Rilascio Docu (1)'), CITTA_RILASCIO_DOCUMENTO)

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', 
    CITTA_RILASCIO_DOCUMENTO, true)

WebUI.click(Seleziona_new)

//WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/strong_MILANO (1)'))
WebUI.selectOptionByValue(findTestObject('Page_Fastweb - Abbonamento Online/select_Comune (1)'), 'COMUNE', true)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Data di Rilascio Documen (1)'), DATA_RILASCIO_DOCUMENTO)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Documento (1)'), 5)

WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Documento (1)'))

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Documento (1)'))

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoPubb (1)'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoPubb (1)'))

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoGest (1)'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoGest (1)'))

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoAnal (1)'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoAnal (1)'))

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoCessionDati'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoCessionDati'))

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiFiscali'), 0)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiFiscali'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiAttivazione'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiAttivazione'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_CompletamentoDati_IndirizzoCopertura'), 
    5)

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiPagamento'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiPagamento'))

WebUI.delay(1)

switch (METODO_PAGAMENTO) {
    case 'CARTA DI CREDITO':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_Completamentodati_MetodoPagamento'), 
            5)

        WebUI.delay(1)

        break
    case 'CONTO CORRENTE':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_Completamentodati_MetodoPagamento'), 
            5)

        WebUI.delay(1)

        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_CompletamentoDatiBanca'), 
            5)

        WebUI.delay(1)

        break
}

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_Documenti'), 5)

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Genera Riepilogo Ordine (1)'), 5)

WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Genera Riepilogo Ordine (1)'), 60)

WebUI.delay(1)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Genera Riepilogo Ordine (1)'))

def CANALE_DISTANZA = findTestData('CANALI').getValue(8, row_canale)

switch (CANALE_DISTANZA) {
    case 'SI':
        if (CANALE != 'AGENTE') {
            WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
                60, FailureHandling.CONTINUE_ON_FAILURE)

            WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi (1)'), FailureHandling.CONTINUE_ON_FAILURE)
        }
        
        WebUI.waitForElementVisible(findTestObject('Page_Fastweb - Abbonamento Online/button_Conferma (1)'), 60)

        WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Conferma (1)'), 60)

        WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
            60)

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Conferma (1)'))

        WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
            60)

        break
    case 'NO':
        WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Stampa_PDA'), 60)

        WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
            60)

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Stampa_PDA'))

        WebUI.closeWindowIndex(1)

        WebUI.switchToWindowIndex(0)

        WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
            60)

        WebUI.waitForElementVisible(findTestObject('Page_Fastweb - Abbonamento Online/button_Conferma_CANALI_NO_DISTANZA'), 
            60)

        WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Conferma_CANALI_NO_DISTANZA'), 
            60)

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Conferma_CANALI_NO_DISTANZA'))

        WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
            60)

        break
}

WebUI.delay(5)

WebUI.closeBrowser()

