import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.*
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.thoughtworks.selenium.webdriven.Windows as Windows
import groovy.transform.Field as Field
import selenium.webdriver.common.*
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import org.openqa.selenium.WebElement
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.apache.poi.ss.usermodel.Workbook as Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory as WorkbookFactory
import org.apache.poi.hssf.usermodel.HSSFCell as HSSFCell
import org.apache.poi.hssf.usermodel.HSSFCellStyle as HSSFCellStyle
import org.apache.poi.hssf.usermodel.HSSFDataFormat as HSSFDataFormat
import org.apache.poi.hssf.usermodel.HSSFRow as HSSFRow
import org.apache.poi.hssf.usermodel.HSSFSheet as HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook as HSSFWorkbook
import org.apache.poi.hssf.util.HSSFColor as HSSFColor
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.ss.usermodel.Sheet as Sheet



WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://qa.stella.cloud.c3.cgm.ag/')

TestObject REGISTRATIONN_START_BUTTON = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id="startRegistrationButton"]')
WebUI.waitForElementVisible(REGISTRATIONN_START_BUTTON, 5)
WebUI.waitForElementPresent(REGISTRATIONN_START_BUTTON, 5)
WebUI.click(REGISTRATIONN_START_BUTTON)

TestObject REGISTRATIONN_CODEPIN_INPUT = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@name="codePin"]')
WebUI.waitForElementVisible(REGISTRATIONN_CODEPIN_INPUT, 5)
WebUI.waitForElementPresent(REGISTRATIONN_CODEPIN_INPUT, 5)
WebUI.setText(REGISTRATIONN_CODEPIN_INPUT, 'iiii-0000-iiii')

WebUI.delay(1)

TestObject REGISTRATIONN_NEXT_BUTTON = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@id="next-button"]')
WebUI.waitForElementVisible(REGISTRATIONN_NEXT_BUTTON, 5)
WebUI.waitForElementPresent(REGISTRATIONN_NEXT_BUTTON, 5)
WebUI.click(REGISTRATIONN_NEXT_BUTTON)

TestObject REGISTRATIONN_USERNAME_INPUT = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@name="username"]')
WebUI.waitForElementVisible(REGISTRATIONN_USERNAME_INPUT, 5)
WebUI.waitForElementPresent(REGISTRATIONN_USERNAME_INPUT, 5)
WebUI.setText(REGISTRATIONN_USERNAME_INPUT, USERNAME)

TestObject REGISTRATIONN_PSW_INPUT = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@name="password"]')
WebUI.waitForElementVisible(REGISTRATIONN_PSW_INPUT, 5)
WebUI.waitForElementPresent(REGISTRATIONN_PSW_INPUT, 5)
WebUI.setText(REGISTRATIONN_PSW_INPUT, PSW)

WebUI.delay(1)

WebUI.click(REGISTRATIONN_NEXT_BUTTON)

TestObject REGISTRATIONN_WORKSTATIONNAME_INPUT = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@name="workstationName"]')
WebUI.waitForElementVisible(REGISTRATIONN_WORKSTATIONNAME_INPUT, 5)
WebUI.waitForElementPresent(REGISTRATIONN_WORKSTATIONNAME_INPUT, 5)
WebUI.setText(REGISTRATIONN_WORKSTATIONNAME_INPUT, 'WorkStation_Tommaso')

WebUI.click(REGISTRATIONN_NEXT_BUTTON)



TestObject REGISTRATIONN_GOTOLOGIN_BUTTON = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@data-automation="WSREGWIZ-SUCCESS-GO-TO-LOGIN-BUTTON"]')
WebUI.waitForElementVisible(REGISTRATIONN_GOTOLOGIN_BUTTON, 5)
WebUI.waitForElementPresent(REGISTRATIONN_GOTOLOGIN_BUTTON, 5)
WebUI.click(REGISTRATIONN_GOTOLOGIN_BUTTON)

WebUI.delay(1)

TestObject LOGIN_USERNAME_INPUT = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@name="username"]')
WebUI.waitForElementVisible(LOGIN_USERNAME_INPUT, 5)
WebUI.waitForElementPresent(LOGIN_USERNAME_INPUT, 5)
WebUI.setText(LOGIN_USERNAME_INPUT, USERNAME)

TestObject LOGIN_PSW_INPUT = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//input[@name="password"]')
WebUI.waitForElementVisible(LOGIN_PSW_INPUT, 5)
WebUI.waitForElementPresent(LOGIN_PSW_INPUT, 5)
WebUI.setText(LOGIN_PSW_INPUT, PSW)

WebUI.delay(1)

TestObject LOGIN_ACCEDI_BUTTON = new TestObject().addProperty('xpath', ConditionType.EQUALS, '//button[@data-automation="Login - Login button"]')
WebUI.waitForElementVisible(LOGIN_ACCEDI_BUTTON, 5)
WebUI.waitForElementPresent(LOGIN_ACCEDI_BUTTON, 5)
WebUI.click(LOGIN_ACCEDI_BUTTON)




//aggiunta di sviluppo di Tommyf
//alcune prove

//Aggiunta di un secodno svilupo di Tommy
//seconda prova


//verificare che si crei un nuovo ramo di sviluppo



//inserimento sviluppo su catena MAIN

















