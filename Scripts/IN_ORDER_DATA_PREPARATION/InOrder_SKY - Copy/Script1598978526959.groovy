import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.io.FileInputStream as FileInputStream
import java.io.FileOutputStream as FileOutputStream
import java.io.FileNotFoundException as FileNotFoundException
import java.io.IOException as IOException
import java.util.Date as Date
import org.apache.poi.hssf.usermodel.HSSFCell as HSSFCell
import org.apache.poi.hssf.usermodel.HSSFCellStyle as HSSFCellStyle
import org.apache.poi.hssf.usermodel.HSSFDataFormat as HSSFDataFormat
import org.apache.poi.hssf.usermodel.HSSFRow as HSSFRow
import org.apache.poi.hssf.usermodel.HSSFSheet as HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook as HSSFWorkbook
import org.apache.poi.hssf.util.HSSFColor as HSSFColor
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import org.apache.poi.openxml4j.exceptions.InvalidFormatException as InvalidFormatException
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.ss.usermodel.Workbook as Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory as WorkbookFactory
import org.apache.poi.ss.usermodel.Sheet as Sheet
import com.kms.katalon.core.exception.StepErrorException as StepErrorException
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.openBrowser('')

WebUI.maximizeWindow()

def String xpath=''

WebUI.navigateToUrl('https://arcadia2015--fullam.cs80.my.salesforce.com')

WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Nome utente_username'), 'barbara.stoia.nttdata@sales.sky.it.fullam')

WebUI.setEncryptedText(findTestObject('Page_Accedi  Salesforce/input_Password_pw'), 'ye0NSEGCGNLVDemxwLSgHA==')

WebUI.click(findTestObject('Page_Accedi  Salesforce/input_Password_Login'))

WebUI.delay(2)

xpath = '//span[contains(text(),\'Crea Interazione manuale\')]'
TestObject CREA_ITERAZIONE = new TestObject('CREA_ITERAZIONE')
CREA_ITERAZIONE.addProperty('xpath', ConditionType.EQUALS, xpath)

WebUI.waitForElementVisible(CREA_ITERAZIONE, 60)

xpath = '//input[@value=\'CREA\']'
TestObject PULSANTE_CREA_ITERAZIONE = new TestObject('PULSANTE_CREA_ITERAZIONE')
PULSANTE_CREA_ITERAZIONE.addProperty('xpath', ConditionType.EQUALS, xpath)

Presenza_pulsante_crea_iterazione = WebUI.verifyElementPresent(PULSANTE_CREA_ITERAZIONE, 3, FailureHandling.OPTIONAL)

WebUI.delay(20)

if (Presenza_pulsante_crea_iterazione == false) {

	//da inserire step per cancellare i tab e creare nuova iterazione
	
}

	//qui inserire step per verificare se è possibile fare l'upsell verificando sel il tasto BB è cliccabile



	//step per eseguire la modifica dell'indirizzo




def click = new ClickUsingJS()

def lavorazione_file_excel = new Lavorazione_file_excel()

WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_TERMINALI'), 2, FailureHandling.OPTIONAL)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_APPARATI'), FailureHandling.CONTINUE_ON_FAILURE)
click.clickUsingJS(findTestObject('Object_MOBILE/Open_Accordion_TERMINALI'), 60)

//WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_TERMINALI'), FailureHandling.OPTIONAL)
//def terminale_string = "//span[text()='$TERMINALE']/preceding::input[@type='radio'][1]"
def terminale_string = "//span[text()[contains(.,'$TERMINALE')]]/preceding::input[@type='radio'][1]"

def Seleziona_terminale_new = WebUI.modifyObjectProperty(findTestObject('/Object_MOBILE/Seleziona_Terminale'), 'xpath', 
    'equals', terminale_string, true)

String dynamicId = 'Katalon123'



TestObject to = new TestObject('objectName')

to.addProperty('xpath', ConditionType.EQUALS, xpath)

WebUI.click(to)

WebUI.navigateToUrl(findTestData('CANALI').getValue(3, row_canale))

def ACCESSO_UTENZA = findTestData('CANALI').getValue(7, row_canale)

switch (ACCESSO_UTENZA) {
    case 'COMMUNITY':
        WebUI.click(findTestObject('Page_Salesforce - Performance Editi/div_Manage External User'))

        WebUI.click(findTestObject('Page_Salesforce - Performance Editi/a_Log in to Community as User'))

        WebUI.click(findTestObject('Page_Partner Community  Partner Sal/a_Inserisci Ordine'))

        break
    case 'NO_COMMUNITY':
        WebUI.click(findTestObject('Page_Salesforce - Performance Editi/CLICK_Login_Utenza_NO_COMMUNITY'))

        WebUI.click(findTestObject('Page_Salesforce - Performance Editi/CLICK_Utenza_NO_COMMUNITY_Inserisci_Ordine'))

        break
}

WebUI.delay(3, FailureHandling.STOP_ON_FAILURE)

switch (SEGMENTO) {
    case 'RES':
        WebUI.waitForElementPresent(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), 60, FailureHandling.STOP_ON_FAILURE)

        if (COMSY == '') {
            WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), findTestData('CANALI').getValue(5, 
                    row_canale), FailureHandling.OPTIONAL)
        } else {
            WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), COMSY, FailureHandling.OPTIONAL)
        }
        
        break
    case 'SHP':
        WebUI.waitForElementPresent(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), 60, FailureHandling.STOP_ON_FAILURE)

        if (COMSY == '') {
            WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), findTestData('CANALI').getValue(6, 
                    row_canale), FailureHandling.OPTIONAL)
        } else {
            WebUI.setText(findTestObject('Page_Accedi  Salesforce/input_Tag_COMSY'), COMSY, FailureHandling.OPTIONAL)
        }
        
        break
}

WebUI.delay(1, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/Selezione_comsy'), FailureHandling.OPTIONAL)

//--------------------------------------------------------INIZIO PARTE ANAGRAFICA------------------------------------------------
WebUI.waitForElementVisible(findTestObject('Page_Fastweb - Abbonamento Online/input_Nome_section1_nome'), 60)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiFiscali'), 5)

//WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Nome_section1_nome'), 5)
WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Nome_section1_nome'), NOME)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Cognome_section1_cognome'), COGNOME)

switch (SESSO) {
    case 'M':
        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_M_section1_sesso'))

        break
    case 'F':
        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_F_section1_sesso'))

        break
}

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Data di Nascita_section1'), DATA_DI_NASCITA)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Stato di Nascita_section'), NAZIONE_DI_NASCITA)

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', 
    NAZIONE_DI_NASCITA, true)

WebUI.click(Seleziona_new)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Comune di Nascita_sectio'), COMUNE_DI_NASCITA)

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', 
    COMUNE_DI_NASCITA, true)

WebUI.click(Seleziona_new)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numerazione Mobile Rappr'), NUMERO_MOBILE)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Mail_section1_email'), EMAIL)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Conferma Mail_section1_e'), EMAIL)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Codice Fiscale_section1_'), CODICE_FISCALE)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Codice Fiscale'), 5)

WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Codice Fiscale'))

WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Codice Fiscale'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Codice Fiscale'))

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiFiscali'), 5)

switch (TIPOLOGIA_OFFERTA) {
    case 'FISSO':
        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Tipologia_Offerta_FISSO'), FailureHandling.OPTIONAL)

        break
    case 'FISSO E MOBILE':
        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Tipologia_Offerta_FISSO_E_MOBILE'), FailureHandling.OPTIONAL)

        break
    case 'MOBILE':
        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Tipologia_Offerta_MOBILE'), FailureHandling.OPTIONAL)

        break
}

WebUI.delay(1)

//-----------------------------------------------FINE DATI ANAGRAFICI--------------------------------------------------------
//-----------------------------------------------INIZIO INDIRIZZO DI ATTIVAZIONE-----------------------------------------------
/*WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiAttivazione'), 5)

//WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Citta di attivazione'), 5)
def row_tec

def max_row_num_tec = findTestData('INDIRIZZI_TECNOLOGIE').getRowNumbers()

for (def rowNum_tec = 1; rowNum_tec <= max_row_num_tec; rowNum_tec++) {
	def tec = findTestData('INDIRIZZI_TECNOLOGIE').getValue(1, rowNum_tec)

	if (tec == TECNOLOGIA) {
		row_tec = rowNum_tec
	}
}

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Citta di attivazione'), findTestData('INDIRIZZI_TECNOLOGIE').getValue(
		2, row_tec))

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
	findTestData('INDIRIZZI_TECNOLOGIE').getValue(2, row_tec), true)

WebUI.click(Seleziona_new)

//WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/conferma_citta'))
WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Indirizzo di Attivazione'), findTestData('INDIRIZZI_TECNOLOGIE').getValue(
		3, row_tec))

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
	findTestData('INDIRIZZI_TECNOLOGIE').getValue(3, row_tec), true)

WebUI.click(Seleziona_new)

//WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/seleziona_indirizzo_attivazione'))
WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Civico di Attivazione_se (1)'), findTestData('INDIRIZZI_TECNOLOGIE').getValue(
		4, row_tec))

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals',
	findTestData('INDIRIZZI_TECNOLOGIE').getValue(5, row_tec), true)

WebUI.click(Seleziona_new)

//WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/seleziona_civico_attivazione'))
WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
	60)

//si puo anche commentare
WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Copertura (1)'), 5)

WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Copertura (1)'))

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Copertura (1)'))

WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'), 60)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'), 5)

WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'))

WebUI.delay(1)

WebUI.takeScreenshot()

// qui si puo fare screen della tecnologia
WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Chiudi (1)'))

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiAttivazione'), 5)

WebUI.delay(1)

//------------------------------------------------------------FINE INDIRIZZO DI ATTIVAZIONE-------------------------------------------------------------------
*/
//-------------------------------------------------------------INIZIO DATI DI RESIDENZA-----------------------------------------------------------------------
WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'), 5)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

WebUI.setText(findTestObject('Object_MOBILE/input_Citta_RESIDENZA'), CITTA_DI_RESIDENZA)

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', 
    CITTA_DI_RESIDENZA, true)

WebUI.click(Seleziona_new)

WebUI.setText(findTestObject('Object_MOBILE/input_Indirizzo_RESIDENZA'), INDIRIZZO_DI_RESIDENZA)

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', 
    INDIRIZZO_DI_RESIDENZA, true)

WebUI.click(Seleziona_new)

WebUI.setText(findTestObject('Object_MOBILE/input_Civico_RESIDENZA'), CIVICO_DI_RESIDENZA)

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', 
    CIVICO_DI_RESIDENZA, true)

WebUI.click(Seleziona_new)

/*

WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Indirizzo di Atti (1)'), 60)

//WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Indirizzo di Atti (1)'), 5)
WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Indirizzo di Atti (1)'))

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Indirizzo di Atti (1)'))

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;',
	60)
*/
WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Dati Fiscali (1)'))

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Dati Fiscali (1)'))

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'), 5)

WebUI.delay(1)

//------------------------------------------------------------------FINE DATI DI RESIDENZA-----------------------------------------------------------------------
//------------------------------------------------------------------INIZIO SEZIONE DATI DI PAGAMENTO-------------------------------------------------------------
switch (METODO_PAGAMENTO) {
    case 'CARTA DI CREDITO':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Carta_di_Credito'), 
            5)

        WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Carta_di_Credito'))

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Carta_di_Credito'))

        break
    case 'CONTO CORRENTE':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Conto corrente'), 
            5)

        WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Conto corrente'))

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Conto corrente'))

        break
    case 'BOLLETTINO POSTALE':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Bollettino_Postale'), 
            5)

        WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Bollettino_Postale'))

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_Pagamento_Mobile_Bollettino_Postale'))

        break
}

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

//--------------------------------------------------------------------FINE SEZIONE DATI DI PAGAMENTO-----------------------------------------------------------------
WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Vai al Carrello (1)'))

switch (TIPO_OFFERTA_MOBILE) {
    case 'MOBILE VOCE':
        WebUI.waitForElementVisible(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'), 60)

        WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'), 60)

        WebUI.focus(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'))

        WebUI.click(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_VOCE'))

        break
    case 'MOBILE DATI':
        WebUI.waitForElementVisible(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'), 60)

        WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'), 60)

        WebUI.focus(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'))

        WebUI.click(findTestObject('Object_MOBILE/Seleziona_TipoOfferta_MOBILE_DATI'))

        break
}

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
    60)

//seleziona offerta
def offerta_string = "//span[(text() = '$OFFERTA_MOBILE')]/parent::td/parent::tr/td[5]"

def Seleziona_offerta_new = WebUI.modifyObjectProperty(findTestObject('Page_/Seleziona_Offerta_Fissa'), 'xpath', 'equals', 
    offerta_string, true)

WebUI.waitForElementClickable(Seleziona_offerta_new, 20)

//properties= Seleziona_offerta_new.getProperties()
//for (prop in properties) {
//	prop.name;
//	}
try {
    WebUI.click(Seleziona_offerta_new, FailureHandling.STOP_ON_FAILURE //This will fail as locator is wrong
        )
}
catch (Exception ex) {
    WebUI.delay(1)

    KeywordUtil.markFailedAndStop('Offerta NON Trovata')
} 

WebUI.delay(3)

//////////////////
WebUI.switchToWindowIndex(1)

//Pagina Coupon
WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'), 60)

WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'))

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'))

WebUI.switchToWindowIndex(0)

WebUI.waitForElementClickable(findTestObject('Page_Coupon/span_Procedi'), 60)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello - Opzioni'), 'style', 
    'display: none;', 60)

//----------------------------------------------INIZIO ACCORDION--------------------------------------------------------
//----------------------------------------------CONFIGURA OFFERTA MOBILE E TERMINALE----------------------------
WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_TIPO_MOBILE'), 2, FailureHandling.OPTIONAL)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_COUPON'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_TIPO_MOBILE'), FailureHandling.OPTIONAL)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

try {
    WebUI.selectOptionByValue(findTestObject('Object_MOBILE/select_Tipo_Famiglia_Mobile'), TIPO_FAMIGLIA_MOBILE, true //This will fail as locator is wrong
        )
}
catch (Exception ex) {
    WebUI.delay(1)

    KeywordUtil.markFailedAndStop('Tipo di Famiglia Mobile NON Presente')
} 

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
    60)

try {
    WebUI.selectOptionByValue(findTestObject('Object_MOBILE/select_Tipo_Vendita_Terminale'), TIPO_VENDITA_TERMINALE, true //This will fail as locator is wrong
        )
}
catch (Exception ex) {
    WebUI.delay(1)

    KeywordUtil.markFailedAndStop('Tipo Vendita Terminale NON Presente')
} 

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
    60)

try {
    WebUI.selectOptionByValue(findTestObject('Object_MOBILE/select_Tipo_Modello_Terminale'), TIPO_MODELLO_TERMINALE, true //This will fail as locator is wrong
        )
}
catch (Exception ex) {
    WebUI.delay(1)

    KeywordUtil.markFailedAndStop('Modello Terminale NON Presente')
} 

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
    60)

//----------------------------------------------FINE CONFIGURA OFFERTA MOBILE E TERMINALE----------------------------
WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_COUPON'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_OFFERTA'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_COUPON'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_OPZIONI'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_OPZIONI'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_OPZIONI'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

//----------------------------------------------INIZIO SELEZIONE TERMINALE-------------------------------------------
WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_TERMINALI'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_APPARATI'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_TERMINALI'), FailureHandling.CONTINUE_ON_FAILURE)

//def terminale_string = "//span[text()='$TERMINALE']/preceding::input[@type='radio'][1]"
def terminale_string = "//span[text()[contains(.,'$TERMINALE')]]/preceding::input[@type='radio'][1]"

def Seleziona_terminale_new = WebUI.modifyObjectProperty(findTestObject('/Object_MOBILE/Seleziona_Terminale'), 'xpath', 
    'equals', terminale_string, true)

WebUI.click(Seleziona_terminale_new, FailureHandling.OPTIONAL)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
    60)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

//----------------------------------------------FINE SELEZIONE TERMINALE-------------------------------------------
WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_CONFIGURA_USIM'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_STS'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_CONFIGURA_USIM'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_OFFERTA'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SOFTWARE'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_OFFERTA'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_OPZIONE_DATI'), 2, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_RSTP'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_OPZIONE_DATI'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

Presenza_accordion = WebUI.verifyElementPresent(findTestObject('Object_MOBILE/Open_Accordion_SCONTI'), 2, FailureHandling.OPTIONAL)

if (Presenza_accordion == true) {
    WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_SCONTI'), 2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_SCONTI'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(1)
}

//---------------------------------------------FINE CARRELLATA ACCORDION---------------------------------------------------------
//----------------------------------------------SELEZIONA MNP-TCR-------------------------------------------------------------
if (RICHIESTA_MNP == 'SI') {
    WebUI.scrollToElement(findTestObject('Object_MOBILE/Open_Accordion_CONFIGURA_USIM'), 2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Open_Accordion_CONFIGURA_USIM'), 2)

    //WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_STS'), FailureHandling.CONTINUE_ON_FAILURE)
    WebUI.click(findTestObject('Object_MOBILE/Open_Accordion_CONFIGURA_USIM'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.focus(findTestObject('Object_MOBILE/Seleziona_Richiesta_MNP'))

    WebUI.click(findTestObject('Object_MOBILE/Seleziona_Richiesta_MNP'))

    WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
        60)

    if (RICHIESTA_TCR == 'SI') {
        WebUI.focus(findTestObject('Object_MOBILE/Seleziona_Richiesta_TCR'))

        //WebUI.focus(findTestObject('Page_Coupon/Open_Accordion_SERVIZI_STS'), FailureHandling.CONTINUE_ON_FAILURE)
        WebUI.click(findTestObject('Object_MOBILE/Seleziona_Richiesta_TCR'), FailureHandling.CONTINUE_ON_FAILURE)

        WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 
            'display: none;', 60)
    }
    
    WebUI.delay(1)
}

//----------------------------------------------FINE SELEZIONA MNP-TCR-------------------------------------------------------------
//----------------------------------------------INIZIO SELEZIONA OPZIONI------------------------------------------------------------------------
/*
if (OPZIONE_F_1 != '') {
	def opzione_string = "//span[text()='$OPZIONE_F_1']/preceding::input[1]"

	def Seleziona_opzione_new = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath',
		'equals', opzione_string, true)

	def open_accordion_string = "//span[text()='$OPZIONE_F_1']/preceding::input[1]/preceding::div[contains(concat(' ', normalize-space(@class), ' '), ' main-content-box-header main-content-box-header-complex ')][1]/img"

	def open_accordion = WebUI.modifyObjectProperty(findTestObject('Page_Coupon/Seleziona_Opzione_Fissa'), 'xpath', 'equals',
		open_accordion_string, true)

	WebUI.click(open_accordion, FailureHandling.OPTIONAL)

	WebUI.waitForElementVisible(Seleziona_opzione_new, 5, FailureHandling.OPTIONAL)

	WebUI.delay(1)

	WebUI.click(Seleziona_opzione_new, FailureHandling.OPTIONAL)

	WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;',
		60)
}
*/
//----------------------------------------------------------------FINE SELEZIONA OPZIONI--------------------------------------------
WebUI.waitForElementClickable(findTestObject('Page_Coupon/span_Procedi'), 60)

WebUI.click(findTestObject('Page_Coupon/span_Procedi'))

// FUORI CARRELLO
WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/span_Successivo'), 60)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
    60)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/span_Successivo'))

//--------------------------INSERIMENTO DATI ICCID--------------------------------
def row_iccid

if (CANALE_DISTANZA == 'NO') {
    WebUI.waitForElementVisible(findTestObject('Object_MOBILE/input_ICCID'), 60)

    def max_row_num_iccid = findTestData('ICCID').getRowNumbers()

    for (def rowNum_iccid = 1; rowNum_iccid <= max_row_num_iccid; rowNum_iccid++) {
        def iccid = findTestData('ICCID').getValue(iccid_3A_3C, rowNum_iccid)

        if (iccid != 'X') {
            row_iccid = rowNum_iccid

            ICCID = findTestData('ICCID').getValue(iccid_3A_3C, row_iccid)

            WebUI.setText(findTestObject('Object_MOBILE/input_ICCID'), ICCID)

            WebUI.waitForElementAttributeValue(findTestObject('Object_MOBILE/WAIT-ICCID'), 'class', 'loading-div ng-hide', 
                60)

            WebUI.delay(1)

            def msisdn = WebUI.getAttribute(findTestObject('Object_MOBILE/input_MSISDN'), 'value')

            if (msisdn != '') {
                break
            } else {
                URL_iccd = findTestData('ICCID').getSourceUrl()

                String filePath = URL_iccd

                Workbook workbook

                FileInputStream fis

                FileOutputStream fos

                fis = new FileInputStream(filePath)

                workbook = WorkbookFactory.create(fis)

                sheet = workbook.getSheetAt(0)

                Row row = sheet.getRow(row_iccid)

                Cell cell = row.getCell(iccid_3A_3C - 1)

                cell.setCellValue('X')

                fis.close()

                fos = new FileOutputStream(filePath)

                workbook.write(fos)

                fos.close()
            }
        }
    }
}

//-------------------------- FINE INSERIMENTO DATI ICCID--------------------------------
//-------------------------------------INIZIO CONFIGURAZIONE MNP-----------------------------------------
if (RICHIESTA_MNP == 'SI') {
    WebUI.waitForElementVisible(findTestObject('Object_MOBILE/input_MNP_Nome'), 60)

    WebUI.setText(findTestObject('Object_MOBILE/input_MNP_Nome'), NOME)

    WebUI.setText(findTestObject('Object_MOBILE/input_MNP_Cognome'), COGNOME)

    WebUI.setText(findTestObject('Object_MOBILE/input_MNP_CF-PIVA'), CODICE_FISCALE)

    WebUI.selectOptionByLabel(findTestObject('Object_MOBILE/select_MNP_Tipo_Documento'), 'Carta di identità', true)

    WebUI.setText(findTestObject('Object_MOBILE/input_MNP_Numero_Documento'), NUMERO_DOCUMENTO)

    WebUI.setText(findTestObject('Object_MOBILE/input_MNP_MSISDN_Portato'), MNP_MSISDN_PORTATO)

    WebUI.selectOptionByLabel(findTestObject('Object_MOBILE/select_MNP_Tipo_Contratto_Origine'), MNP_CONTRATTO_ORIGINE, 
        true, FailureHandling.OPTIONAL)

    WebUI.selectOptionByLabel(findTestObject('Object_MOBILE/select_MNP_Operatore_Telefonico'), MNP_OPERATORE_TELEFONICO, 
        true)

    WebUI.sendKeys(findTestObject('Object_MOBILE/input_MNP_ICCID_Donating_13_cifre'), MNP_ICCID_DONATING_13_cifre)

    WebUI.delay(1)
}

//-------------------------------------FINE CONFIGURAZIONE MNP-----------------------------------------
WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Seleziona_Procedi_Configuratore_USIM'), 60)

//-------------------------------------INIZIO CONFIGURAZIONE IMEI-----------------------------------------
if (TIPO_MODELLO_TERMINALE != '') {
    if (IMEI == '') {
        def row_terminale

        int col_imei

        def imei

        def max_row_num_terminale = findTestData(FILE_IMEI).getRowNumbers()

        for (def rowNum_terminale = 1; rowNum_terminale <= max_row_num_terminale; rowNum_terminale++) {
            def terminale = findTestData(FILE_IMEI).getValue(1, rowNum_terminale)

            if (terminale == TERMINALE) {
                row_terminale = rowNum_terminale

                break
            }
        }
        
        def colNum_imei_partenza = findTestData(FILE_IMEI).getValue(5, row_terminale)

        int colNum_imei_partenza_intero = Integer.parseInt(colNum_imei_partenza)

        def tasto_procedi

        for (col_imei = colNum_imei_partenza_intero; col_imei <= 4; col_imei++) {
            if (tasto_procedi == 'disabled') {
                break
            }
            
            imei = findTestData(FILE_IMEI).getValue(col_imei, row_terminale)

            if (imei == 'X') {
                //
            } else {
                WebUI.setText(findTestObject('Object_MOBILE/input_IMEI'), imei)

                WebUI.waitForElementAttributeValue(findTestObject('Object_MOBILE/WAIT-IMEI'), 'class', 'loading-div ng-hide', 
                    60)

                tasto_procedi = WebUI.getAttribute(findTestObject('Object_MOBILE/input_IMEI'), 'disabled')

                if (tasto_procedi) {
                    break
                } else {
                    URL_imei = findTestData(FILE_IMEI).getSourceUrl()

                    String filePath = URL_imei

                    Workbook workbook

                    FileInputStream fis

                    FileOutputStream fos

                    fis = new FileInputStream(filePath)

                    workbook = WorkbookFactory.create(fis)

                    sheet = workbook.getSheetAt(0)

                    Row row = sheet.getRow(row_terminale)

                    Cell cell = row.getCell(col_imei - 1)

                    cell.setCellValue('X')

                    fis.close()

                    fos = new FileOutputStream(filePath)

                    workbook.write(fos)

                    fos.close()
                }
            }
        }
        
        // aggiorno la colonna di partenza imei per il prossimo test
        URL_imei = findTestData(FILE_IMEI).getSourceUrl()

        String filePath = URL_imei

        Workbook workbook

        FileInputStream fis

        FileOutputStream fos

        fis = new FileInputStream(filePath)

        workbook = WorkbookFactory.create(fis)

        sheet = workbook.getSheetAt(0)

        Row row = sheet.getRow(row_terminale)

        Cell cell = row.getCell(4)

        cell.setCellValue(col_imei + 1)

        fis.close()

        fos = new FileOutputStream(filePath)

        workbook.write(fos)

        fos.close()
    } else {
        WebUI.setText(findTestObject('Object_MOBILE/input_IMEI'), IMEI)

        WebUI.waitForElementAttributeValue(findTestObject('Object_MOBILE/WAIT-IMEI'), 'class', 'loading-div ng-hide', 60)
    }
}

//------------------------------------------FINE CONFIGURAZIONE IMEI-------------------------------
WebUI.delay(1)

if ((CANALE_DISTANZA == 'NO') || (RICHIESTA_MNP == 'SI')) {
    WebUI.waitForElementClickable(findTestObject('Object_MOBILE/Seleziona_Procedi_Configuratore_USIM'), 60)

    WebUI.focus(findTestObject('Object_MOBILE/Seleziona_Procedi_Configuratore_USIM'))

    CustomKeywords.'com.at.util.ClickUsingJS.clickUsingJS'(findTestObject('Object_MOBILE/Seleziona_Procedi_Configuratore_USIM'), 
        60)
}

// FASE 2
//scrol su Completamento Dati Attivazione e Portabilità
WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

/*
WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Scala_section2_scala (1)'), 5)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Scala_section2_scala (1)'), '1')

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Piano_section2_piano (1)'), '1')

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Interno_section2_interno (1)'), '1')

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_di_ativazione'), 5)

WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_di_ativazione'),
	5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_di_ativazione'))
*/
switch (METODO_PAGAMENTO) {
    case 'CARTA DI CREDITO':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/select_Visa (1)'), 5)

        WebUI.selectOptionByValue(findTestObject('Page_Fastweb - Abbonamento Online/select_Visa (1)'), CIRCUITO, true)

        WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero Carta_section3_nu'), NUMERO_CARTA_CONTO)

        WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Data di Scadenza_section (1)'), '12/22')

        WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Pagamento (1)'), 
            60)

        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Pagamento (1)'), 5)

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Pagamento (1)'))

        break
    case 'CONTO CORRENTE':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Dati Clienti'), 5)

        WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Dati Clienti'))

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia Dati Clienti'))

        WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
            60)

        // scroll su Completamento Dati Banca
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero conto corrente'), 5)

        WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero conto corrente'))

        WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero conto corrente'), NUMERO_CARTA_CONTO)

        break
    case 'BOLLETTINO POSTALE':
        break
}

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

//scroll su Documenti
WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Nazionalit Documento_sec (1)'), NAZIONALITA_DOCUMENTO)

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', 
    NAZIONALITA_DOCUMENTO, true)

WebUI.click(Seleziona_new)

//WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/strong_ITALIA (1)'))
WebUI.selectOptionByValue(findTestObject('Page_Fastweb - Abbonamento Online/select_Carta di identita (1)'), 'CARTA DI IDENTITA', 
    true)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Numero Documento_section (1)'), NUMERO_DOCUMENTO)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Localit di Rilascio Docu (1)'), CITTA_RILASCIO_DOCUMENTO)

Seleziona_new = WebUI.modifyObjectProperty(findTestObject('Page_Fastweb - Abbonamento Online/Seleziona'), 'title', 'equals', 
    CITTA_RILASCIO_DOCUMENTO, true)

WebUI.click(Seleziona_new)

//WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/strong_MILANO (1)'))
WebUI.selectOptionByValue(findTestObject('Page_Fastweb - Abbonamento Online/select_Comune (1)'), 'COMUNE', true)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Data di Rilascio Documen (1)'), DATA_RILASCIO_DOCUMENTO)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Documento (1)'), 5)

WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Documento (1)'))

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Verifica Documento (1)'))

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

//------------------------------------------INIZIO CONSENSI--------------------------------------
WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoPubb (1)'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoPubb (1)'))

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoGest (1)'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoGest (1)'))

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoAnal (1)'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoAnal (1)'))

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoCessionDati'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoCessionDati'))

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

//------------------------------------------FINE CONSENSI--------------------------------------
WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiFiscali'), 0)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiFiscali'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiAttivazione'), 5, FailureHandling.OPTIONAL)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiAttivazione'), FailureHandling.OPTIONAL)

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_CompletamentoDati_IndirizzoCopertura'), 
    5, FailureHandling.OPTIONAL)

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiPagamento'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiPagamento'))

WebUI.delay(1)

switch (METODO_PAGAMENTO) {
    case 'CARTA DI CREDITO':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_Completamentodati_MetodoPagamento'), 
            5)

        WebUI.delay(1)

        break
    case 'CONTO CORRENTE':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_Completamentodati_MetodoPagamento'), 
            5)

        WebUI.delay(1)

        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_CompletamentoDatiBanca'), 
            5)

        WebUI.delay(1)

        break
}

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_Documenti'), 5)

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Genera Riepilogo Ordine (1)'), 5)

WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Genera Riepilogo Ordine (1)'), 60)

WebUI.delay(1)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Genera Riepilogo Ordine (1)'))

//def CANALE_DISTANZA = findTestData('CANALI').getValue(8, row_canale)
switch (CANALE_DISTANZA) {
    case 'SI':
        if (CANALE != 'AGENTE') {
            WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
                60, FailureHandling.CONTINUE_ON_FAILURE)

            WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi (1)'), FailureHandling.CONTINUE_ON_FAILURE)
        }
        
        WebUI.waitForElementVisible(findTestObject('Page_Fastweb - Abbonamento Online/button_Conferma (1)'), 60)

        WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Conferma (1)'), 60)

        WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
            60)

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Conferma (1)'))

        WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
            60)

        break
    case 'NO':
        WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Stampa_PDA'), 60)

        WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
            60)

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Stampa_PDA'))

        WebUI.closeWindowIndex(1)

        WebUI.switchToWindowIndex(0)

        WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
            60)

        WebUI.waitForElementVisible(findTestObject('Page_Fastweb - Abbonamento Online/button_Conferma_CANALI_NO_DISTANZA'), 
            60)

        WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Conferma_CANALI_NO_DISTANZA'), 
            60)

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Conferma_CANALI_NO_DISTANZA'))

        WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
            60)

        break
}

WebUI.delay(5)

if (CANALE_DISTANZA == 'NO') {
    URL_iccd = findTestData('ICCID').getSourceUrl()

    String filePath = URL_iccd

    Workbook workbook

    FileInputStream fis

    FileOutputStream fos

    fis = new FileInputStream(filePath)

    workbook = WorkbookFactory.create(fis)

    sheet = workbook.getSheetAt(0)

    Row row = sheet.getRow(row_iccid)

    Cell cell = row.getCell(iccid_3A_3C - 1)

    cell.setCellValue('X')

    fis.close()

    fos = new FileOutputStream(filePath)

    workbook.write(fos)

    fos.close()
}

WebUI.closeBrowser()

