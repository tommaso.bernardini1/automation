import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.io.FileInputStream as FileInputStream
import java.io.FileOutputStream as FileOutputStream
import java.io.FileNotFoundException as FileNotFoundException
import java.io.IOException as IOException
import java.util.Date as Date
import org.apache.poi.hssf.usermodel.HSSFCell as HSSFCell
import org.apache.poi.hssf.usermodel.HSSFCellStyle as HSSFCellStyle
import org.apache.poi.hssf.usermodel.HSSFDataFormat as HSSFDataFormat
import org.apache.poi.hssf.usermodel.HSSFRow as HSSFRow
import org.apache.poi.hssf.usermodel.HSSFSheet as HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook as HSSFWorkbook
import org.apache.poi.hssf.util.HSSFColor as HSSFColor
import org.apache.poi.ss.usermodel.Cell as Cell
import org.apache.poi.xssf.usermodel.XSSFCell as XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow as XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import org.apache.poi.openxml4j.exceptions.InvalidFormatException as InvalidFormatException
import org.apache.poi.ss.usermodel.Row as Row
import org.apache.poi.ss.usermodel.Workbook as Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory as WorkbookFactory
import org.apache.poi.ss.usermodel.Sheet as Sheet
import com.kms.katalon.core.exception.StepErrorException as StepErrorException
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.By as By
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject

import internal.GlobalVariable as GlobalVariable




WebUI.openBrowser('')

def row_canale= CustomKeywords.'com.at.util.Canale.Seleziona_Canale'(CANALE)
def CANALE_DISTANZA = CustomKeywords.'com.at.util.Canale.Canale_a_distanza'(CANALE)

//---------------------------------------

def iccid_3A_3C
def FILE_IMEI
def FILE_ICCID

if (TIPOLOGIA_OFFERTA != 'FISSO') {
	def List<String> file = CustomKeywords.'com.at.util.Seleziona_file_iccid_imei.recupera_file_iccid_imei'(CATENA, SEGMENTO)
	iccid_3A_3C = file.get(0)
	FILE_IMEI = file.get(1)
	FILE_ICCID = file.get(2)
}

def URL_EXCEL_INPUT
def Workbook workbook_excel_input
def FileInputStream fis_excel_input
def FileOutputStream fos_excel_input
def max_row_num_cliente



if (GlobalVariable.VERIFICHE_ATTIVE== 'SI')  {
	
	GlobalVariable.STRING_FILE='Data Files/FILE_INPUT_ANAGRAFICA_DATA_PREPARATION/INPUT_ANAGRAFICA_' + TIPOLOGIA_OFFERTA + '_' + SEGMENTO
	max_row_num_cliente = findTestData(GlobalVariable.STRING_FILE).getRowNumbers()
	
	switch (SEGMENTO) {
		case 'RES':
		CustomKeywords.'com.at.util.Lavorazione_file_excel.calcola_riga_cliente'(CODICE_FISCALE, max_row_num_cliente, GlobalVariable.NUM_COLONNA_CODICE_FISCALE)
		break
		
		case 'SHP':
		CustomKeywords.'com.at.util.Lavorazione_file_excel.calcola_riga_cliente'(PARTITA_IVA, max_row_num_cliente, GlobalVariable.NUM_COLONNA_PARTITA_IVA)
		break
	}
}




//------------------------------------------
CustomKeywords.'com.at.util.Accesso_Con_Selezione_Catena_AccessoUtenza_Comsy.Selezione_Catena'(CATENA, row_canale)

CustomKeywords.'com.at.util.Accesso_Con_Selezione_Catena_AccessoUtenza_Comsy.Selezione_AccessoUtenza'(row_canale)

CustomKeywords.'com.at.util.Accesso_Con_Selezione_Catena_AccessoUtenza_Comsy.Selezione_Comsy'(SEGMENTO, COMSY, row_canale)






//------------INIZIO PARTE ANAGRAFICA - FASE 1------------------------------------------------
switch (SEGMENTO) {
    case 'RES':
        CustomKeywords.'com.at.util.Anagrafica_RES.Inserimento_Anagrafica_RES'(NOME, COGNOME, SESSO, DATA_DI_NASCITA, NAZIONE_DI_NASCITA, 
            COMUNE_DI_NASCITA, NUMERO_MOBILE, EMAIL, CODICE_FISCALE, TIPOLOGIA_OFFERTA)

        break
    case 'SHP':
        CustomKeywords.'com.at.util.Anagrafica_SHP.Inserimento_Anagrafica_SHP'(RAGIONE_SOCIALE, FORMA_GIURIDICA, NOME_RAPPRESENTANTE_LEGALE, 
            COGNOME_RAPPRESENTANTE_LEGALE, SESSO_RAPPRESENTANTE_LEGALE, DATA_DI_NASCITA_RAPPRESENTANTE_LEGALE, NAZIONE_DI_NASCITA_RAPPRESENTANTE_LEGALE, 
            COMUNE_DI_NASCITA_RAPPRESENTANTE_LEGALE, NUMERO_MOBILE, EMAIL, CODICE_FISCALE_RAPPRESENTANTE_LEGALE, PARTITA_IVA, 
            TIPOLOGIA_OFFERTA)

        break
}

//----------FINE DATI ANAGRAFICI--------------------------------------------------------
//-----------------INIZIO INDIRIZZO DI ATTIVAZIONE e RESIDENZA-----------------------------------------------
//
switch (SEGMENTO) {
	case 'RES':
	CustomKeywords.'com.at.util.Inserimento_IndirizzoAttivazione_e_IndirizzoResidenza.Inserimento_Ind_Attivazione_Ind_Residenza_RES'(TIPOLOGIA_OFFERTA, TECNOLOGIA, SEGMENTO, CITTA_DI_RESIDENZA, INDIRIZZO_DI_RESIDENZA, CIVICO_DI_RESIDENZA)
	break
	
	case 'SHP':
	CustomKeywords.'com.at.util.Inserimento_IndirizzoAttivazione_e_IndirizzoResidenza.Inserimento_Ind_Attivazione_Ind_Residenza_SHP'(TIPOLOGIA_OFFERTA, TECNOLOGIA, SEGMENTO, CITTA_DI_RESIDENZA_SEDE_LEGALE,
	INDIRIZZO_DI_RESIDENZA_SEDE_LEGALE, CIVICO_DI_RESIDENZA_SEDE_LEGALE)
	break
}
//

//---------------------FINE INDIRIZZO DI ATTIVAZIONE e RESIDENZA-----------------------------------------------
//------INIZIO SEZIONE SELEZIONE DATI DI PAGAMENTO-------------------------------------------------------------
CustomKeywords.'com.at.util.Selezione_Metodo_di_Pagamento.Inserimento_Ind_Attivazione_Ind_Residenza'(TIPOLOGIA_OFFERTA, 
    METODO_PAGAMENTO)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

//-----------FINE SEZIONE SELEZIONE DATI DI PAGAMENTO-----------------------------------------------------------------
WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Vai al Carrello (1)'))

//------------------SELEZIONA OFFERTA-------------------------------


switch (SEGMENTO) {
	case 'RES':
	
	CustomKeywords.'com.at.util.Selezione_Offerta.Seleziona_Offerta_Fissa'(OFFERTA_FISSA, GlobalVariable.NUM_COLONNA_ESITO_VENDIBILITA_OFFERTA_FISSA_RES)

		break
		
	case 'SHP':
	
	CustomKeywords.'com.at.util.Selezione_Offerta.Seleziona_Offerta_Fissa'(OFFERTA_FISSA_SHP, GlobalVariable.NUM_COLONNA_ESITO_VENDIBILITA_OFFERTA_FISSA_SHP)
	
		break
}
WebUI.delay(2)

//--------------------------------PAGINA COUPON-----------------------------------
WebUI.switchToWindowIndex(1)

WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'), 60)

WebUI.focus(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'))

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Procedi'))

WebUI.switchToWindowIndex(0)

WebUI.waitForElementClickable(findTestObject('Page_Coupon/span_Procedi'), 60)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello - Opzioni'), 'style', 
    'display: none;', 60)

// CARRELLO
//------------Apriamo gli accordion----------------------------
CustomKeywords.'com.at.util.Apertura_Accordion.AperturaTuttigliAccordion'()

//------------fine apertura accordion-----------------------------------
//----------------------------------------------INIZIO SELEZIONA OPZIONI------------------------------------------------------------------------
switch (SEGMENTO) {
	case 'RES':
	CustomKeywords.'com.at.util.Selezione_Opzioni.Seleziona_Opzione'(OPZIONE_F_1, OPZIONE_F_2, OPZIONE_F_3, OPZIONE_F_4, OPZIONE_F_5, GlobalVariable.NUM_COLONNA_ESITO_OPZIONE_F_1_FISSO_RES, GlobalVariable.NUM_COLONNA_ESITO_OPZIONE_F_2_FISSO_RES, GlobalVariable.NUM_COLONNA_ESITO_OPZIONE_F_3_FISSO_RES, GlobalVariable.NUM_COLONNA_ESITO_OPZIONE_F_4_FISSO_RES, GlobalVariable.NUM_COLONNA_ESITO_OPZIONE_F_5_FISSO_RES)
	break
	case 'SHP':
	CustomKeywords.'com.at.util.Selezione_Opzioni.Seleziona_Opzione'(OPZIONE_F_1, OPZIONE_F_2, OPZIONE_F_3, OPZIONE_F_4, OPZIONE_F_5, GlobalVariable.NUM_COLONNA_ESITO_OPZIONE_F_1_FISSO_SHP, GlobalVariable.NUM_COLONNA_ESITO_OPZIONE_F_2_FISSO_SHP, GlobalVariable.NUM_COLONNA_ESITO_OPZIONE_F_3_FISSO_SHP, GlobalVariable.NUM_COLONNA_ESITO_OPZIONE_F_4_FISSO_SHP, GlobalVariable.NUM_COLONNA_ESITO_OPZIONE_F_5_FISSO_SHP)
	break
}

//-------------------------------------------FINE SELEZIONA OPZIONI---------------------------------------------------
WebUI.waitForElementClickable(findTestObject('Page_Coupon/span_Procedi'), 60)

WebUI.click(findTestObject('Page_Coupon/span_Procedi'))

// FUORI CARRELLO
WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/span_Successivo'), 60)

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT - Carrello'), 'style', 'display: none;', 
    60)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/span_Successivo'))

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

// FASE 2
//Completamento Dati Attivazione e Portabilit�
WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_Scala_section2_scala (1)'), 5)

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Scala_section2_scala (1)'), '1')

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Piano_section2_piano (1)'), '1')

WebUI.setText(findTestObject('Page_Fastweb - Abbonamento Online/input_Interno_section2_interno (1)'), '1')

switch (SEGMENTO) {
    case 'RES':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_di_ativazione'), 
            5)

        WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_di_ativazione'), 
            5)

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_di_ativazione'))

        break
    case 'SHP':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_di_ativazione - SHP'), 
            5)

        WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_di_ativazione - SHP'), 
            5)

        WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Copia_Indirizzo_di_ativazione - SHP'))

        break
}

//completamento dati metodo di pagamento
switch (SEGMENTO) {
    case 'RES':
        CustomKeywords.'com.at.util.Completamento_Dati_MetodoDiPagamento.Inserimento_Dati_Pagamento_RES'(METODO_PAGAMENTO, 
            CIRCUITO, NUMERO_CARTA_CONTO)

        break
    case 'SHP':
        CustomKeywords.'com.at.util.Completamento_Dati_MetodoDiPagamento.Inserimento_Dati_Pagamento_SHP'(METODO_PAGAMENTO, 
            CIRCUITO, NUMERO_CARTA_CONTO, IBAN_CDC_INTESTATO_AZIENDA)

        break
}

//completamento dati documenti
CustomKeywords.'com.at.util.Completamento_Dati_Documento.Inserimento_Dati_Documento'(NAZIONALITA_DOCUMENTO, NUMERO_DOCUMENTO, 
    CITTA_RILASCIO_DOCUMENTO, DATA_RILASCIO_DOCUMENTO)

// completamento dati Consensi
WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoPubb (1)'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoPubb (1)'))

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoGest (1)'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoGest (1)'))

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoAnal (1)'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoAnal (1)'))

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoCessionDati'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/input_SI_section4_consensoCessionDati'))

WebUI.waitForElementAttributeValue(findTestObject('Page_Fastweb - Abbonamento Online/WAIT'), 'style', 'display: none;', 
    60)

WebUI.delay(1)

//apertura accordion dell'anagrafica 
WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiFiscali'), 0)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiFiscali'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiAttivazione'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiAttivazione'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiResidenza'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_CompletamentoDati_IndirizzoCopertura'), 
    5)

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiPagamento'), 5)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_DatiDiPagamento'))

WebUI.delay(1)

switch (METODO_PAGAMENTO) {
    case 'CARTA DI CREDITO':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_Completamentodati_MetodoPagamento'), 
            5)

        WebUI.delay(1)

        break
    case 'CONTO CORRENTE':
        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_Completamentodati_MetodoPagamento'), 
            5)

        WebUI.delay(1)

        WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_CompletamentoDatiBanca'), 
            5)

        WebUI.delay(1)

        break
}

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_espandi_Documenti'), 5)

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('Page_Fastweb - Abbonamento Online/button_Genera Riepilogo Ordine (1)'), 5)

WebUI.waitForElementClickable(findTestObject('Page_Fastweb - Abbonamento Online/button_Genera Riepilogo Ordine (1)'), 60)

WebUI.delay(1)

WebUI.click(findTestObject('Page_Fastweb - Abbonamento Online/button_Genera Riepilogo Ordine (1)'))

//finalizzazione ordine post genera riepilogo ordine
CustomKeywords.'com.at.util.Finalizzazione_Ordine_Post_GeneraRiepilogoOrdine.finalizzazione_Ordine'(CANALE_DISTANZA, CANALE)

//wait e chiusura Browser
WebUI.delay(3)

WebUI.closeBrowser()

